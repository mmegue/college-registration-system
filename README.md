<p align="center"><a href="https://gitlab.com/mmegue/college-registration-system" target="_blank"><img src="https://gitlab.com/uploads/-/system/project/avatar/55102073/cms-logo.png" width="400" alt="Registration Management System"></a></p>

## About College Registration Management System
The College Registration System is an application designed to simplify and streamline the process of course registration for students and administrators in colleges and universities. This system allows students to view course offerings, register for classes, drop classes, and view their schedules. Administrators can add or remove courses, manage course capacities, and view enrollment statistics. The application is built with a focus on user-friendliness and robust error handling, ensuring a smooth registration experience for all users.


## Requirements
- PHP >= 8.0
- Download, install and setup Git
- Download and install Composer

## Installation
- Run `git clone https://gitlab.com/mmegue/college-registration-system`.
- `cd college-registration-system`
- Run `composer install`
- From the projects root run `cp .env.example .env`
- Configure your `.env` file
- Run `php artisan key:generate`
- Run `php artisan migrate` (Make sure the database credentials are set correctly in .env)
- Run `php artisan db:seed`

## Login Details
**[USERNAME]**: super-admin@localhost.local
**[PASSWORD]**: password

## License
College Registration System is licensed under the MIT license.

