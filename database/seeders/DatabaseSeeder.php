<?php

namespace Database\Seeders;

use App\Models\CompanyDetail;
use App\Models\LecturerAllocation;
use App\Models\MyDefaultModule;
use App\Models\PaymentArrangment;
use App\Models\Quotation;
use App\Models\QuotationModule;
use App\Models\Setting;
use App\Models\TimetableWeekdaySat;
use App\Models\TimetableWeekdaySun;
use App\Models\TimetableWeekendSat;
use App\Models\TimetableWeekendSun;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        /** SETTINGS*/

        $settings = [
            'banking' => [
                'bank_name' => "Your Bank Name",
                'account_name' => "Your Account Name",
                'account_number' => "Your Account Number",
                'account_type' => "Your Account Type",
                'branch_code' => "Your Branch Code",
            ],

            'contact' => [
                'company_name' => 'College Registration System',
                'cell_1' => '+27 12 345 0000',
                'cell_2' => '+27 12 345 0001',
                'cell_3' => '+27 12 345 0002',
                'email' => 'info@localhost.local',
                'from_email' => 'no-reply@localhost.local',
            ],

            'address' => [
                'address1' => '373, Xyz Building',
                'address2' => 'Jose Str',
                'city' => 'Harare',
                'state' => 'Mashonaland',
                'zip' => '0263',
                'country' => 'Zimbabwe',
            ],

            'registration' => [
                "currency_symbol" => "$",
                "reg_fee"=> "200",
                "semester"=> "2024S3",
                "tax_period"=> "TX2024",
                "allocation_period"=> "2024",
                "semesters" => "2024S1,2024S2,2024S1,2025S2,2025S1,2025S2",
            ],

            'communication' => [
                'slack_webhook_prod' => "https://hooks.slack.com/services/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",
                'slack_webhook_sandbox' => "https://hooks.slack.com/services/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",
            ],

            'url_hashing'  => [
                'url_hashing' => true,
            ]
        ];

        foreach($settings as $key => $value){
            Setting::store($key, $value);
        }


        /**PERMISSIONS*/
        $permissions=[

            /* LOGS */
            'can_view_smsemail_logs','can_view_classlists_logs','can_view_system_logs',

            /** SETTINGS */
            'can_update_settings','can_view_settings',

            /** AUTHENTICATION */
            'can_change_roles', 'can_change_permissions','can_update_role_permissions', 'can_view_users', 'can_crud_users','can_update_password',
            'can_create_users','can_view_user',

            /** TODOS */
            'can_crud_todo','can_create_todo','can_edit_todo',

            /** CLASSLIST */
            'can_request_classlist_master', 'can_export_master_classlist',

            /** FINANCE */
            'can_crud_transactions','can_view_transactions','can_export_transactions','can_import_transactions','can_dump_transactions','can_view_deleted_transactions',
            'can_crud_payment_arrangment','can_view_payment_arrangment','can_view_statements','can_export_statement','can_email_statement',
            'can_view_cash_receipts','can_crud_cash_receipts','can_view_outstanding_debtors',

            /** MASTER DATA */
            'can_view_masterdata', 'can_crud_masterdata','can_export_masterdata', 'can_import_masterdata',

            /** ALLOCATIONS */
            'can_do_allocations',

            /** REGISTRATION */
            'can_view_registration','can_request_quote','can_view_quote','can_view_quotes','can_invoice_quote','can_edit_quote','can_edit_invoice',
            'can_delete_quote','can_delete_invoice','can_delete_payment_arrangement',
            'can_download_quote', 'can_email_quote','can_download_invoice', 'can_email_invoice',
            'can_change_reg_fee', 'can_assign_modules_to_lecturers', 'can_permanent_delete',

            /** TIMETABLE */
            'can_crud_timetable',

            /* REPORTS */
            'can_view_reports','can_view_taxreports',

            'can_view_assignments','can_allocate_assignment',
            'can_view_classlist_master','can_download_statement'
        ];

        /*ROLES*/
        foreach($permissions as $ar){
            Permission::create(['name' => $ar, 'guard_name' => 'web']);
        }

        /*ROLES*/

        Role::create(['name' => 'super-admin'])
            ->givePermissionTo(Permission::all());

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'finance']);
        Role::create(['name' => 'academic']);
        Role::create(['name' => 'online']);
        Role::create(['name' => 'assessment']);
        Role::create(['name' => 'portfolio']);
        Role::create(['name' => 'marketing']);
        Role::create(['name' => 'lecturer']);
        Role::create(['name' => 'student']);
        Role::create(['name' => 'uars']);

        /**SUPER-ADMIN*/
        $dev = User::create([
            'title' => 'Mr',
            'name' => 'Super',
            'surname' => 'Admin',
            'cell' => '27123456789',
            'email' => 'super-admin@localhost.local',
            'password' => Hash::make('password'),
        ]);
        $dev->assignRole('super-admin');

        /**UPPER-ADMIN*/
        $dev = User::create([
            'title' => 'Ms',
            'name' => 'Upper',
            'surname' => 'Admin',
            'cell' => '27123456788',
            'email' => 'upper-admin@localhost.local',
            'password' => Hash::make('password'),
        ]);
        $dev->assignRole('finance');

        /**ADMIN*/
        $dev = User::create([
            'title' => 'Mrs',
            'name' => 'Main',
            'surname' => 'Admin',
            'cell' => '27123456787',
            'email' => 'admin@localhost.local',
            'password' => Hash::make('password'),
        ]);
        $dev->assignRole('admin');

        /**LECTURER*/
        $dev = User::create([
            'title' => 'Mr',
            'name' => 'Lecturer',
            'surname' => 'System',
            'cell' => '27123456786',
            'email' => 'lecturer@localhost.local',
            'password' => Hash::make('password'),
        ]);
        $dev->assignRole('lecturer');

        /**STUDENT*/
        $dev = User::create([
            'title' => 'Mr',
            'name' => 'Student',
            'surname' => 'System',
            'cell' => '27123456785',
            'email' => 'student@localhost.local',
            'password' => Hash::make('password'),
        ]);
        $dev->assignRole('student');

        /**UARS*/
        $dev = User::create([
            'title' => 'Mr',
            'name' => 'Uars',
            'surname' => 'System',
            'cell' => '27123456784',
            'email' => 'uars@localhost.local',
            'password' => Hash::make('password'),
        ]);
        $dev->assignRole('uars');

        $files = [
            'categories'=>'categories',
            'modules'=>'modules',
            'pricings'=>'pricings',
            'service_types'=>'service_types',
        ];

        foreach($files as $table=>$file){
            $path = __DIR__."/json/{$file}.json";
            $json = json_decode(file_get_contents($path), true);
            foreach(array_chunk($json, 1000) as $arr){
                DB::table($table)->insert($arr);
            }
        }
    }
}
