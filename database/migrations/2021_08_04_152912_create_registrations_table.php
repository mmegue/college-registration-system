<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('module_code');
            $table->string('service_type');
            $table->integer('price');

            $table->string('status')->default('Invoiced');
            $table->string('quotation_number');
            $table->string('invoice_number');
            $table->string('semester');
            $table->string('attendance')->nullable();

            $table->integer('quote_created_by');
            $table->integer('invoiced_by');
            $table->integer('referred_by')->nullable();
            $table->integer('lecturer_id_default')->nullable();
            $table->integer('lecturer_id_alt')->nullable();
            $table->integer('module_added_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->text('note')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
