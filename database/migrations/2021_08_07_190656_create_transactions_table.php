<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('amount');
            $table->text('item')->nullable();
            $table->string('gl_account')->nullable();
            $table->string('campus')->nullable();
            $table->string('transaction_date')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('fs_accounts')->nullable();
            $table->string('tax_period')->nullable();
            $table->string('allocation_period')->nullable();
            $table->text('short_description')->nullable();
            $table->string('receipt_number')->nullable();
            $table->string('semester_period')->nullable();
            $table->string('internal_transaction_number')->nullable();
            $table->string('cell_number')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('credit')->nullable();
            $table->string('debit')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
