<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_quotations', function (Blueprint $table) {
            $table->id();
            $table->string('module_code');
            $table->string('service_type');
            $table->string('attendance')->nullable();
            $table->integer('price');
            $table->unsignedBigInteger('cell');
            $table->integer('added_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_quotations');
    }
}
