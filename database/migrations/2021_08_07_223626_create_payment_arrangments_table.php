<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentArrangmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_arrangments', function (Blueprint $table) {
            $table->id();
            $table->integer('quotation_id');
            $table->string('when')->nullable();
            $table->string('how_much')->nullable();
            $table->boolean('approval')->default(false);
            $table->string('status')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('send_statement')->nullable();
            $table->integer('added_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_arrangments');
    }
}
