<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('status')->default('Quotation');
            $table->string('quotation_number');
            $table->string('invoice_number')->nullable();
            $table->string('semester');
            $table->integer('created_by')->nullable(); //default 0 to represent guest
            $table->integer('invoiced_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('referred_by')->nullable();
            $table->text('note')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
