<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todos', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('priority');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('assigned_to')->nullable();
            $table->integer('created_by');
            $table->boolean('importance')->default(false);
            $table->string('status')->default("Pending");
            $table->longText('description');
            $table->string('batch_code');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todos');
    }
}
