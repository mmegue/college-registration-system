<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->id();
            $table->integer('lecturer_id');
            $table->integer('student_id');
            $table->integer('quotation_modules_id');
            $table->string('module_code');
            $table->string('service_type');
            $table->string('status')->default('Pending');
            $table->string('assignment_type');
            $table->longText('assignment')->nullable();
            $table->text('lecturer_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
