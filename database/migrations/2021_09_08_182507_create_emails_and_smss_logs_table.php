<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailsAndSmssLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails_and_smss_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cell_number');
            $table->integer('sent_by');
            $table->string('subject');
            $table->longText('message');
            $table->string('log_type');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails_and_smss_logs');
    }
}
