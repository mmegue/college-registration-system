<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_modules', function (Blueprint $table) {
            $table->id();
            $table->integer('quotation_id');
            $table->string('module_code');
            $table->string('service_type');
            $table->integer('price');
            $table->string('attendance')->nullable();

            $table->string('semester');
            $table->string('cell');
            $table->string('status')->default('Quotation');

            $table->integer('lecturer_id_default')->nullable();
            $table->integer('lecturer_id_alt')->nullable();

            $table->integer('assignment_lecturer_allocation')->nullable();
            $table->integer('assignment_id')->nullable();

            $table->integer('added_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_modules');
    }
}
