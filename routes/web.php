<?php

use App\Http\Hasher;
use App\Http\Livewire\GuestRequestQuote;
use App\Http\Livewire\RequestQuote;
use App\Http\Traits\Helper;
use App\Models\Setting;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

if (Schema::hasTable('settings')) {
    if(Setting::get_single_setting('url_hashing')){
        Route::bind('id', function ($id) {
            return Hasher::decode($id);
        });
    }
}

/*Route::post('login', [LoginController::class, 'authenticate']);  */


Route::get('/timetable-weekday', function () {
    return view('livewire.timetable-weekday');
})->name('timetable-weekday');

Route::get('/timetable-weekend-sat', function () {
    return view('livewire.timetable-weekend-sat');
})->name('timetable-weekend-sat');

Route::get('/timetable-weekend-sun', function () {
    return view('livewire.timetable-weekend-sun');
})->name('timetable-weekend-sun');

/*Route::get('guest/quotation', GuestQuotation::class)->name('guest.quotation'); */

Route::get('request-quote',GuestRequestQuote::class)->name('guest.request.quote');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/addperm', function () {
    /**PERMISSIONS*/
    $permissions=[

        'can_view_outstanding_debtors'
    ];

    /*ROLES*/
    foreach($permissions as $ar){
        Permission::create(['name' => $ar, 'guard_name' => 'web']);
    }
});

Route::get('/addsett', function () {
    $settings = [
        'communication' => [
            'slack_webhook_prod' => null,
            'slack_webhook_sandbox' => null,
        ],
    ];

    foreach($settings as $key => $value){
        Setting::store($key, $value);
    }

});

Auth::routes();

Route::group(['middleware' => ['auth:web', 'verified']], function () {

    Route::get('account/user-profile', \App\Http\Livewire\Authentication\UserProfile::class)->name('user.profile');
    /*Apps*/
    Route::get('app/notes', \App\Http\Livewire\Notes\Notes::class)->name('notes');
    Route::get('app/todos', \App\Http\Livewire\Todos\Todos::class)->name('todos');
    Route::get('app/chat', \App\Http\Livewire\Chat\Chat::class)->name('chat');
    /** ADMIN ROUTE PREFIX *//** Livewire template */
    Route::group(['middleware' => ['role:super-admin|admin|finance|academic|online|assessment|portfolio|marketing|lecturer']], function () {

        Route::prefix('sysmain')->group(function () {
            /**Local API*/
            Route::post('view-quotation-pdf',function(){
                return view('livewire.registration.quotation-template');
            })->name('view-quotation-pdf');

            /*Dashboard*/
            Route::get('/dashboard', \App\Http\Livewire\Dashboard::class)->name('admin.dashboard');

            /*Logs*/
            Route::get('/sms-emails-logs', \App\Http\Livewire\Logs\EmailsAndSmssLog::class)
                ->name('sms.emails.logs')->middleware('permission:can_view_smsemail_logs');
            Route::get('/generated-classlist', \App\Http\Livewire\Logs\GeneratedClassListLogs::class)
                ->name('generated.classlist')->middleware('permission:can_view_classlists_logs');

            /*Authentication & Security*/
            Route::get('/students', \App\Http\Livewire\Authentication\Students::class)->name('students');
            Route::get('/users', \App\Http\Livewire\Authentication\Users::class)->name('users');
            Route::get('/view-user/{id}', \App\Http\Livewire\Authentication\ViewUser::class)->name('view.user');
            Route::get('/roles-permissions/', \App\Http\Livewire\Authentication\RolesPermissions::class)->name('roles.permissions');


            /**My Admin*/
            Route::get('/mydefault-modules', \App\Http\Livewire\MyDefaultModules\MyDefaultModule::class)
                ->name('mydefault-modules')->middleware('permission:can_request_quote');

            /**Allocations*/
            Route::group(['middleware' => ['permission:can_do_allocations']], function () {
                Route::get('/lecturer-allocations', \App\Http\Livewire\Lecturer\Allocations::class)->name('lecturer.allocations');
                Route::get('/bulk-allocations', \App\Http\Livewire\BulkAllocation\BulkAllocations::class)->name('bulk.allocations');
            });


            /**Classlists*/
            Route::get('/classlist-master', \App\Http\Livewire\Classlist\Master::class)
                ->name('classlist.master')->middleware('permission:can_view_classlist_master');


            /**Master Data*/
            Route::group(['middleware' => ['permission:can_view_masterdata|can_crud_masterdata']], function () {
                Route::get('/categories', \App\Http\Livewire\Categories\Category::class)->name('categories');
                Route::get('/modules', \App\Http\Livewire\Modules\Module::class)->name('modules');
                Route::get('/service-types', \App\Http\Livewire\ServiceTypes\ServiceType::class)->name('servicetypes');
                Route::get('/pricing', \App\Http\Livewire\Pricings\Pricing::class)->name('pricing');
                Route::get('/payments', \App\Http\Livewire\Payments\Payment::class)->name('payments');
            });

            /**Reports*/
            Route::get('/reports', \App\Http\Livewire\Reports\Report::class)
                ->name('reports')->middleware('permission:can_view_reports');
            Route::get('/tax-reports', \App\Http\Livewire\Reports\TaxReport::class)
                ->name('taxreports')->middleware('permission:can_view_taxreports');

            /**Finance*/
            Route::get('/transactions', \App\Http\Livewire\Transaction\Transactions::class)
                ->name('transactions')->middleware('permission:can_view_transactions');

            Route::get('/view-transaction/{id}', \App\Http\Livewire\Transaction\ViewTransaction::class)
                ->name('view.transaction')->middleware('permission:can_view_transactions');

            Route::get('/dump-transactions', \App\Http\Livewire\Transaction\DumpTransactions::class)
                ->name('dump.transactions')->middleware('permission:can_dump_transactions');

            Route::get('/deleted-transactions',\App\Http\Livewire\Transaction\DeletedTransactions::class)
                ->name('deleted.transactions')->middleware('permission:can_view_deleted_transactions');

            Route::get('/cash-receipts', \App\Http\Livewire\Transaction\CashReceipts::class)
                ->name('cash.receipts')->middleware('permission:can_view_cash_receipts');

            Route::get('/statements', \App\Http\Livewire\Statement\Statements::class)
                ->name('statements')->middleware('permission:can_view_statements');

            Route::get('/payment-arrangement', \App\Http\Livewire\PaymentArrangments\PaymentArrangments::class)
                ->name('payment.arrangement')->middleware('permission:can_view_payment_arrangment');

            Route::get('/outstanding-debtors', \App\Http\Livewire\Debtors\OutstandingDabtors::class)
                ->name('outstanding.debtors')->middleware('permission:can_view_outstanding_debtors');

            /**TIME TABLE*/
            Route::get('/timetable-weekend-sat',\App\Http\Livewire\Timetable\TimetableWeekendSat::class)
                ->name('timetable.weekend.sat')->middleware('permission:can_crud_timetable');

            Route::get('/timetable-weekend-sun', \App\Http\Livewire\Timetable\TimetableWeekendSun::class)
                ->name('timetable.weekend.sun')->middleware('permission:can_crud_timetable');

            Route::get('/timetable-weekday-sat', \App\Http\Livewire\Timetable\TimetableWeekdaySat::class)
                ->name('timetable.weekday.sat')->middleware('permission:can_crud_timetable');

            Route::get('/timetable-weekday-sun', \App\Http\Livewire\Timetable\TimetableWeekdaySun::class)
                ->name('timetable.weekday.sun')->middleware('permission:can_crud_timetable');

            /**Registration*/
            Route::get('/request-quote', \App\Http\Livewire\Registration\RequestQuote::class)
                ->name('requestquote')->middleware('permission:can_request_quote');
            Route::get('/view-quote/{id}', \App\Http\Livewire\Registration\ViewQuote::class)
                ->name('viewquote')->middleware('permission:can_view_quote');
            Route::get('/quotations', \App\Http\Livewire\Registration\Quotations::class)
                ->name('quotations')->middleware('permission:can_view_quotes');
            Route::get('/registrations', \App\Http\Livewire\Registration\Registration::class)
                ->name('registrations')->middleware('permission:can_view_registration');

            /**Settings*/
            Route::get('settings', \App\Http\Livewire\Settings::class)
                ->name('admin.settings')->middleware('permission:can_view_settings');

            /**Online Management*/
            Route::get('assignments', \App\Http\Livewire\Assignment\Assignments::class)
                ->name('admin.assignments')->middleware('permission:can_view_assignments');
            Route::get('assignment-allocations', \App\Http\Livewire\Online\AssignmentAllocations::class)
                ->name('assignment.allocations')->middleware('permission:can_allocate_assignment');
            Route::get('assignment/{id}', \App\Http\Livewire\Online\ViewAssignment::class)
                ->name('view.assignment')->middleware('permission:can_view_assignments');

        });
    });

    /** STUDENT ROUTE PREFIX *//** Livewire template */
    Route::group(['middleware' => ['role:supper-admin|student']], function () {
        Route::prefix('std')->group(function () {
            /*Dashboard*/
            Route::get('/dashboard', \App\Http\Livewire\StudentDashboard::class)->name('student.dashboard');
            Route::get('/request-quote', \App\Http\Livewire\GuestRequestQuote::class)
                ->name('student.requestquote')->middleware('permission:can_request_quote');
            Route::get('/view-quote/{id}', \App\Http\Livewire\StudentViewQuote::class)
                ->name('student.viewquote')->middleware('permission:can_view_quote');
            Route::get('/quotations', \App\Http\Livewire\StudentQuotations::class)
                ->name('student.quotations');

        });
    });

    /** Staff ROUTE PREFIX *//** Livewire template */
    Route::group(['middleware' => ['role:supper-admin|uars']], function () {
        Route::prefix('uars')->group(function () {
            /*Dashboard*/
            Route::get('/dashboard', \App\Http\Livewire\UarsDashboard::class)->name('uars.dashboard');
        });
    });

    /** Staff ROUTE PREFIX *//** Livewire template */
    Route::group(['middleware' => ['role:super-admin']], function () {
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs')
            ->middleware('permission:can_view_system_logs');
    });

});
