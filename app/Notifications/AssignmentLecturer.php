<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AssignmentLecturer extends Notification
{
    use Queueable;

    public $assignment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($assignment)
    {
        $this->assignment = $assignment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'heading' => 'New Assignment Added',
            'message' => $this->assignment->module_code.' '.$this->assignment->service_type.', for student '.(new User)->get_username($this->assignment->lecturer_id).', was allocated to you.',
            'data' => $this->assignment,
        ];
    }


}
