<?php

namespace App\Imports;

use App\Models\Category;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class Import implements ToModel, WithHeadingRow, WithBatchInserts, WithUpserts
{
    public $unique_by;
    public array $fields_to_export;
    public $model;

    public function __construct($fields_to_export, $unique_by, $model){
        $this->unique_by = $unique_by;
        $this->fields_to_export = $fields_to_export;
        $this->model = $model;
    }

    /**
     * @param array $row
     */
    public function model(array $row)
    {
        $array = [];
        foreach($this->fields_to_export as $column => $value){
            $array[$column] = $row[$value];
        }
        return new $this->model($array);
    }

    public function batchSize(): int
    {
        return 500;
    }

    /**
     * If it exists update otherwise insert
     * @return string|array
     */
    public function uniqueBy()
    {
        return $this->unique_by;
    }

}
