<?php

namespace App\Observers;

use App\Models\Module;

class ModulesObserver
{
    /** Update cache */
    public function syncCache(){
        refresh_cached_modules();
    }

    /**
     * Handle the Modules "created" event.
     *
     * @param Module $modules
     * @return void
     */
    public function created(Module $modules)
    {
        $this->syncCache();
    }

    /**
     * Handle the Modules "updated" event.
     *
     * @param Module $modules
     * @return void
     */
    public function updated(Module $modules)
    {
        $this->syncCache();
    }

    /**
     * Handle the Modules "deleted" event.
     *
     * @param Module $modules
     * @return void
     */
    public function deleted(Module $modules)
    {
        $this->syncCache();
    }

    /**
     * Handle the Modules "restored" event.
     *
     * @param Module $modules
     * @return void
     */
    public function restored(Module $modules)
    {
        $this->syncCache();
    }

    /**
     * Handle the Modules "force deleted" event.
     *
     * @param Module $modules
     * @return void
     */
    public function forceDeleted(Module $modules)
    {
        $this->syncCache();
    }
}
