<?php

namespace App\Observers;

use App\Models\ServiceType;

class ServiceTypeObserver
{
    /**
     * Handle the ServiceType "created" event.
     *
     * @param  \App\Models\ServiceType  $serviceType
     * @return void
     */
    public function created(ServiceType $serviceType)
    {
        //
    }

    /**
     * Handle the ServiceType "updated" event.
     *
     * @param  \App\Models\ServiceType  $serviceType
     * @return void
     */
    public function updated(ServiceType $serviceType)
    {
        //
    }

    /**
     * Handle the ServiceType "deleted" event.
     *
     * @param  \App\Models\ServiceType  $serviceType
     * @return void
     */
    public function deleted(ServiceType $serviceType)
    {
        //
    }

    /**
     * Handle the ServiceType "restored" event.
     *
     * @param  \App\Models\ServiceType  $serviceType
     * @return void
     */
    public function restored(ServiceType $serviceType)
    {
        //
    }

    /**
     * Handle the ServiceType "force deleted" event.
     *
     * @param  \App\Models\ServiceType  $serviceType
     * @return void
     */
    public function forceDeleted(ServiceType $serviceType)
    {
        //
    }
}
