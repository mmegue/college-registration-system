<?php

namespace App\Observers;

use App\Models\Setting;
use Illuminate\Support\Facades\Cache;

class SettingsObserver
{
    /**
     * Handle the Setting "created" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function created(Setting $setting)
    {
        $this->syncCache();
    }

    /**
     * Handle the Setting "updated" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function updated(Setting $setting)
    {
        $this->syncCache();
    }

    /**
     * Handle the Setting "deleted" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function deleted(Setting $setting)
    {
        $this->syncCache();
    }

    /**
     * Handle the Setting "restored" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function restored(Setting $setting)
    {
        $this->syncCache();
    }

    /**
     * Handle the Setting "force deleted" event.
     *
     * @param Setting $setting
     * @return void
     */
    public function forceDeleted(Setting $setting)
    {
        $this->syncCache();
    }

    /** Update cache */
    public function syncCache(){
        refresh_cached_settings();
    }
}
