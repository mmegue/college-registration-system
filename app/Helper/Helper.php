<?php

namespace App\Helper;

use App\Http\Traits\Settings;
use App\Models\User;
use Auth;
use Spatie\Permission\Models\Role;

class Helper
{
    use \App\Http\Traits\Helper, Settings;

    const ARRAY = [
        'super-admin','admin','finance',
        'academic','online','assessment',
        'portfolio','marketing','lecturer','uars',
    ];

    public static function hashId($id){
        return self::hsh($id);
    }
    
    public static function all_staff_roles(){
        return Role::whereIn('name',self::ARRAY)->get();
    }

    public static function all_staff(){
        return User::get_users_with_roles(self::ARRAY);
    }

    public static function is_super_admin(){
       return Auth::user()->hasRole('super-admin');
    }

    public static function get_names($id){
        $user = User::find($id);
        if(!is_null($user))
            return $user->name." ".$user->surname;
        return ;
    }


}