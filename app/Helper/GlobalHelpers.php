<?php

use App\Helper\Cached;
use App\Models\Category;
use App\Models\Module;
use App\Models\ServiceType;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

/** Modules caching */
function cached_modules(){
    return Cache::rememberForever('cached_modules', function () {
        return Module::all();
    });
}

function refresh_cached_modules(){
    Cache::forget('cached_modules');
    cached_modules();
}

/** Modules caching */
function cached_categories(){
    return Cache::rememberForever('cached_categories', function () {
        return Category::all();
    });
}

function refresh_cached_categories(){
    Cache::forget('cached_categories');
    cached_categories();
}

/** Settings caching */
function cached_settings(){
    return Cache::rememberForever('cached_settings', function () {
        return Setting::all();
    });
}

function refresh_cached_settings(){
    Cache::forget('cached_settings');
    cached_settings();
}

function get_setting($key){
     return json_decode(
            collect(cached_settings())
                ->firstWhere('key',$key)->value, true);
}

function current_semester(){
   return get_setting('registration')['semester'];
}

/** Service types caching */
function cached_service_types(){
    return Cache::rememberForever('cached_service_types', function () {
        return ServiceType::all();
    });
}

function refresh_cached_service_types(){
    Cache::forget('cached_service_types');
    cached_service_types();
}

/** Cn Helper */
/** Get setting from cached settings files */
function cached_permissions(){
    $id = Auth::id();
    //dd(Cache::get($id.'_cached_permissions'));
    //User::find($id)->getAllPermissions();
    return Cache::rememberForever($id.'_cached_permissions', function () use($id){
        return User::find($id)->getAllPermissions();
     });
}

function cached_roles(){
    $id = session()->get('auth_id');
    return Cache::rememberForever($id.'_cached_roles', function () use($id){
        return User::find($id)->roles()->get();
     });
}

function cn($permission): bool
{
    return collect(cached_permissions())
            ->where('name',$permission)->isNotEmpty();
}

function is_super_admin():bool{
    return (Auth::user()->hasRole('super-admin'));
}

function cached_auth_user_id(){
   return User::get_users_without_roles(['student']);
}

/** Service types caching */
function cached_staff_users(){
    return Cache::rememberForever('cached_staff_users', function () {
        return User::get_users_without_roles(['student']);
    });
}

function refresh_cached_staff_users(){
    Cache::forget('cached_staff_users');
    cached_staff_users();
}

function regenerate_staff_cached_users(){
    refresh_cached_staff_users();
    cached_staff_users();
    refresh_cached_all_users_with_roles();
    refresh_cached_lecturers();
}

function auth_user_has_role($value): bool
{

    return in_array(
        $value,
        cached_roles()->pluck('name')->toArray()
    );
}

/** All users with roles */
function cached_all_users_with_roles(){
   return Cache::rememberForever('cached_all_users_with_roles', function () {
        return collect(User::all());
    });
}

function refresh_cached_all_users_with_roles(){
     Cache::forget('refresh_cached_all_users_with_roles');
     cached_all_users_with_roles();
}

/** lectueres*/
function cached_lecturers()
{
    return Cache::rememberForever('cached_lecturers', function () {
        return User::role('lecturer')->get();
    });
}

function refresh_cached_lecturers(){
     Cache::forget('refresh_cached_lecturers');
     cached_lecturers();
}

function check_url_id($id){
    $key = "url_hashing";
    if(get_setting($key)[$key]){
        return (new \App\Helper\Helper)->hash($id);
    }else{
        return $id;
    }


}



