<?php

namespace App\Providers;

use App\Models\Module;
use App\Models\ServiceType;
use App\Models\Setting;
use App\Observers\ModulesObserver;
use App\Observers\ServiceTypeObserver;
use App\Observers\SettingsObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        'App\Events\Notification'=> [
            'App\Listeners\NotificationListener',
        ],

        'App\Events\ChatEvent'=> [
            'App\Listeners\ChatListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Module::observe(ModulesObserver::class);
        Setting::observe(SettingsObserver::class);
        ServiceType::observe(ServiceTypeObserver::class);
    }
}
