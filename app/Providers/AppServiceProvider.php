<?php

namespace App\Providers;

use App\View\Components\Button;
use App\View\Components\ConfirmModal;
use App\View\Components\DialogModal;
use App\View\Components\Input;
use App\View\Components\InputNoLabel;
use App\View\Components\Modal;
use App\View\Components\Textarea;
use App\View\Components\TextareaNoLabel;
use Hashids\Hashids;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Hashids::class, function () {
            return new Hashids(env('HASHIDS_SALT'), 15);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('input', Input::class);
        Blade::component('textarea', Textarea::class);
        Blade::component('button', Button::class);
        Blade::component('input-no-label', InputNoLabel::class);
        Blade::component('textarea-no-label', TextareaNoLabel::class);
        Blade::component('dialog-modal', DialogModal::class);
        Blade::component('modal', Modal::class);
        Blade::component('confirm-modal', ConfirmModal::class);
    }
}
