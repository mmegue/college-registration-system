<?php

namespace App\Repositories;

use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use App\Models\EmailsAndSmssLog;
use App\Models\QuotationModule;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class StatementsRepository
{
    use Helper, Settings;

    public $from_email, $company_name, $cell;

    public function __construct($id){
        $user = User::find($id);
        $this->cell = $user->cell;
        $this->from_email = $this->contact()->from_email;
        $this->company_name = $this->contact()->company_name;
        //dd($this->cell);
    }

    public function data_statement(){

        $quote_m = QuotationModule::join('quotations','quotations.id','=','quotation_modules.quotation_id')
            ->join('users','users.id','=','quotation_modules.added_by')
            ->where('quotation_modules.status','Invoiced')
            ->where('quotation_modules.cell',$this->cell)
            ->where('quotation_modules.semester',$this->semester())
            ->select('users.username','quotation_modules.*','quotations.quotation_number','quotations.invoice_number')->get();
        $transactions = Transaction::whereCellNumber($this->cell)->where('semester_period', $this->semester());
        return [
            'quotation_details' => User::whereCell($this->cell)->first(),
            'quotation_modules' => $quote_m,
            'total' =>  $quote_m->sum('price'),
            'transactions' => $transactions,
            'statement_number' => "S".$this->cell
        ];
    }

    public function download_statement(){
        $this->continue('can_download_statement');
        $data = $this->data_statement();
        return response()->streamDownload(function () use($data){
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('livewire.registration.statement-template',$data);
            echo $pdf->stream();
        }, 'S-'.$data['quotation_details']['cell'].'.pdf');
    }

    public function email_statement(){
        $this->continue('can_email_statement');
        if(Setting::get('send_emails')==="1" && cn('can_email_statement')){
            $data = $this->data_statement();
            Mail::send('emails.statement-mail', $data, function($message) use ($data){
                //libxml_use_internal_errors(true);
                $pdf = PDF::loadView('livewire.registration.statement-template',$data);
                //libxml_use_internal_errors(false);
                $message->to($data['quotation_details']['email'],$data['quotation_details']['name']." ".$data['quotation_details']['surname'])
                    ->subject('Statement# '.$data['timestamp'].' For '.$data['quotation_details']['title'].' '.$data['quotation_details']['surname']);
                $message->from($this->from_email,$this->company_name);
                $message->attachData($pdf->output(), 'S-'.$data['quotation_details']['cell'].'.pdf');
            });
            /** Send a copy to Info2 for records purposes */
            Mail::send('emails.statement-mail', $data, function($message) use ($data){
                //libxml_use_internal_errors(true);
                $pdf = PDF::loadView('livewire.registration.statement-template',$data);
                //libxml_use_internal_errors(false);
                $message->to($this->from_email,"-Statement Email Copy")
                    ->subject('Statement# '.$data['timestamp'].' For '.$data['quotation_details']['title'].' '.$data['quotation_details']['surname']);
                $message->from($this->from_email,$this->company_name." Web Application");
                $message->attachData($pdf->output(), 'Statement-'.$data['timestamp'].'.pdf');
            });

            EmailsAndSmssLog::create([
                'cell_number' => $data['quotation_details']['cell'],
                'sent_by' => Auth::id(),
                'subject' => 'Statement Report For:'.$data['quotation_details']['cell'],
                'message' => 'Statement Report For:'.$data['quotation_details']['cell'].' email was sent successfully to: '.$data['quotation_details']['email'],
                'log_type' => 'Email',
            ]);

            $this->success('Statement successfully Emailed to '.$data['quotation_details']['email']);
        }else{
            $data = $this->data_statement();
            Mail::send('emails.statement-mail', $data, function($message) use ($data){
                //libxml_use_internal_errors(true);
                $pdf = PDF::loadView('livewire.registration.statement-template',$data);
                //libxml_use_internal_errors(false);
                $message->to($this->from_email,"Testing Purposes")
                    ->subject('Statement Report:Email Sending is DISABLED');
                $message->from($this->from_email,$this->company_name);
                $message->attachData($pdf->output(), 'S-'.$data['quotation_details']['cell'].'.pdf');
            });
            $this->success('Email was not send to the recipient because, Sending of emails is currently disabled by the admin.');
        }

    }
}