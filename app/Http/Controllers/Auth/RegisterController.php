<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     * protected $redirectTo = RouteServiceProvider::HOME;
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'cell' => ['required', 'numeric','unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'title' => $data['title'],
            'name' => $data['name'],
            'surname' => $data['surname'],
            'cell' => $data['cell'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        $user->assignRole('student');
        return $user;
    }

    /**
     * Redirect users after a successfull login
     *
     * @return string
     */
    public function redirectTo()
    {
        $roles = Auth::user()->getRoleNames();

        switch($roles[0]){

            case 'super-admin';
            case 'admin';
            case 'finance';
            case 'academic';
            case 'online';
            case 'assessment';
            case 'portfolio';
            case 'marketing';
            case 'lecturer';
                $this->redirectTo = RouteServiceProvider::ADMIN;
                return $this->redirectTo;
                break;
            case 'uars';
                $this->redirectTo = RouteServiceProvider::UARS;
                return $this->redirectTo;
                break;
            case 'student';
                $this->redirectTo = RouteServiceProvider::STUDENT;
                return $this->redirectTo;
                break;

            default:
                $this->redirectTo = '/login';
                return $this->redirectTo;
        }
    }
}
