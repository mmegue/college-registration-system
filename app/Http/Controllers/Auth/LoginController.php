<?php

namespace App\Http\Controllers\Auth;

use App\Helper\Cached;
use App\Http\Controllers\Controller;
use App\Observers\SettingsObserver;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public $id;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    protected function authenticated(Request $request, $user)
    {
        $this->id = Auth::id();
        /** Cache items */
        Cache::forget($this->id.'_cached_permissions');
        Cache::forever($this->id.'_cached_permissions', cached_permissions());

        Cache::forget($this->id.'_cached_roles');
        Cache::forever($this->id.'_cached_roles', cached_permissions());

        /**Sessions*/
        //$request->session()->put('auth_id',Auth::id());
        session()->put('auth_id',$this->id);
    }

    protected function loggedOut(Request $request)
    {
        Cache::forget($this->id.'_cached_permissions');
        Cache::forget($this->id.'_cached_roles');
    }

    /**
     * Redirect users after a successfull login
     *
     * @return string
     */
    public function redirectTo()
    {
        $roles = Auth::user()->getRoleNames();

        switch($roles[0]){

            case 'super-admin';
            case 'admin';
            case 'finance';
            case 'academic';
            case 'online';
            case 'assessment';
            case 'portfolio';
            case 'marketing';
            case 'lecturer';
                $this->redirectTo = RouteServiceProvider::ADMIN;
                return $this->redirectTo;
                break;
            case 'uars';
                $this->redirectTo = RouteServiceProvider::UARS;
                return $this->redirectTo;
                break;
            case 'student';
                $this->redirectTo = RouteServiceProvider::STUDENT;
                return $this->redirectTo;
                break;

            default:
                $this->redirectTo = '/login';
                return $this->redirectTo;
        }
    }
}
