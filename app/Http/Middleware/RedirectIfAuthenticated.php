<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                $auth = Auth::user();
                if(
                    $auth->hasRole('super-admin') ||
                    $auth->hasRole('upper-admin') ||
                    $auth->hasRole('admin') ||
                    $auth->hasRole('lecturer')
                ){
                    return redirect(RouteServiceProvider::ADMIN);
                }

                if(Auth::user()->hasRole('uars')){
                    return redirect(RouteServiceProvider::UARS);
                }

                if(Auth::user()->hasRole('student')){
                    return redirect(RouteServiceProvider::STUDENT);
                }

            }
        }

        return $next($request);
    }
}
