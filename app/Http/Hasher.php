<?php

namespace App\Http;
use App\Http\Traits\Helper;
use Hashids\Hashids;

class Hasher
{
    use Helper;

    public static function encode(...$args)
    {
        return app(Hashids::class)->encode(...$args);
    }

    public function get_error(){
        return $this->get_error();
    }

    public static function decode($enc)
    {
        if (is_int($enc)) {
            return $enc;
        }

        if(empty(app(Hashids::class)->decode($enc))){
            abort(404);
        }

        return app(Hashids::class)->decode($enc)[0];
    }
}