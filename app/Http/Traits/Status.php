<?php

namespace App\Http\Traits;

use App\Models\Setting;

trait Status
{
    public static function payment_status(){
        return [
            'Paid','Pending','Refunded','Cancelled'
        ];
    }

    public static function payment_methods(){
        return [
            'EFT','Deposit','Cash','Instant','Pep','Shoprite'
        ];
    }

    public static function payment_banks(){
        return [
            'FNB','ABSA','Capitec','Standard','TymeBank','Nedbank','Other'
        ];
    }

    public static function titles(){
        return [
            'Mr','Mrs','Miss','Ms',
        ];
    }

    public static function assignment_status(){
        return [
            'Used','Not Used'
        ];
    }

    public function quotation_status(){
        return ['Quotation','Invoiced'];
    }

    public static function semesters(){
        return explode(",", get_setting('registration')['semesters']);
    }

    public static function attendance(){
        return ['Weekend Evening','Weekend Day','Midweek Evening','Midweek Day'];
    }

    public static function gl_acc(){
        return [
            'FEES','PACKS','EXAM'
        ];
    }

    public static function campus(){
        return [
            'CAMPUS-0NE','HRE-CBD'
        ];
    }
}
