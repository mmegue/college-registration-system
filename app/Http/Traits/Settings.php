<?php

namespace App\Http\Traits;

use App\Models\Setting;

trait Settings
{

    public function semester(){
        return $this->registration()->semester;
    }

    public function tax_period(){
        return $this->registration()->tax_period;
    }

    public function allocation_period(){
        return $this->registration()->allocation_period;
    }

    public function get_reg_fee(){
        return $this->registration()->reg_fee;
    }

    public function address(){
        return Setting::get('address');
    }

    public function contact(){
        return Setting::get('contact');
    }

    public function registration(){
        return Setting::get('registration');
    }

    public function banking(){
        return Setting::get('banking');
    }
}