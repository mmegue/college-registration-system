<?php


namespace App\Http\Traits;


use App\Helper\Permissions;
use App\Models\Category;
use App\Models\GeneratedClassListLog;
use App\Models\InvoiceGenerator;
use App\Models\Module;
use App\Models\MyDefaultModule;
use App\Models\Pricing;
use App\Models\Quotation;
use App\Models\QuotationModule;
use App\Models\QuoteGenerator;
use App\Models\ServiceType;
use App\Models\Setting;
use App\Http\Hasher;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Auth;


trait Helper
{

    public function auth_error(){
        $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }

    public function log_classlist_generated($data){
        GeneratedClassListLog::create([
            'initiated_by' => $data['initiated_by'],
            'details' => $data['details'],
        ]);
    }

    public function class($priority){
        if($priority === "High"){ return "danger"; }
        if($priority === "Medium"){ return "warning"; }
        if($priority === "Low"){ return "primary"; }
        return "";
    }

    public function delete_row($id){
        DB::transaction(function() use($id){
            $quotation_module = QuotationModule::find($id);
            /** Update the delete by column first */
            $quotation_module->update(['deleted_by'=>Auth::id()]);
            /** delete the records */
            $quotation_module->delete();

            $this->emit('refresh');
        });
    }

    public function or_continue(array $permissions)
    {
        foreach ($permissions as $permission){
            /** Continue if true is reached*/
            if(cn($permission)) {
                break;
            }else{
                abort(403);
            }
        }

    }

    public function and_continue(array $permissions)
    {
        foreach ($permissions as $permission){
            $this->continue($permission);
        }
    }

    public function continue($permission)
    {
       /** If doesn't have permission emit the error message*/
        if(! cn($permission)){
            abort(403);
        }
    }

    /** URL HASHING */
    public function hash($id){
       return self::hsh($id);
    }

    public static function hsh($id){
        return (get_setting('url_hashing') )
            ? Hasher::encode($id) : $id;
    }

    public function hash_decode($id){
       return Hasher::decode($id);
    }

    public function check_url_id($id){
        return (get_setting('url_hashing'))
            ? $this->hash_decode($id) : $id;
    }

    /** END URL HASHING */

    public static function get_checkbox_setting($key){
        return Setting::get_checkbox_setting($key);
    }

    /**Check System Settings*/
    public static function check($key){
        return Setting::check($key);
    }

    public function success($message){
         $this->dispatchBrowserEvent('swal', [
            'title' => '<p style="color:white">'.$message.'</p>',
            'timer'=>6000,
            'icon'=>'success',
            'toast'=>true,
            'position'=>'top-right',
            'showCancelButton' => false,
            'showConfirmButton' => false,
            'background' => '#28a745',
        ]);
    }

    public function error($message){
         $this->dispatchBrowserEvent('swal', [
            'title' => '<p style="color:#000">'.$message.'</p>',
            'timer'=>6000,
            'icon'=>'danger',
            'toast'=>true,
            'position'=>'top-right',
            'showCancelButton' => false,
            'showConfirmButton' => false,
            'background' => '#ff0a16',
        ]);
    }

    public function str_format($string){
        return trim(ucwords(strtolower($string)));
    }

    public function getAttendanceProperty(){
        return \App\Http\Traits\Status::attendance();
    }

    public function category_groups(){
        return Category::with('modules');
    }

    public function getCategoriesProperty(){
        return cached_categories();
    }
    
    public function getModulesProperty(){
        return cached_modules();
    }

    public function getUsersProperty(){
        return cached_staff_users();
    }

     public function getServicetypesProperty(){
        return cached_service_types();
    }


    public function getCategorygroupsProperty(){
        return $this->category_groups()->get();
    }

    public function cell_format($cell){
        return preg_replace("/^0/", "27", $cell);
    }

    public function users(){
        return cached_staff_users();
    }

    public function year_d(){
        return date('Ymd');
    }

    public function generate_quote_number(){

        DB::transaction(function() use(&$model){
            //DB::select(DB::raw('LOCK TABLES quote_generators WRITE'));
            $model = QuoteGenerator::create(['current_date'=>date('Ymd')]);
            //DB::select(DB::raw('UNLOCK TABLES'));
        });

        if($model->id<=9){
           return $this->year_d()."000".$model->id;
        }elseif($model->id>=10 && $model->id<=99){
           return $this->year_d()."00".$model->id;
        }elseif($model->id>=100 && $model->id<=999){
           return $this->year_d()."0".$model->id;
        }else{
           return $this->year_d().$model->id;
        } 
    }

    public function generate_invoice_number(){

        DB::transaction(function() use(&$model){
            //DB::select(DB::raw('LOCK TABLES invoice_generators WRITE'));
            $model = InvoiceGenerator::create(['current_date'=>date('Ymd')]);
            //DB::select(DB::raw('UNLOCK TABLES'));
        });

        if($model->id<=9){
           return $this->year_d()."000".$model->id;
        }elseif($model->id>=10 && $model->id<=99){
           return $this->year_d()."00".$model->id;
        }elseif($model->id>=100 && $model->id<=999){
           return $this->year_d()."0".$model->id;
        }else{
           return $this->year_d().$model->id;
        }
    }

}