<?php

namespace App\Http\Livewire\Modules;

use App\Exports\Export;
use App\Http\Traits\Helper;
use App\Imports\Import;
use App\Models\Category;
use Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class Module extends Component
{
    use Helper, WithFileUploads;

    public $page_title = 'Modules';

    public $module_code, $category_id, $description ;
    public $create, $edit, $download, $upload, $show = false;
    public $form_state = "table";
    public $m_id, $file;

    protected $rules=[
        'module_code' => 'required|max:10',
        'category_id' => 'required',
        'description' => 'required|max:150',
    ];

    protected $listeners = [ 'showEdit' => 'show_edit' ];

    public function render()
    {
        $this->or_continue(['can_view_masterdata', 'can_crud_masterdata']);
        return view('livewire.modules.module');
    }

    public function show_edit($id,\App\Models\Module $module){
        $this->reset_booleans();
        $this->edit = true;
        $this->m_id = $id;
        $module = \App\Models\Module:: find($this->m_id);
        $this->module_code = $module->module_code;
        $this->category_id = $module->category_id;
        $this->description = $module->description;
    }

    public function show_create(){
        $this->reset_booleans();
        $this->create = true;
    }

    public function show_upload(){
        $this->reset_booleans();
        $this->upload = true;
    }

    public function create_module(){
        $this->continue('can_crud_masterdata');
        $validated_data = $this->validate();
        $validated_data['created_by'] = Auth::id();
        \App\Models\Module::create($validated_data);
        $this->reset();
        $this->emit('refreshTable');
        $this->success("Module Successfully created.");
    }

    public function update_module(){
        $this->continue('can_crud_masterdata');
        $validated_data = $this->validate();
        \App\Models\Module::find($this->m_id)->update($validated_data);
        $this->reset();
        $this->emit('refreshTable');
        $this->success("Module Successfully updated.");
    }

    public function reset_booleans(){
        $this->create = false; $this->edit = false;$this->upload = false;
    }

    /************** EXCEL OPERATION FILES ***************/

    public function show_upload_modal(){
        $this->show = true;
    }

    public function export_collection(){
        return \App\Models\Module::all();
    }

    public function export_headings(){
        return [
            'id',
            'module_code',
            'description',
            'category_id',
            'created_by',
            'deleted_at',
            'created_at',
            'updated_at',
        ];
    }

    public function unique_by(){
        return 'module_code';
    }

    public function model(){
        return new \App\Models\Module();
    }

    public function import_column_fields(){
        return [
            'id' =>'id',
            'module_code'=>'module_code',
            'description'=>'description',
            'category_id'=>'category_id',
            'created_by'=>'created_by',
        ];
    }

    public function file_name(){
        return "Modules";
    }

    public function download_file(){
        $this->continue('can_export_masterdata');
        return Excel::download(
            new Export($this->export_collection(), $this->export_headings()),
            $this->file_name().'.xlsx'
        );
    }

    /* xport Model data to excel */
    public function upload_file(){
        $this->continue('can_import_masterdata');
        $this->validate([
            'file' => 'required|max:50000|mimes:xlsx', // 1MB Max
        ]);
        /*Import data*/
        Excel::import(
            new Import(
                $this->import_column_fields(),
                $this->unique_by(),
                $this->model()
            ),
            $this->file
        );
        /*Close the modal box*/
        $this->redirect('modules');
        $this->success('File Imported Successfully!!!');
    }

}
