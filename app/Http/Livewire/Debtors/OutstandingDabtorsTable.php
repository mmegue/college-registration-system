<?php

namespace App\Http\Livewire\Debtors;

use App\Http\Traits\Helper;
use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class OutstandingDabtorsTable extends LivewireDatatable
{

    use Helper;

    public $hideable = 'select';
    public $exportable = true;

    public function builder()
    {
        $this->continue('can_view_outstanding_debtors');
        return User::role('student');
    }

    public function columns()
    {
        return [
            Column::name('quotation_modules_.price:sum')->label('Invoiced'),
            Column::name('transactions.amount:sum')->label('Paid'),

            Column::name('users.title')->label('Title'),
            Column::name('users.name')->label('Name')->searchable()->filterable(),
            Column::name('users.surname')->label('Surname')->searchable()->filterable(),
            Column::name('users.email')->label('Email')->searchable()->filterable(),
            Column::name('users.cell')->label('Cell')->searchable()->filterable(),

        ];
    }

}