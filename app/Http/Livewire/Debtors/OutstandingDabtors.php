<?php

namespace App\Http\Livewire\Debtors;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class OutstandingDabtors extends Component
{
    public $page_title = "Outstanding Debtors";

    public function render()
    {
        return view('livewire.debtors.outstanding-dabtors');
    }

    public function dummy(){
        User::query()->select(DB::RAW('sum(transactions.amount) as totalPaid'),
            DB::RAW('sum(quotation_modules.price) as totalInvoiced'),
            DB::RAW('users.title, users.name, users.surname, users.cell, users.email'))
            ->join('quotations','quotations.user_id','=','users.id')
            ->join('quotation_modules','quotations.id','=','quotation_modules.quotation_id')
            ->join('transactions','transactions.cell_number','=','users.cell')
            ->groupBy('users.id','quotations.id')
            ->get()->dd();

        User::role('student')->with([
            'quotations_' => function($query){$query->where('quotations.status','Invoiced');},
            'quotation_modules_'=> function($query){$query->where('quotation_modules.status','Invoiced');},
            'transactions',

        ])->select('users.*')
            ->withSum('quotation_modules_ as totalInvoiced', 'price')
            ->withSum('transactions as totalPaid', 'amount')->get()->toArray();
    }
}
