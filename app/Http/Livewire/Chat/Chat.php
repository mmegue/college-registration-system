<?php

namespace App\Http\Livewire\Chat;

use App\Events\ChatEvent;
use App\Http\Traits\Helper;
use App\Models\User;
use App\Models\Chat as Chaat;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Chat extends Component
{
    use Helper;
    public $page_title= "Chat";
    public $message;
    public $contact_id = null;
    public $contact_details;
    public $time;

    protected $listeners = ['sendMessagex' => 'sendMessage'];

    public function mount(){
        $this->contact_details =
            (is_null($this->contact_id))
                ? null :User::find($this->contact_id);
    }

    public function render()
    {
        return view('livewire.chat.chat');
    }

    public function sendMessage(){
        Chaat::create([
            'from_user' => Auth::id(),
            'to_user' => $this->contact_id,
            'message'  => $this->message,
            'unread_to' => $this->contact_id,
        ]);

    }

    public function count_unread($from_user){
       return $this->chats_for_count($from_user)->count();
    }

    public function chats_for_count($from_user){
        return Chaat::where('from_user',$from_user)->Where('to_user',Auth::id())
        ->where('unread_to',Auth::id());
    }


    public function chats($from_user){
        return Chaat::where(function($query) use($from_user) {
            $query->where('from_user',$from_user)
                ->where('to_user',Auth::id());
        })->orWhere(function($query) use($from_user){
            $query->where('from_user',Auth::id())
                ->where('to_user',$from_user);
        });

    }

    public function mark_all_as_read($from_user){
        $chats = $this->chats($from_user);
        //mark as read all messages from contactid to auth
        if($chats->exists()){
            $chats->update(['unread_to'=>0]);
        }
    }

    public function get_user_last_seen($from_user){
        return optional(Chaat::where('from_user',$from_user)
            ->Where('to_user',Auth::id())
            ->latest()->first())
            ->created_at;
    }

    public function fetch_contacts(){
        $users = User::with('roles')->get();
        return $users->reject(function ($user, $key) {
            return $user->hasRole(['student','lecturer','super-admin','finance','academic','online','assessment','portfolio','marketing','lecturer','uars']);
        });
    }

    public function fetchMessages($from_user){

        $data = $this->chats($from_user)
            ->latest()
            ->limit(10)->get()->reverse()->values();
        $array = collect($data->toArray())->last();
        $this->time = (is_null($array))? null : date('F j, g:i a');
        return $data;
    }

    public function set_id($contact_id){
        $this->contact_id = $contact_id;
        $this->contact_details =  User::find($this->contact_id);
        $this->message = "";
        $this->mark_all_as_read($contact_id);
    }
}
