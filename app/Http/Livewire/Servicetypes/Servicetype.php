<?php

namespace App\Http\Livewire\ServiceTypes;

use App\Exports\Export;
use App\Http\Traits\Helper;
use App\Imports\Import;
use Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;
use Str;

class ServiceType extends Component
{
    use Helper, WithFileUploads;

    public $name, $status, $slug, $created_by;
    public $page_title = 'Service Types';
    public $create, $edit, $download, $upload, $show = false;
    public $form_state = "table";
    public $m_id, $file;

    protected $rules=[
        'name' => 'required|max:50',
    ];

    protected $listeners = [ 'showEdit' => 'show_edit' ];



    public function render()
    {
        $this->or_continue(['can_view_masterdata', 'can_crud_masterdata']);
        return view('livewire.service-types.service-type');
    }

    public function show_edit($id,\App\Models\ServiceType $service_type){
        $this->reset_booleans();
        $this->edit = true;
        $this->m_id = $id;
        $service_type = \App\Models\ServiceType:: find($this->m_id);
        $this->name = $service_type->name;
    }

    public function show_create(){
        $this->reset_booleans();
        $this->create = true;
    }

    public function show_upload(){
        $this->reset_booleans();
        $this->upload = true;
    }

    public function create_service(){
        $this->continue('can_crud_masterdata');
        $this->validate();
        \App\Models\ServiceType::create([
            'name' => Str::upper($this->name),
            'slug' => Str::slug($this->name),
            'created_by' => Auth::id(),
        ]);
        $this->reset();
        $this->emit('refreshTable');
        $this->success("Service Successfully Created.");
    }

    public function update_service(){
        $this->continue('can_crud_masterdata');
        $validated_data = $this->validate();
        \App\Models\ServiceType::find($this->m_id)->update(
            [
                'name' => Str::upper($this->name),
                'slug' => Str::slug($this->name),
            ]
        );
        $this->reset();
        $this->emit('refreshTable');
        $this->success("Service Type Successfully Updated.");
    }

    public function reset_booleans(){
        $this->create = false; $this->edit = false;$this->upload = false;
    }

    /************** EXCEL OPERATION FILES ***************/

    public function show_upload_modal(){
        $this->show = true;
    }

    public function export_collection(){
        return \App\Models\ServiceType::all();
    }

    public function export_headings(){
        return [
            'id',
            'name',
            'status',
            'slug',
            'created_by',
            'deleted_at',
            'created_at',
            'updated_at',
        ];
    }

    public function unique_by(){
        return 'slug';
    }

    public function model(){
        return new \App\Models\ServiceType();
    }

    public function import_column_fields(){
        return [
            'id' =>'id',
            'name'=>'name',
            'status'=>'status',
            'slug'=>'slug',
            'created_by'=>'created_by',
        ];
    }

    public function file_name(){
        return "ServiceTypes";
    }

    public function download_file(){
        $this->continue('can_export_masterdata');
        return Excel::download(
            new Export($this->export_collection(), $this->export_headings()),
            $this->file_name().'.xlsx'
        );
    }

    /* xport Model data to excel */
    public function upload_file(){
        $this->continue('can_import_masterdata');
        $this->validate([
            'file' => 'required|max:50000|mimes:xlsx', // 1MB Max
        ]);
        /*Import data*/
        Excel::import(
            new Import(
                $this->import_column_fields(),
                $this->unique_by(),
                $this->model()
            ),
            $this->file
        );
        /*Close the modal box*/
        $this->reset();
        $this->success('File Imported Successfully!!!');
    }

}
