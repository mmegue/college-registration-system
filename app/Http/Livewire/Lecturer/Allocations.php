<?php

namespace App\Http\Livewire\Lecturer;

use App\Models\User;
use Livewire\Component;

class Allocations extends Component
{
    public $page_title = "Lecturer Allocations";
    public $modules = [];

    protected $listeners = ['openModal'];

    public function render()
    {
        return view('livewire.lecturer.allocations');
    }

    public function get_lecturers()
    {
        return User::role('lecturer')->get();
    }
}
