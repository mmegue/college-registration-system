<?php

namespace App\Http\Livewire\Lecturer;

use App\Http\Traits\Helper;
use App\Models\LecturerAllocation;
use App\Models\User;
use Auth;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class AllocationsTable extends LivewireDatatable
{
    use Helper;
    /*** Datatable Configurations ***/
    public $model = LecturerAllocation::class;
    public $hideable = 'select';
    public $exportable = false;

    /*** Model id ***/
    public $model_id;
    public $default = 0;
    public $module_code;
    public $modules = [];


    /*** Edit class variables ***/
    public $showModalAllocation = false;

    protected $listeners = ['refresh' => '$refresh'];

    public function builder()
    {
        return User::query()->role('lecturer');
    }
    
    public function columns()
    {
        return [

            Column::callback(['id'], function ($id) {
                return view('livewire.lecturer.lecturer-allocation', [
                    'id' => $id,
                    'showModalAllocation' => $this->showModalAllocation,
                    'modules' => $this->modules
                ]);
            }),

            Column::name('users.name')
                ->label('Name')
                ->filterable()
                ->searchable(),

            Column::name('users.surname')
                ->label('Surname')
                ->filterable()
                ->searchable(),

            Column::name('users.cell')
                ->label('Cell Number')
                ->filterable()
                ->searchable(),

            Column::name('users.username')
                ->label('Username')
                ->filterable()
                ->searchable(),

            Column::name('users.email')
                ->label('Email Address')
                ->filterable(),

        ];
    }

    public function open_allocation_module($id){
        $this->modules = LecturerAllocation::where('lecturer_id',$id)->get();
        $this->model_id = $id;
        $this->showModalAllocation = true;
    }

    public function save_allocation(){
        $this->validate(['module_code'=>'required']);
        $does_the_module_have_default = LecturerAllocation::where('default',1)->where('module_code',$this->module_code)->exists();
        $does_the_module_marked_default_for_the_user = LecturerAllocation::where('lecturer_id',$this->model_id)->where('module_code',$this->module_code)
            ->where('default',1)->exists();
        $does_module_allocated_to_the_lecturer = LecturerAllocation::where('lecturer_id',$this->model_id)->where('module_code',$this->module_code)->exists();

        if($this->default){

            if($does_the_module_have_default){
                $this->success('Module:'.$this->module_code.' already allocated as a default to another lecturer.');
            }elseif($does_the_module_marked_default_for_the_user){
                $this->success('Module:'.$this->module_code.' already allocated as a default to this lecturer.');
            }elseif($does_module_allocated_to_the_lecturer){
                $this->success('Module:'.$this->module_code.' already allocated to this lecturer.');
            }
            else{

                LecturerAllocation::create([
                    'lecturer_id' =>  $this->model_id,
                    'module_code' => $this->module_code,
                    'default' => $this->default,
                    'allocated_by' => Auth::id(),
                ]);
                $this->module_code = "";
                $this->default = 0;

                $this->success('Module:'.$this->module_code.' allocated successfully.Refresh the page if not appearing.');

            }

        }
        else{
            if($does_module_allocated_to_the_lecturer){
                $this->success('Module:'.$this->module_code.' already allocated to this lecturer.');
            }else{
                LecturerAllocation::create([
                    'lecturer_id' =>  $this->model_id,
                    'module_code' => $this->module_code,
                    'default' => $this->default,
                    'allocated_by' => Auth::id(),
                ]);
                $this->module_code = "";
                $this->default = 0;

                $this->success('Module:'.$this->module_code.' allocated successfully.Refresh the page if not appearing.');
            }
        }

    }

    public function delete_allocation($id){
        LecturerAllocation::find($id)->delete();
    }
}