<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UarsDashboard extends Component
{
    public function render()
    {
        return view('livewire.uars-dashboard');
    }
}
