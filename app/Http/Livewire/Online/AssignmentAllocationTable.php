<?php

namespace App\Http\Livewire\Online;

use App\Http\Livewire\BulkAllocation\BulkAllocationTable;
use App\Http\Traits\Helper;
use App\Models\Assignment;
use App\Models\LecturerAllocation;
use App\Models\Pricing;
use App\Models\QuotationModule;
use App\Models\User;
use App\Notifications\AssignmentLecturer;
use DB;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class AssignmentAllocationTable extends LivewireDatatable
{
    use Helper;
    public $hideable = 'select';
    public $lecturer;
    public $showModalAllocation = [];
    public $module_code,$service_type,$lecturer_id, $computed_service_types = [], $computed_lecturers = [];

    public function open_allocation_lecturer($id){
        //$this->module_code = $module_code;
    }

    public function updatedModuleCode($value){
        $this->computed_service_types = Pricing::serviceTypesPerModule($value);
        $this->computed_lecturers = LecturerAllocation::whereModuleCode($value)->get();
    }

    public function builder()
    {
        return (new QuotationModule)->AllQuoteModulesWithAssociatedData();
    }

    public function columns()
    {
        return [

            Column::callback(['quotation_modules.id','module_code','service_type','assignment_lecturer_allocation'],
                function ($id,$module_code,$service_type,$assignment_lecturer_allocation) {
                    return view('livewire.assignment.allocation-modal',[
                        'assignments' => Assignment::whereQuotationModulesId($id)->get(),
                        'computed_service_types' => Pricing::serviceTypesPerModule($module_code),
                        'computed_lecturers' => LecturerAllocation::whereModuleCode($module_code)->get(),
                        'module_code' => $module_code,'service_type' => $service_type,'id'=>$id,'assignment_lecturer_allocation'=>$assignment_lecturer_allocation
                    ]);
                })->label('Allocated To'),

            Column::name('cell')->label('Student Cell')->searchable()->filterable(),
            Column::name('module_code')->label('Module Code')->searchable()->filterable($this->modules->pluck('module_code')),
            Column::name('service_type')->label('Service Type')->searchable()->filterable($this->servicetypes->pluck('slug')),
            Column::name('assignment_lecturer_allocation')->label('Allocation')->searchable()->filterable([true,false]),
            Column::callback(['quotation_modules.id'],
                function ($id) {
                    //muass cheka kuti mune quote id here
                    $check = Assignment::whereQuotationModulesId($id)->exists();
                    return ($check)? '<span class="text-success"><strong>Allocated</strong></span>' :
                        '<span class="text-danger"><strong>UnAllocated</strong></span>';
                })->label('Allocated'),
        ];
    }

    public function add_assignment_to_lecturer($quotation_modules_id){

        collect($this->selected)->each(function ($data) use($quotation_modules_id){
            $this->allocate_lecturer($quotation_modules_id, $this->lecturer_id, $data);
        });
    }


    public function remove_allocation($id){
        DB::transaction(function() use($id){
            $quote_module = QuotationModule::find($id);
            $quote_module->update([
                'assignment_lecturer_allocation' => null,
                'assignment_id' => null,
            ]);
            Assignment::whereQuotationModulesId($id)->delete();
        });
    }

    public function allocate_lecturer($quotation_modules_id, $lecturer_id, $service_type){
        if($lecturer_id === "" || is_null($lecturer_id ) || $service_type === "" || is_null($service_type)){
           $this->error('Lecturer or Service Type Is Empty!');
        }else{
            DB::transaction(function() use($quotation_modules_id, $lecturer_id, $service_type) {
                $quote_module = QuotationModule::find($quotation_modules_id);
                $quote_module->update([
                    'assignment_lecturer_allocation' => $lecturer_id,
                ]);
                $assignment = Assignment::create([
                    'lecturer_id' =>$lecturer_id,
                    'student_id' =>$quote_module->user->id,
                    'quotation_modules_id' => $quotation_modules_id,
                    'module_code' =>$quote_module->module_code,
                    'assignment_type' => 'answers',
                    'service_type' =>$service_type,
                ]);
                $quote_module->update([
                    'assignment_id' => $assignment->id,
                ]);

                User::find($lecturer_id)->notify(new AssignmentLecturer($assignment));
                $this->reset(['service_type']);
                $this->success('Assignment(s) allocated successfully..');
            });
        }
    }
}