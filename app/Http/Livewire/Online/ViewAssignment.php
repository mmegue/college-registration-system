<?php

namespace App\Http\Livewire\Online;

use App\Http\Traits\Helper;
use App\Models\Assignment;
use App\Models\User;
use App\Notifications\AssignmentStudent;
use Illuminate\Support\Facades\Notification;
use Livewire\Component;

class ViewAssignment extends Component
{
    use Helper;
    public $page_title = "View Assignment";
    public $view_id, $update_assignment = false;
    public $assignment, $lecturer_note,$assignment_model, $student_name, $lecturer_name, $student_username, $status;


    public function mount($id){
        $this->view_id = $this->check_url_id($id);
        $this->assignment_model = Assignment::find($this->view_id);
        $this->lecturer_name = (new User)->get_username($this->assignment_model->lecturer_id);
        $this->student_username = (new User)->get_username($this->assignment_model->student_id);
        $this->student_name = (new User)->get_fullnames($this->assignment_model->student_id);
        $this->status = $this->assignment_model->status === "Completed";
    }

    public function render()
    {
        return view('livewire.online.view-assignment');
    }

    public function updatedStatus($state){
       if($state){
           $this->assignment_model ->update([
               'status' =>'Completed'
           ]);
           $this->success('Assignment Status Successfully Changed.');
       }else{
           $this->assignment_model ->update([
               'status' =>'Pending'
           ]);
           $this->success('Assignment Status Successfully Changed.');
       }
    }

    public function add_lecturer_note(){
        $this->validate(['lecturer_note' => 'required']);
        $this->assignment_model ->update([
            'lecturer_note'=>$this->lecturer_note
        ]);
        $this->success('Note Saved Successfully.');
    }

    public function publish_assignment(){
        $this->validate(['assignment' => 'required']);
        $this->assignment_model ->update([
            'assignment'=>$this->assignment,
            'status' =>'Completed'
        ]);
        $this->redirectRoute('view.assignment',$this->view_id);
        $this->success('Assignment successfully published to student with status completed.');
    }

    public function save_to_draft(){
        $this->validate(['assignment' => 'required']);
        $this->assignment_model ->update([
            'assignment'=>$this->assignment,
            'status' =>'Draft'
        ]);
        $this->success('Saved to draft.');
    }

}
