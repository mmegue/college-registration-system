<?php

namespace App\Http\Livewire\Online;

use App\Models\LecturerAllocation;
use App\Models\Pricing;
use Livewire\Component;

class AssignmentAllocations extends Component
{
    public $page_title = "Assignment Allocations";
    public $module_code,$service_type,$lecturer_id, $computed_service_types = [], $computed_lecturers = [];

    public function render()
    {
        return view('livewire.online.assignment-allocations');
    }

    public function updatedModuleCode($value){
        $this->computed_service_types = Pricing::serviceTypesPerModule($value);
        $this->computed_lecturers = LecturerAllocation::whereModuleCode($value)->get();
    }


}
