<?php

namespace App\Http\Livewire;

use App\Events\Test;
use App\Helper\Cached;
use App\Helper\Permissions;
use App\Http\Traits\Helper;
use App\Models\Setting;
use App\Models\User;
use App\Observers\SettingsObserver;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;

class Settings extends Component
{
    use Helper;

    public Setting $setting;
    public $address1,$address2,$city,$state,$zip,$country;
    public $company_name,$cell_1,$cell_2,$cell_3,$email, $from_email;
    public $currency_symbol, $reg_fee, $semester,$semesters, $allocation_period, $tax_period ;
    public $slack_notifications, $sms_notifications, $email_notifications, $url_hashing, $unauthorised_error_message;
    public $bank_name,$account_name,$account_number,$account_type,$branch_code;
    public $slack_webhook_prod,$slack_webhook_sandbox;

    public $canupdate;

    public function mount(){
        Setting::all()->each(function($value){
            $array = json_decode($value->value);
            foreach($array as $key => $value){
                $this->$key = $value;
            }
        });
        $this->canupdate = auth()->user()->can('can_update_settings');
    }

    public function render()
    {
        return view('livewire.settings',[
            'page_title'=>'Settings',
            'contact' => Setting::check("contact"),
            'address' => Setting::check("address"),
            'registration' => Setting::check("registration"),
            'banking' => Setting::check("banking"),
        ]);
    }

    public function update_checkbox($key){
        if ($this->canupdate) {
            $array = [ $key => $this->$key ];
            if(Setting::check($key)){
                Setting::edit($key,$array);
            } else{
                Setting::store($key,$array);
            }
            return $this->success("Saved!");
        }
        event(new Test($key));
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }

    public function update($key){
        if ($this->canupdate) {
            $array = $this->validate([
                $key => 'required'
            ]);
            if(Setting::check($key)){
                Setting::edit($key,$array);
                return $this->success("Updated Successfully!");
            } else{
                Setting::store($key,$array);
                return $this->success("Saved Successfully!");
            }

        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));

    }


    /**REGISTRATION*/
    public function registration_validation() {
        return $this->validate([
            'currency_symbol' => 'required',
            'reg_fee' => 'required|numeric',
            'semester' => 'required|max:10',
            'semesters' => 'required',
            'allocation_period' => 'required',
            'tax_period' => 'required',
        ]);
    }

    public function create_registration(){
        if ($this->canupdate) {
            Setting::store('registration',$this->registration_validation());
            return $this->success("Registration settings successfully saved!");
        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }

    public function update_registration($key){
        if ($this->canupdate) {
            Setting::edit($key, $this->registration_validation());
            return $this->success("Registration settings successfully updated!");
        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }

    /** BANKING DETAILS */
    public function update_banking_details($key) {
        $validated_data = $this->validate([
            'bank_name' => 'required|max:50',
            'account_name' => 'required|max:50',
            'account_number' => 'required|numeric',
            'account_type' => 'required|max:50',
            'branch_code' => 'required|numeric',
        ]);

        if ($this->canupdate) {
            Setting::edit($key, $validated_data);
            return $this->success("Banking Details Successfully Updated!");
        }
    }

    /** BANKING DETAILS */
    public function update_communication() {
        $validated_data = $this->validate([
            'slack_webhook_prod' => 'nullable',
            'slack_webhook_sandbox' => 'nullable',
        ]);

        if ($this->canupdate) {
            Setting::edit('communication', $validated_data);
            return $this->success("Communication Settings Successfully Updated!");
        }
    }

    /**CONTACT*/
    public function contact_validation() {
        return $this->validate([
            'company_name' => 'required|max:100',
            'cell_1' => 'required|max:100',
            'cell_2' => 'required',
            'cell_3' => 'required',
            'email' => 'required',
            'from_email' => 'required',
        ]);
    }

    public function create_contact(){
        if ($this->canupdate) {
            Setting::store('contact', $this->contact_validation());
            return $this->success("Contact successfully saved!");
        }
    }

    public function update_contact($key){
        if ($this->canupdate) {
            Setting::edit($key, $this->contact_validation());
            return $this->success("Contact successfully updated!");
        }
    }

    /**ADDRESS*/
    public function address_validation() {
        return $this->validate([
            'address1' => 'required|max:100',
            'address2' => 'required|max:100',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'country' => 'required',
        ]);
    }

    public function create_address(){
        if ($this->canupdate) {
            Setting::store('address', $this->address_validation());
            return $this->success("Address successfully saved!");
        }
    }

    public function update_address($key){
        if ($this->canupdate) {
            Setting::edit($key, $this->address_validation());
            return $this->success("Address successfully updated!");
        }
    }

}
