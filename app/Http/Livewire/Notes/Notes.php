<?php

namespace App\Http\Livewire\Notes;

use App\Http\Traits\Helper;
use App\Models\Note;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Notes extends Component
{
    use Helper, WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $page_title = "Notes";
    public $user_id;
    public $title;
    public $description;
    public $tag;
    public $state = 'all';

    public $add_note = false;

    public function render()
    {
        return view('livewire.notes.notes');
    }

    public function my_notes(){
        if($this->state === "personal" || $this->state === "social" || $this->state === "important" || $this->state === "school"){
            return Note::get_tag($this->state);
        }

        if($this->state === "favourite"){
            return Note::get_fav();
        }

        return Note::all_notes();
    }

    public function get_state($state){
        if($state === 'all'){ $this->state = "all";}
        if($state == 'favourite'){ $this->state = "favourite"; }
        if($state === 'personal'){ $this->state = "personal";}
        if($state === 'school'){ $this->state = "school";}
        if($state === 'social'){ $this->state = "social";}
        if($state === 'important'){ $this->state = "important";}

    }

    public function assign_tag($id, $tag){
        $note =  Note::find($id);
        $note->tag = $tag;
        $note->save();
    }

    public function favourite_action($id){
        $note =  Note::find($id);
        if(Note::where('favourite',1)->where('id',$id)->exists()){
            $note->favourite = false;
        }else{
            $note->favourite = true;
        }
        $note->save();
    }

    public function delete_note($id){
        Note::destroy($id);
        $this->success("Note Successfully Removed!");
    }

    public function add_note(){
        $validated_data = $this->validate([
            'title' => 'required|max:30',
            'description' => 'required|max:100',
        ]);
        $validated_data['user_id'] = Auth::id();
        $note = Note::create($validated_data);
        $this->reset();
        $this->add_note = false;
        $this->success($note->title. "Created Successfully!");
    }
}
