<?php

namespace App\Http\Livewire;

use App\Models\Quotation;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class StudentQuotations extends Component
{
    public $page_title = "Quotations";

    public function render()
    {
        return view('livewire.student-quotations');
    }

    public function qi($status){
        return Quotation::where('user_id', Auth::id())
            ->where('status',$status)->count();
    }
}
