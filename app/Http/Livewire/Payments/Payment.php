<?php

namespace App\Http\Livewire\Payments;

use Livewire\Component;

class Payment extends Component
{
    public $page_title = 'Payments';

    public function render()
    {
        return view('livewire.payments.payment');
    }
}
