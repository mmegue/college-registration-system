<?php

namespace App\Http\Livewire\Pricings;

use App\Exports\Export;
use App\Http\Traits\Helper;
use App\Imports\Import;
use App\Models\Category;
use App\Models\Module;
use Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class Pricing extends Component
{
    use Helper, WithFileUploads;

    public $page_title = 'Pricing';

    public $module_code, $service_type, $price ;
    public $create, $edit, $download, $upload, $show = false;
    public $form_state = "table";
    public $m_id, $file;

    protected $rules=[
        'module_code' => 'required|max:10',
        'service_type' => 'required|string',
        'price' => 'required|numeric',
    ];

    protected $listeners = [ 'showEdit' => 'show_edit' ];

    public function render()
    {
        $this->or_continue(['can_view_masterdata', 'can_crud_masterdata']);
        return view('livewire.pricings.pricing');
    }

    public function show_edit($id,\App\Models\Pricing $pricing){
        $this->reset_booleans();
        $this->edit = true;
        $this->m_id = $id;
        $pricing = \App\Models\Pricing:: find($this->m_id);
        $this->module_code = $pricing->module_code;
        $this->service_type = $pricing->service_type;
        $this->price = $pricing->price;
    }

    public function show_create(){
        $this->reset_booleans();
        $this->create = true;
    }

    public function show_upload(){
        $this->reset_booleans();
        $this->upload = true;
    }

    public function create_pricing(){
        $this->continue('can_crud_masterdata');
        $validated_data = $this->validate();
        $validated_data['created_by'] = Auth::id();
        \App\Models\Pricing::create($validated_data);
        $this->reset();
        $this->emit('refreshTable');
        $this->success("Pricing Successfully created.");
    }

    public function update_pricing(){
        $this->continue('can_crud_masterdata');
        $validated_data = $this->validate();
        \App\Models\Pricing::find($this->m_id)->update($validated_data);
        $this->reset();
        $this->emit('refreshTable');
        $this->success("Pricing Successfully updated.");
    }

    public function reset_booleans(){
        $this->create = false; $this->edit = false;$this->upload = false;
    }

    /************** EXCEL OPERATION FILES ***************/

    public function show_upload_modal(){
        $this->show = true;
    }

    public function export_collection(){
        return \App\Models\Pricing::all();
    }

    public function export_headings(){
        return [
            'id',
            'module_code',
            'service_type',
            'price',
            'created_by',
            'deleted_at',
            'created_at',
            'updated_at',
        ];
    }

    public function unique_by(){
        return 'module_code';
    }

    public function model(){
        return new \App\Models\Pricing();
    }

    public function import_column_fields(){
        return [
            'id' =>'id',
            'module_code'=>'module_code',
            'service_type'=>'service_type',
            'price'=>'price',
            'created_by'=>'created_by',
        ];
    }

    public function file_name(){
        return "Pricing";
    }

    public function download_file(){
        $this->continue('can_export_masterdata');
        return Excel::download(
            new Export($this->export_collection(), $this->export_headings()),
            $this->file_name().'.xlsx'
        );
    }

    /* xport Model data to excel */
    public function upload_file(){
        $this->continue('can_import_masterdata');
        $this->validate([
            'file' => 'required|max:50000|mimes:xlsx', // 1MB Max
        ]);
        /*Import data*/
        Excel::import(
            new Import(
                $this->import_column_fields(),
                $this->unique_by(),
                $this->model()
            ),
            $this->file
        );
        /*Close the modal box*/
        $this->redirect('pricing');
        $this->success('File Imported Successfully!!!');
    }
}
