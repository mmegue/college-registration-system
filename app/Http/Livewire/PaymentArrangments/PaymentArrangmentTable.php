<?php

namespace App\Http\Livewire\PaymentArrangments;

use App\Http\Traits\Helper;
use App\Http\Traits\Status;
use App\Models\PaymentArrangment;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class PaymentArrangmentTable extends LivewireDatatable
{
    use Status, Helper;

    /*** Datatable Configurations ***/
    public $hideable = 'select';
    public $exportable = true;

    public $when,$how_much,$send_statement, $status, $approval, $pa_id;
    protected $listeners = ['refresh'=>'$refresh'];

    public function builder()
    {
        return PaymentArrangment::query()
            ->join('quotations','quotations.id','=','payment_arrangments.quotation_id')
            ->join('users','users.id','=','quotations.user_id');
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->filterable()->defaultSort('desc')->hide(),

            Column::callback(['id'],
                function ($id) {
                    return view('livewire.payment-arrangments.components.edit-payment-arrangements',[
                        'id' => $id,
                    ]);
                })->label('Action'),

            DateColumn::name('when')->label('PaymentDate')->filterable(),
            Column::name('how_much')->label('Amount')->filterable(),
            Column::name('status')->label('Status')->filterable(),
            Column::name('send_statement')->label('Statements')->filterable(),
             Column::callback(['approval'], function ($approval) {
                 return (!$approval) ? '<span class="text-danger font-weight-bold">Pending</span>' : '<span class="font-weight-bold text-success">Approved</span>';
            })->label('Approval')->alignCenter(),
            Column::name('quotations.quotation_number')->label('Quote#')->filterable()->searchable(),
            Column::name('quotations.invoice_number')->label('Invoice#')->filterable()->searchable(),
            Column::name('users.name')->label('Name')->filterable()->searchable(),
            Column::name('users.surname')->label('Surname')->filterable()->searchable(),
            Column::name('users.cell')->label('Cell')->filterable()->searchable(),
            Column::name('users.email')->label('Email')->filterable()->searchable(),
            DateColumn::name('created_at')->filterable()->hide(),
            DateColumn::name('updated_at')->filterable()->hide(),
        ];
    }

    public function getStatusProperty(){
        return $this->quotation_status();
    }

    public function open_edit_payment_arrangement_form($id){
        $this->pa_id = $id;
        $model = PaymentArrangment::find($id);
        $this->when = $model->when;
        $this->how_much = $model->how_much;
        $this->send_statement = $model->send_statement;
        $this->status = $model->status;
        $this->approval = $model->approval;
    }

    public function update_payment_arrangement($id){
        $this->validate([
            "when" => "required",
            "how_much" => "required",
        ]);
       $array=[
            "when" => $this->when,
            "how_much" => $this->how_much,
            "send_statement" => $this->send_statement,
            "status" => $this->status,
            "approval" => $this->approval,
       ];

       PaymentArrangment::find($id)->update($array);
       $this->success('Payment arrangement successfully updated.');
       $this->emit('refresh');
    }
}