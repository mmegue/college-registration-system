<?php

namespace App\Http\Livewire\PaymentArrangments;

use Livewire\Component;

class PaymentArrangments extends Component
{
    public $page_title = 'Payment Arrangement';
    
    public function render()
    {
        return view('livewire.payment-arrangments.payment-arrangments');
    }
}
