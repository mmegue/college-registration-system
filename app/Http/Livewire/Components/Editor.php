<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;

class Editor extends Component
{
    public function render()
    {
        return view('livewire.components.editor');
    }
}
