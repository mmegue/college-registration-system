<?php

namespace App\Http\Livewire\Components;

use App\Models\Chat;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Livewire\Component;

class Navigation extends Component
{
    use Notifiable;
    
    public $notifications, $lecturer_assignments_notifications;
    public $auth, $chats;
    public $count_notifications;

    protected $listeners = ['refreshNav'=>'$refresh'];

    public function mount(){
        $this->auth = Auth::user();
        $this->notifications =  Auth::user()->unreadNotifications;
        $this->chats = Chat::all_chats_for_count()->count();
        $count = 0;
        foreach($this->notifications as $not){ $count++; }
        $this->count_notifications = ($count == 0)? "" :$count;
    }

    public function render()
    {
        return view('livewire.components.navigation');
    }

    public function mark_as_read($notification){
        $this->notifications->where('id',$notification['id'])
        ->markAsRead() ;
        $this->notifications =  Auth::user()->unreadNotifications;
        $this->emit('refreshNav');
    }

    public function mark_all_as_read(){
        foreach ($this->notifications as $notification) {
            $notification->markAsRead();
        }
       $this->notifications =  Auth::user()->unreadNotifications;
        $this->emit('refreshNav');
    }

    public function chat(){
        return $this->redirectRoute('chat');
    }
}
