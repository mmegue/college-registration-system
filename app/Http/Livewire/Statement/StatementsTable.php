<?php

namespace App\Http\Livewire\Statement;

use App\Http\Livewire\Registration\ViewQuote;
use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use App\Models\Quotation;
use App\Models\QuotationModule;
use App\Models\User;
use App\Repositories\StatementsRepository;
use Auth;
use DB;
use Illuminate\Support\Facades\App;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class StatementsTable extends LivewireDatatable
{
    use Helper, Settings;
    public $hideable = "select";
    
    public function builder()
    {
        $this->continue('can_view_statements');
        return User::role('student');
    }

    public function columns()
    {
        return [
            Column::callback(['id','cell'], function ($id) {
                //$user = User::find($id);

                return view('livewire.statement.options',compact('id'));
            }),

            Column::callback(['cell',], function ($cell) {
                return optional(QuotationModule::where('status','Invoiced')
                    ->selectRaw('GROUP_CONCAT(DISTINCT(quotation_modules.module_code)) as modules')
                    ->groupBy('quotation_modules.cell')->where('quotation_modules.cell',$cell)->first())->modules;
            })->label('Modules')->searchable()->filterable(),
            
            Column::name('name')->label('Name')->searchable(),
            Column::name('surname')->label('Surname')->searchable(),
            Column::name('cell')->label('Cell#')->searchable(),
            Column::name('email')->label('Email')->searchable(),
        ];
    }

    public function download_statement($id){
        $this->continue('can_export_statement');
        return (new StatementsRepository($id))->download_statement();
    }

    public function email_statement($id){
        $this->continue('can_email_statement');
        (new StatementsRepository($id))->email_statement();
    }
}