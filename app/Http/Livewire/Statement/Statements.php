<?php

namespace App\Http\Livewire\Statement;

use Livewire\Component;

class Statements extends Component
{
    public $page_title = "Statements";

    public function render()
    {
        return view('livewire.statement.statements');
    }
}
