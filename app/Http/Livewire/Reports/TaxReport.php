<?php

namespace App\Http\Livewire\Reports;

use Livewire\Component;

class TaxReport extends Component
{
    public $page_title = 'Tax Reports';

    public function render()
    {
        return view('livewire.reports.tax-report');
    }
}
