<?php

namespace App\Http\Livewire\Reports;

use Livewire\Component;

class Report extends Component
{
    public $page_title = 'Reports';

    public function render()
    {
        return view('livewire.reports.report');
    }
}
