<?php

namespace App\Http\Livewire\Transaction;

use App\Http\Traits\Helper;
use App\Models\Quotation;
use App\Models\Transaction;
use App\Models\User;
use Livewire\Component;

class ViewTransaction extends Component
{
    use Helper;

    public $modelId;
    public $page_title = "View Transaction";

    public function mount($id){
        $this->modelId = $this->check_url_id($id);
    }

    public function render()
    {
        $this->continue('can_view_transactions');
        $model = Transaction::find($this->modelId);
        $transactions = Transaction::where('cell_number', $model->cell_number)->get();
        $quotation =optional(Quotation::join('users','users.id','=','quotations.user_id')
            ->where('users.cell',$model->cell_number))->first();
        return view('livewire.transaction.view-transaction',[
            'transactions' => $transactions,
            'transaction' => $model,
            'quotation' => $quotation,
        ]);
    }
}
