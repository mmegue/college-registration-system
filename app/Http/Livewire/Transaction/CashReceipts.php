<?php

namespace App\Http\Livewire\Transaction;

use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use App\Models\Transaction;
use Auth;
use Carbon\Carbon;
use Livewire\Component;

class CashReceipts extends Component
{
    use Helper, Settings;
    public $page_title = "Cash Receipts";

    public $amount,$gl_account,$campus,$short_description, $cell_number;

    protected $rules = [
        'amount'=>'required|integer',
        'gl_account'=>'required',
        'campus'=>'required',
        'cell_number'=>'required|min:10|numeric',
    ];

    protected $listeners = ['refresh' => '$refresh'];

    public function render()
    {
        $this->continue('can_view_cash_receipts');
        return view('livewire.transaction.cash-receipts');
    }
                            
    public function create_cash_receipt(){
        $this->continue('can_crud_cash_receipts');
        Transaction::create([
            'user_id'=>Auth::id(),
            'amount'=>$this->amount,
            'item'=>$this->cell_number,
            'gl_account'=>$this->gl_account,
            'campus'=>$this->campus,
            'transaction_date'=>Carbon::now(),
            'transaction_type'=>'Cash Receipt',
            'fs_accounts'=> 'Sales',
            'tax_period'=> $this->tax_period(),
            'allocation_period' => $this->allocation_period(),
            'short_description'=>$this->short_description,
            'receipt_number'=>'RCT-'.date('YmdHis'),
            'semester_period'=>$this->semester(),
            'internal_transaction_number'=>'T'.date('YmdHis'),
            'cell_number'=>$this->cell_format($this->cell_number),
        ]);
        $this->emit('refresh');
        $this->success('Receipts created successfully');
        $this->reset();
    }
}
