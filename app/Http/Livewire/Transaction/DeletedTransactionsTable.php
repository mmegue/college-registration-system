<?php

namespace App\Http\Livewire\Transaction;

use App\Http\Traits\Helper;
use App\Models\TempTransaction;
use App\Models\Transaction;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class DeletedTransactionsTable extends LivewireDatatable
{
    use Helper;

    /*** Datatable Configurations ***/
    public $hideable = 'select';
    public $exportable = false;

    public function builder()
    {
        return TempTransaction::query()->leftjoin('users','users.id','=','temp_transactions.user_id')
            ->distinct('temp_transactions.id');
    }

    public function columns()
    {
        return[
            Column::callback(['temp_transactions.id'], function ($id) {
                return '<a href="view-transaction/'.$this->hash($id).'" class="btn btn-sm btn-secondary">View Transaction</a>';
            }),
            NumberColumn::name('temp_transactions.id')->filterable()->defaultSort('desc')->hide(),
            Column::name('users.username')->label('Created By')->filterable()->searchable(),
            NumberColumn::name('amount')->label('Amount'),
            Column::name('item')->label('Item')->filterable(),
            Column::name('gl_account')->label('GL Account')->filterable(),
            Column::name('campus')->label('Campus')->filterable(),
            Column::name('transaction_date')->label('Trans date')->filterable()->searchable(),
            Column::name('transaction_type')->label('Trans Type')->filterable()->searchable(),
            Column::name('fs_accounts')->label('FS Account')->filterable(),
            Column::name('tax_period')->label('TAX Period')->filterable(),
            Column::name('allocation_period')->label('Allocation Period')->filterable(),
            Column::name('short_description')->label('Description'),
            Column::name('receipt_number')->label('Receipt #')->filterable(),
            Column::name('semester_period')->label('Semester')->filterable(),
            Column::name('internal_transaction_number')->label('Internal #')->filterable()->searchable(),
            Column::name('cell_number')->label('Cell Number')->filterable()->searchable(),
            Column::name('invoice_number')->label('Invoice #')->filterable()->searchable(),
//            Column::name('quotations.name')->label('Client Name')->filterable(),
//            Column::name('quotations.cell')->label('Client Cell')->filterable(),
//            Column::name('quotations.email')->label('Client Email')->filterable(),
        ];
    }
}