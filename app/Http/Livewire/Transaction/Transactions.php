<?php

namespace App\Http\Livewire\Transaction;

use App\Exports\Export;
use App\Http\Traits\Helper;
use App\Imports\Import;
use App\Models\TempTransaction;
use App\Models\Transaction;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class Transactions extends Component
{
    use Helper, WithFileUploads;
    public $page_title = "Transactions";

    public $create, $edit, $download, $upload, $show = false;
    public $form_state = "table";
    public $m_id, $file;

    public function render()
    {
        $this->continue('can_view_transactions');
        return view('livewire.transaction.transactions');
    }

    public function truncate_transactions(){
        $this->continue('can_crud_transactions');
        Transaction::all()
            ->each(function ($all_trans) {
                $newTrans = $all_trans->replicate();
                $newTrans ->setTable('temp_transactions');
                $newTrans ->save();
                /**Delete from the transtable*/
                $all_trans->delete();
            });
        $this->success('Successfully trancated the transactions');
        $this->emit('refresh');
    }

    public function restore_all_truncate_transactions(){
        $this->continue('can_crud_transactions');
        TempTransaction::all()
            ->each(function ($all_trans) {
                $newTrans = $all_trans->replicate();
                $newTrans ->setTable('transactions');
                $newTrans ->save();
                /**Delete from the transtable*/
                $all_trans->delete();
            });
        $this->emit('refresh');
        $this->success('Transactions successfully restored!');
    }

    /************** EXCEL OPERATION FILES ***************/

    public function show_upload_modal(){
        $this->show = true;
    }

    public function show_upload(){
        $this->upload = true;
    }

    public function export_collection(){
        $this->continue('can_export_transactions');
        return \App\Models\Transaction::all();
    }

    public function export_headings(){
        return [
            'id',
            'user_id',
            'amount',
            'item',
            'gl_account',
            'campus',
            'transaction_date',
            'transaction_type',
            'fs_accounts',
            'tax_period',
            'allocation_period',
            'short_description',
            'receipt_number',
            'semester_period',
            'internal_transaction_number',
            'cell_number',
            'invoice_number',
        ];
    }

    public function unique_by(){
        return 'internal_transaction_number';
    }

    public function model(){
        return new \App\Models\Transaction();
    }

    public function import_column_fields(){

        return [
            'id' => 'id',
            'user_id' => 'user_id',
            'amount' => 'amount',
            'item' => 'item',
            'gl_account' => 'gl_account',
            'campus' => 'campus',
            'transaction_date' => 'transaction_date',
            'transaction_type' => 'transaction_type',
            'fs_accounts' => 'fs_accounts',
            'tax_period' => 'tax_period',
            'allocation_period' => 'allocation_period',
            'short_description' => 'short_description',
            'receipt_number' => 'receipt_number',
            'semester_period' => 'semester_period',
            'internal_transaction_number' => 'internal_transaction_number',
            'cell_number' => 'cell_number',
            'invoice_number' => 'invoice_number',
        ];
    }

    public function file_name(){
        return "Transactions";
    }

    public function download_file(){
        $this->continue('can_export_transactions');
        return Excel::download(
            new Export($this->export_collection(), $this->export_headings()),
            $this->file_name().'.xlsx'
        );
    }

    /* xport Model data to excel */
    public function upload_file(){
        $this->continue('can_import_transactions');
        $this->validate([
            'file' => 'required|max:50000|mimes:xlsx', // 1MB Max
        ]);
        /*Import data*/
        Excel::import(
            new Import(
                $this->import_column_fields(),
                $this->unique_by(),
                $this->model()
            ),
            $this->file
        );
        /*Close the modal box*/
        $this->redirect('transactions');
        $this->success('File Imported Successfully!!!');
    }
}
