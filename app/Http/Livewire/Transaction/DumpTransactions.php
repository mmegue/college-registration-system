<?php

namespace App\Http\Livewire\Transaction;

use App\Http\Traits\Helper;
use App\Models\Transaction;
use Exception;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class DumpTransactions extends Component
{
    use Helper;
    public $page_title = "Dump Transactions";
    public $transactions;
    protected $listeners = ['refresh'=>'$refresh'];
    
    public function render()
    {
        $this->continue('can_dump_transactions');
        return view('livewire.transaction.dump-transactions');
    }

    public function dump_transactions(){
        $this->continue('can_dump_transactions');
        $count = 0;
        $update = 0;
        try {
            $lines = explode("\n", $this->transactions);
            foreach($lines as $line) {
                $tr = explode(';',$line)[19];
                $model = Transaction::where('internal_transaction_number',$tr);
                $exists = $model->exists();
                list(
                    $timestamp,
                    $user_id,
                    $item,
                    $amount,
                    $gl_account,
                    $campus,
                    $dot,
                    $student_number,
                    $transaction_type,
                    $fs_accounts,
                    $select_device_charged,
                    $staff_email,
                    $tax_period,
                    $allocation_period,
                    $short_description,
                    $x_dummy_surname_name,
                    $receipt_number,
                    $transperiod,
                    $x_dummy_transaction_entry,
                    $internal_transaction_number,
                    $x_dummy_student_number,
                    $cell_number

                    ) = explode(";", $line);
                $array = [
                    'user_id'=>Auth::id(),
                    'amount'=>$amount,
                    'item'=>$item,
                    'gl_account'=>$gl_account,
                    'campus'=>$campus,
                    'transaction_date'=>$dot,
                    'transaction_type'=>$transaction_type,
                    'fs_accounts'=> $fs_accounts,
                    'tax_period'=>$tax_period,
                    'allocation_period' => $allocation_period,
                    'short_description'=>$short_description,
                    'receipt_number'=>$receipt_number,
                    'semester_period'=>$transperiod,
                    'internal_transaction_number'=>$internal_transaction_number,
                    'cell_number'=>$cell_number,
                    //'invoice_number'=>$invoice_number,
                ];
                if(!$exists){
                    Transaction::create($array);
                    $count = $count+1;
                } else{
                    $model->update($array);
                    $update = $update+1;
                }
            }
            $this->transactions = "";
            $this->emitSelf('refresh');
            $this->success( $count.' of '.count($lines).' transactions created. '.$update.' Updated successfully');
        } catch (Exception $e) {
            $this->error($e);
        }
    }
}
