<?php

namespace App\Http\Livewire\Transaction;

use App\Http\Traits\Helper;
use App\Models\TempTransaction;
use App\Models\Transaction;
use Livewire\Component;

class DeletedTransactions extends Component
{
    use Helper;
    public $page_title = "Deleted Transactions";
    protected $listeners = ['refresh' => '$refresh'];

    public function render()
    {
        $this->continue('can_view_deleted_transactions');
        return view('livewire.transaction.deleted-transactions');
    }

    public function truncate_transactions(){
        $this->continue('can_crud_transactions');
        TempTransaction::truncate();
        $this->emit('refresh');
        $this->success('Deleted Transactions Successfully truncated');

    }
}
