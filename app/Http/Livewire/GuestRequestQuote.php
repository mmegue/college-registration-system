<?php

namespace App\Http\Livewire;

use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use App\Models\CompanyDetail;
use App\Models\MyDefaultModule;
use App\Models\Pricing;
use App\Models\Quotation;
use App\Models\QuotationModule;
use App\Models\TempQuotation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class GuestRequestQuote extends Component
{
    use Helper, Settings;

    public $page_title = 'Request Quote';

    public $listeners = ['refresh' => '$refresh'];

    public $check_box_module, $check_temp_quote_existance;
    public $module, $regfee, $companies=[];
    public $temp_service_type_update=[], $temp_attendance_update=[],$temp_module_code_update=[];
    public $service_type, $service_types=[], $quote_modules=[], $is_disabled=false;
    public $company_details = false,$cell_form = true, $choose_modules = false, $personal_info = false ;

    public $username, $title, $name, $surname, $email, $cell;
    public $company_name, $address_1, $address_2, $suburb, $city, $company_cell, $company_email, $vat_number, $note;

    public function mount(){
        //$this->reset_all();
        $this->cell  = auth()->user()->cell;
        $model = optional(User::whereCell($this->cell)->first());
        $this->username=$model->username;
        $this->title=$model->title;
        $this->name=$model->name;
        $this->surname=$model->surname;
        $this->email=$model->email;
    }

    public function render()
    {
        return view('livewire.guest-request-quote')
            ->layout('layouts.app');
    }

    public function updatedCompanies(){

    }

    public function updatedCell(){
        if(substr($this->cell, 0, 1) == '0'){
            $this->cell = substr_replace($this->cell,'27',0,($this->cell[0]=='0'));
        }

        $this->quote_modules = $this->get_quote_modules();
        $model = optional(User::whereCell($this->cell)->first());
        $this->username=$model->username;
        $this->title=$model->title;
        $this->name=$model->name;
        $this->surname=$model->surname;
        $this->email=$model->email;

    }

    public function open_choose_modules(){
        $this->validate(['cell'=>'required|integer|digits_between:10,12']);
        $this->check_session();
        $this->emit('refresh');
        $this->emitSelf('refresh');
    }

    public function get_quote_modules(){

        return (new QuotationModule)
            ->allWithUserAndQuotataion($this->cell)
            ->where('quotations.status','Quotation')->get();
    }

    public function get_client_quotations(){
        return $this->user_instance()->quotations;
    }

    public function user_instance(){
        return optional(User::where('cell',$this->cell)->first());
    }

    public function reset_all(){
        $this->destroy_session();
        $this->redirect('request-quote');
    }

    /**Check if there is a sessions*/
    private function check_session(){
        if(!session()->has('cell_key')){
            session(['cell_key' => $this->cell]);
        }
    }

    /**Destroy session */
    public function destroy_session(){
        session()->forget('cell_key');
    }

    /**Get session */
    private function get_session(){
        return  session('cell_key');
    }

    public function open_cell_form(){
        $this->close_all();
        $this->cell_form = true;
    }

    public function open_personal_info(){
        if(Auth::user()->tempquotations()->count() > 0){
            $this->close_all();
            $this->personal_info = true;
        }else{
            $this->error('Choose at least 1 module to proceed..');
        }
    }

    public function total_temp(){
        return TempQuotation::total($this->cell) + $this->regfee;
    }

    public function create_quotation(){
        $this->validate([
            'title' => 'required',
            'username' => 'nullable',
            'name' => 'required|string|max:50',
            'surname' => 'required|string|max:50',
            'cell' => 'required|integer|digits_between:10,14',
            'email' => 'required|email',
            'note' => 'max:150|nullable',

            'company_name' => 'string|nullable',
            'address_1' => 'string|nullable',
            'address_2' => 'string|nullable',
            'suburb' => 'string|nullable',
            'city' => 'string|nullable',
            'company_cell' => 'integer|digits_between:10,14|nullable',
            'company_email' => 'email|nullable',
            'vat_number' => 'max:25|nullable',
        ]);
        $quote_number = $this->generate_quote_number();
        /** Perform a series of operations incase of failer it will roll back*/
        DB::transaction(function() use ($quote_number){

            /**Create user and assign a role*/
            $user = User::updateOrCreate(
                ['cell' => $this->cell],
                [
                    'title' => $this->str_format($this->title),
                    'username' => $this->str_format($this->username),
                    'name' => $this->str_format($this->name),
                    'surname' => $this->str_format($this->surname),
                    'cell' => $this->cell_format($this->cell),
                    'email' => trim(strtolower($this->email)),
                    'password' => Hash::make($this->cell_format($this->cell)),

                ]
            );
            if(!$user->hasRole('student')){
                $user->assignRole('student');
            }

            /**Create Quotation*/
            $quotation = Quotation::create([
                'user_id' => $user->id,
                'quotation_number' => $quote_number,
                'semester' => $this->semester(),
                'created_by'=> Auth::id(),
                'note' => $this->str_format($this->note),
            ]);

            /**Create Company Details*/
            CompanyDetail::create([
                'user_id' => $user->id,
                'company_name' => $this->str_format($this->company_name),
                'address_1' => $this->str_format($this->address_1),
                'address_2' => $this->str_format($this->address_2),
                'suburb' => $this->str_format($this->suburb),
                'city' => $this->str_format($this->city),
                'company_cell' => trim($this->cell_format($this->company_cell)),
                'company_email' => trim($this->company_email),
                'vat_number' => trim($this->vat_number),
            ]);

            /**Get quotation Modules from the Temp table*/
            TempQuotation::tempQuoteModule($this->cell)
                ->each(function($temp_quote) use($quotation){
                    /**Move the temp module to the quotation modules*/
                    QuotationModule::create([
                        'quotation_id' => $quotation->id,
                        'module_code' => $temp_quote->module_code,
                        'service_type' => $temp_quote->service_type,
                        'price' => $temp_quote->price,
                        'cell' => $temp_quote->cell,
                        'semester' => $this->semester(),
                        //'lecturer_id_default' => $temp_quote->lecturer_id_default,
                        //'lecturer_id_alt' => $temp_quote->lecturer_id_alt,
                        'added_by' => $temp_quote->added_by,
                    ]);
                });
            /*After a successful transfer clear the temp table*/
            TempQuotation::tempQuoteModule($this->cell)->each(function($query){
                $query->delete();
            });

            /**Clearing session to accommodate a new number*/
            $this->destroy_session();
            $this->redirectRoute('student.viewquote',$this->hash($quotation->id));
        });

    }

    public function updatedCompanyDetails($state){
        $this->company_details = (bool)$state;
    }

    public function updatedTempAttendanceUpdate($state){
        $id = array_key_first($this->temp_attendance_update);
        TempQuotation::find($id)->update([
            'attendance'=>$this->temp_attendance_update[$id]
        ]);
        //$this->emit('refresh');
        //$this->emitSelf('attendanceUpdate');
        //$this->redirect('request-quote');
    }

    public function updatedTempServiceTypeUpdate($state){
        $id = array_key_first($this->temp_service_type_update);
        $temp_quote = TempQuotation::find($id);
        $temp_quote->update([
            'service_type' => $this->temp_service_type_update[$id],
            'price' =>  Pricing::price($temp_quote->module_code,$this->temp_service_type_update[$id])
        ]);
        //$this->emit('refresh');
    }

    public function updatedTempModuleCodeUpdate($state){
        $id = array_key_first($this->temp_module_code_update);
        $temp_quote = TempQuotation::find($id);
        $temp_quote->update([
            'module_code' => $state,
            'price' =>  Pricing::price($this->temp_module_code_update[$id], $temp_quote->service_type,)
        ]);
        //$this->emit('refresh');
    }

    public function updatedRegfee($state){
        if($state){
            TempQuotation::create([
                'module_code'=>"REGFEE",
                'service_type'=>'reg-fee',
                'price'=>$this->get_reg_fee(),
                'cell'=>$this->cell,
                'added_by'=>Auth::id(),
            ]);
        }else{
            TempQuotation::regfee($this->cell)->delete();
        }
        $this->emit('refresh');
    }

    public function delete_temp($id){
        TempQuotation::destroy($id);
        //$this->redirect('request-quote');
    }

    public function updatedPaidCash()
    {
        if($this->paid_cash)
            $this->isDisabled = true;
        else
            $this->isDisabled = false;
    }

    public function my_categories(){
        return MyDefaultModule::mydefaultCategories();
    }

    public function get_my_default_category_modules($category_id){
        return MyDefaultModule::mydefaultCategoryModules($category_id)->get();
    }

    public function get_temp_quote_modules($cell){
        return TempQuotation::tempQuoteModule($cell)
            ->where('added_by',Auth::id())->get();
    }

    /**
     * Get service types on module input update
     */
    public function updatedModule(){
        /**Retrieve Service types from pricing.*/
        $this->service_types =
            Pricing::serviceTypesPerModule($this->module);
    }

    public function add_to_temp_quotation_modules($service_type, $module_code = null){
        $module = (is_null($module_code))? $this->module : $module_code;
        $this->check_temp_quote_existance = TempQuotation::exist($this->module, $service_type, $this->cell, Auth::id());
        if(! $this->check_temp_quote_existance){
            TempQuotation::create([
                'module_code'=>$module,
                'service_type'=>$service_type,
                'price'=>Pricing::price($module,$service_type),
                'cell'=>$this->cell,
                'added_by'=>Auth::id(),
            ]);
        }

    }
}
