<?php

namespace App\Http\Livewire\MyDefaultModules;

use App\Http\Traits\Helper;
use App\Models\Category;
use Auth;
use Livewire\Component;


class MyDefaultModule extends Component
{
    use Helper;

    public $page_title = 'My Default Modules';
    public $modules = [];

    public function mount(){
        \App\Models\MyDefaultModule::where('user_id', Auth::id())
            ->each(function($mydefault){
                $this->modules[] =
                    Auth::id()."-".$mydefault->module_id."-".$mydefault->category_id;
            });

        //dd(array_values($this->modules));
    }

    public function render()
    {
        return view('livewire.my-default-modules.my-default-module');
    }

    public function add_my_default_module($module_id, $category_id){
        $query = \App\Models\MyDefaultModule::where('user_id', Auth::id())
            ->where('module_id',$module_id)->where('category_id', $category_id);

        if($query->exists()){
            $query->delete();
        }else{
            \App\Models\MyDefaultModule::create([
                'module_id' => $module_id,
                'category_id' =>  $category_id,
                'user_id' => Auth::id(),
            ]);
        }

    }
}
