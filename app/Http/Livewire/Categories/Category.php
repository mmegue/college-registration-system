<?php

namespace App\Http\Livewire\Categories;

use App\Exports\Export;
use App\Http\Traits\Helper;
use App\Imports\Import;
use Auth;
use Livewire\Component;
use App\Models\Category as _Category;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class Category extends Component
{
    use Helper, WithFileUploads;

    public $page_title = 'Categories';

    public $category_name, $sequence, $category_description;
    public $create, $edit, $download, $upload, $show = false;
    public $form_state = "table";
    public $category, $m_id, $file;

    protected $rules=[
        'category_name' => 'required|max:50',
        'sequence' => 'required|numeric',
        'category_description' => 'max:150',
    ];

    protected $listeners = [ 'showEdit' => 'show_edit' ];

    public function render(){
        $this->or_continue(['can_view_masterdata', 'can_crud_masterdata']);
        return view('livewire.categories.category');
    }

    public function show_edit($id,_Category $category){
        $this->reset_booleans();
        $this->edit = true;
        $this->m_id = $id;
        $category = _Category:: find($this->m_id);
        $this->category_name = $category->category_name;
        $this->sequence = $category->sequence;
        $this->category_description = $category->category_description;
    }

    public function show_create(){
        $this->reset_booleans();
        $this->create = true;
    }

    public function show_upload(){
        $this->reset_booleans();
        $this->upload = true;
    }

    public function create_category(){
        $this->continue('can_crud_masterdata');
        $validated_data = $this->validate();
        $validated_data['created_by'] = Auth::id();
        _Category::create($validated_data);
        $this->reset();
        $this->emit('refreshCategoryTable');
        $this->success("Category Successfully created.");
    }

    public function update_category(){
        $this->continue('can_crud_masterdata');
        $validated_data = $this->validate();
        _Category::find($this->m_id)->update($validated_data);
        $this->reset();
        $this->emit('refreshCategoryTable');
        $this->success("Category Successfully updated.");
    }

    public function reset_booleans(){
        $this->create = false; $this->edit = false;
    }

    /************** EXCEL OPERATION FILES ***************/

    public function show_upload_modal(){
        $this->show = true;
    }

    public function export_collection(){
        return _Category::all();
    }

    public function export_headings(){
        return [
            'id',
            'category_name',
            'category_description',
            'sequence',
            'created_by',
            'deleted_at',
            'created_at',
            'updated_at',
        ];
    }

    public function unique_by(){
        return 'category_name';
    }

    public function model(){
        return new \App\Models\Category();
    }

    public function import_column_fields(){
        return [
            'id' =>'id',
            'category_name'=>'category_name',
            'category_description'=>'category_description',
            'sequence'=>'sequence',
            'created_by'=>'created_by',
        ];
    }

    public function file_name(){
        return "Categories";
    }

    public function download_file(){
        $this->continue('can_export_masterdata');
        return Excel::download(
            new Export($this->export_collection(), $this->export_headings()),
            $this->file_name().'.xlsx'
        );
    }

    /* xport Model data to excel */
    public function upload_file(){
        $this->continue('can_import_masterdata');
        $this->validate([
            'file' => 'required|max:50000|mimes:xlsx', // 1MB Max
        ]);
        /*Import data*/
        Excel::import(
            new Import(
                $this->import_column_fields(),
                $this->unique_by(),
                $this->model()
            ),
            $this->file
        );
        /*Close the modal box*/
        $this->redirect('categories');
        $this->success('File Imported Successfully!!!');
    }


}
