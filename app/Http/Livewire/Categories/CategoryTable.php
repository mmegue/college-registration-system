<?php

namespace App\Http\Livewire\Categories;

use App\Http\Traits\Helper;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use App\Models\Category;

class CategoryTable extends LivewireDatatable
{
    use Helper;

    public $hideable = 'select';
    protected $listeners = ['refreshCategoryTable' => '$refresh'];

    public function mount($model = null, $include = [], $exclude = [], $hide = [], $dates = [], $times = [], $searchable = [], $sort = null, $hideHeader = null, $hidePagination = null, $perPage = 10, $exportable = false, $hideable = false, $beforeTableSlot = false, $afterTableSlot = false, $params = [])
    {
        parent::mount($model, $include, $exclude, $hide, $dates, $times, $searchable, $sort, $hideHeader, $hidePagination, $perPage, $exportable, $hideable, $beforeTableSlot, $afterTableSlot, $params); // TODO: Change the autogenerated stub
        $this->or_continue(['can_crud_masterdata']);
    }

    public function builder(){
        return Category::query()
            ->join('users','users.id','=','categories.created_by')
            ->orderBy('categories.id','desc');
    }

    public function columns()
    {
        return [
             Column::callback(['id'], function ($id) {
                return '<button wire:click="$emit(\'showEdit\','.$id.')"  class="bg-secondary"><i class="fa fa-edit"></i></button>';
            })->label('Action')->alignCenter(),

            Column::name('categories.category_name')
                ->label('Name')
                ->searchable()
                ->filterable(),

            Column::name('categories.category_description')
                ->label('Description')
                ->searchable()
                ->filterable(),

            Column::name('categories.sequence')
                ->label('Sequence')
                ->searchable()
                ->filterable(),

            Column::name('users.username')
                ->label('Created By')
                ->filterable()->searchable(),

            DateColumn::name('categories.created_at')
                ->filterable()->hide(),

            DateColumn::name('categories.updated_at')
                ->filterable()->hide(),
        ];
    }
}