<?php

namespace App\Http\Livewire\Assignment;

use App\Http\Livewire\ServiceTypes\ServiceType;
use App\Models\Assignment;
use App\Models\LecturerAllocation;
use App\Models\Pricing;
use Livewire\Component;

class Assignments extends Component
{
    public $page_title = "Assignments";

    public function render()
    {
        return view('livewire.assignment.assignments');
    }

}
