<?php

namespace App\Http\Livewire\Assignment;

use App\Http\Livewire\BulkAllocation\BulkAllocationTable;
use App\Http\Traits\Helper;
use App\Models\Assignment;
use App\Models\LecturerAllocation;
use App\Models\Pricing;
use App\Models\QuotationModule;
use DB;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class AssignmentsTable extends LivewireDatatable
{
    use Helper;
    public $hideable = 'select';

    public function builder()
    {
        return (new Assignment)->assignment();
    }

    public function columns()
    {
        return [
            Column::callback(['id'], function ($id) {
                return '<a role="button" target="_blank" href="'.route('view.assignment',$this->hash($id)).'" class=" btn btn-sm btn-secondary bg-secondary"><i class="fa fa-eye mr-2"></i>View</a>';
            })->label('Action')->alignCenter(),

            Column::name('usersLecturer.username')->label('Lecturer')->searchable()->filterable(),
            Column::name('usersStudent.name')->label('StudentName')->searchable()->filterable(),
            Column::name('usersStudent.surname')->label('StudentSurname')->searchable()->filterable(),

            Column::name('usersStudent.username')->label('Student#')->searchable()->filterable(),
            Column::name('usersStudent.myunisa_pwd')->label('MyUNI pwd')->searchable()->filterable(),
            Column::name('usersStudent.myit_pwd')->label('MyIT pwd')->searchable()->filterable(),

            Column::name('module_code')->label('Module Code')->searchable()->filterable(),
            Column::name('service_type')->label('Service Type')->searchable()->filterable(),
            Column::name('status')->label('Status')->searchable()->filterable(),
        ];
    }

}