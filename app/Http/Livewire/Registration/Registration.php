<?php

namespace App\Http\Livewire\Registration;

use Livewire\Component;

class Registration extends Component
{
    public $page_title = "Registrations";
    public function render()
    {
        return view('livewire.registration.registration');
    }
}
