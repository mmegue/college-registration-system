<?php

namespace App\Http\Livewire\Registration;

use App\Http\Traits\Helper;
use App\Http\Traits\Status;
use App\Models\Quotation;
use App\Models\QuotationModule;
use Auth;
use DB;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class QuotationTable extends LivewireDatatable
{
    use Helper, Status;
    public $hideable = 'select';
    public $exportable = true;

    protected $listeners = ['refreshTable'=>'$refresh'];
    
    public function builder(){

        if(auth()->user()->hasRole('super-admin') ){
            //if(auth_user_has_role('super-admin') || auth_user_has_role('marketing')){
            return Quotation::query()
                ->join('users','users.id','=','quotations.user_id')
                ->join('users AS admin','admin.id','=','quotations.created_by')
                ->where('semester', current_semester())
                ->orderBy('quotations.id','desc')
                //->where('quotations.id',1902)
                ->select(['quotations.*','users.title','users.name','users.surname','users.cell','users.email','admin.username']);
        }
        return Quotation::query()
            ->join('users','users.id','=','quotations.user_id')
            ->join('users AS admin','admin.id','=','quotations.created_by')
            ->orderBy('quotations.id','desc')
            ->where('quotations.created_by',Auth::id())
            ->where('semester', current_semester())
            ->select(['quotations.*','users.title','users.name','users.surname','users.cell','users.email','admin.username']);
    }

    public function delete_row($id){
        DB::transaction(function() use($id){
            $quotations = Quotation::find($id);
            /** Update the delete by column first */
            $quotations->quotation_modules()
                ->update(['deleted_by'=>Auth::id()]);

            /** delete the records */
            $quotations->quotation_modules()
                ->delete();

            /**Then update and delete the actual record*/
            $quotations->update(['deleted_by'=>Auth::id()]);
            $quotations->delete();
        });

       //$quotation_modules_ids = Quotation::find($id)->quotation_modules->pluck('id');
       //QuotationModule::destroy($quotation_modules_ids);
    }


    public function columns()
    {

        return [

            Column::callback(['quotations.id'], function ($id) {
                //return $this->hash($id);
                $continue = "";
                if(cn('can_view_quote')){
                    $continue .= '<div><a target="_blank" href="'.route('viewquote',check_url_id($id)).'" type="button" class="btn btn-sm btn-info px-1 py-0"><i class="fa fa-eye"></i></a></div>';
                }

                if(cn('can_delete_quote')){
                    $continue .= '<div x-show="!open" ><button @click="open=true" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-trash"></i></button></div>
                                        <div x-show="open" ><button @click="open=false" wire:click="delete_row('.$id.')" type="button" class="btn btn-sm btn-success px-1 py-0"><i class="fa fa-check"></i></button></div>
                                        <div x-show="open" ><button @click="open=false" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-times"></i></button></div>';
                }
                return '<div x-data="{ open: false }">
                                <div class="btn-group" role="group" >
                                    '.$continue.'
                                </div>
                            </div>';

            })->label('Action')->alignCenter(),

            //Column::name('quotations.id')
            //   ->label('ID'),

            Column::name('quotations.quotation_number')
                ->label('Quotation#')
                ->searchable()->filterable(),

            Column::callback(['quotations.status'], function ($status) {
                if ($status === 'Invoiced') {
                    return '<span class="badge badge-success">' . $status . '...' . '</span>';
                }
                if ($status === 'Quotation') {
                    return '<span class="badge badge-danger">' . $status . '</span>';
                }
                return $status;
            })->label('Status')->filterable(['Quotation', 'Invoiced'])->searchable(),

            Column::name('quotations.semester')
                ->label('Semester')
                ->filterable(),

//            Column::name('users.title')
//                ->label('Title')
//                ->searchable()->filterable($this->titles()),
//
            Column::name('users.name')
                ->label('Name')
                ->searchable()->filterable(),

            Column::name('users.surname')
                ->label('Surname')
                ->searchable()->filterable(),

            Column::name('users.cell')
                ->label('Cell')
                ->searchable()->filterable(),

            Column::name('users.email')
                ->label('Email')
                ->searchable()->filterable(),

            Column::name('admin.username')
                ->label('Added By')
                ->filterable($this->users()->pluck('username')),

//            DateColumn::name('quotations.created_at')
//                ->filterable()->hide(),
//
//            DateColumn::name('quotations.updated_at')
//                ->filterable()->hide(),
        ];
        
    }
}