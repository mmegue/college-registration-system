<?php

namespace App\Http\Livewire\Registration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Livewire\Component;

class Quotations extends Component
{

    public $page_title = "Quotations";

    public function render()
    {
        return view('livewire.registration.quotations');
    }
}
