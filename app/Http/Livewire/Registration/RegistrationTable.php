<?php

namespace App\Http\Livewire\Registration;

use App\Http\Traits\Helper;
use App\Models\QuotationModule;
use App\Models\ServiceType;
use App\Models\User;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Exports\DatatableExport;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class RegistrationTable extends LivewireDatatable
{
    use Helper;

    public $hideable = 'select';
    public $exportable = true;

    public function builder()
    {
        if(Auth::user()->hasRole('super-admin')){
            return (new QuotationModule)->AllQuoteModulesWithAssociatedData();
        }

        return (new QuotationModule)->AllQuoteModulesWithAssociatedData()
            ->where('uB.username',Auth::id());
    }

    public function export()
    {
        $this->forgetComputed();
        return Excel::download(new DatatableExport($this->getQuery()->get()), 'Registrations.xlsx');
    }

    public function columns()
    {
        return [
            Column::name('U.title')->label('Title'),
            Column::name('U.name')->label('Name')->searchable()->filterable(),
            Column::name('U.surname')->label('Surname')->searchable()->filterable(),
            Column::name('Q.quotation_number')->label('Quote#')->searchable()->filterable(),
            Column::name('Q.invoice_number')->label('Invoice#')->searchable()->filterable(),
            Column::name('quotation_modules.module_code')->label('ModuleCode')->filterable($this->modules->pluck('module_code'))->searchable(),
            Column::name('quotation_modules.service_type')->label('ServiceT')->filterable($this->servicetypes->pluck('slug'))->searchable(),
            Column::name('uB.username')->label('QCreatedBy')->filterable($this->users->pluck('username')),
            Column::name('uA.username')->label('MAddedBy')->filterable($this->users->pluck('username')),
            Column::name('uC.username')->label('DefaultLect')->filterable($this->users->pluck('username')),
            Column::name('quotation_modules.attendance')->label('Attendance')->filterable()->searchable(),
            Column::name('quotation_modules.price')->label('Price')->filterable()->searchable(),
        ];
    }
    
}