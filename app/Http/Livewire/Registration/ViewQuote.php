<?php

namespace App\Http\Livewire\Registration;

use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use App\Models\EmailsAndSmssLog;
use App\Models\LecturerAllocation;
use App\Models\PaymentArrangment;
use App\Models\Pricing;
use App\Models\Quotation;
use App\Models\QuotationModule;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Livewire\Component;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Repositories\StatementsRepository;

class ViewQuote extends Component
{
    use Helper, Settings;

    public $page_title = 'View Quotation';
    public $quote_id, $quotation,$send_statement, $quotation_modules=[], $user, $data=[];
    public $temp_module_code_update=[],$temp_lecturer_update=[], $temp_semester_update=[],
           $temp_service_type_update=[], $temp_attendance_update=[], $temp_price_update=[];
    public $state = "pi", $module; //$from_email, $company_name;
    public $prc = false, $service_types=[];
    public $amount,$gl_account,$campus,$description, $when, $how_much;
    //public $user;
    public Quotation $quotationp;
    public User $userModel;
    protected $listeners = ['refresh'=>'$refresh'];
    public $pdf_data, $user_id, $regfee; public $quote_details, $company;

    protected $rules = [
        'userModel.title'=> 'required|max:255',
        'userModel.name'=> 'required|max:255',
        'userModel.surname'=> 'required|max:255',
        'userModel.cell'=> 'required|integer|digits_between:10,15',
        'userModel.email'=> 'required|email|max:255',
        'userModel.username'=> 'required|max:55',
        'userModel.myit_pwd'=> 'nullable|max:255',
        'userModel.myunisa_pwd'=> 'nullable|max:255',

        'company.company_name' => 'nullable|max:255',
        'company.address_1' => 'nullable|max:255',
        'company.address_2' => 'nullable|max:255',
        'company.suburb' => 'nullable|max:255',
        'company.city' => 'nullable|max:255',
        'company.company_cell' => 'nullable|integer|digits_between:10,15',
        'company.company_email' => 'nullable|email|max:255',
        'company.vat_number' => 'nullable|max:255',
        'company.note' => 'nullable|max:255',
    ];
    
    public function mount($id){
        $this->quote_id = $this->check_url_id($id);
        $this->quotation =  Quotation::with([
            'user',
            'quotation_modules',
            'payment_arrangments'
        ])
            ->where('quotations.id', $this->quote_id)
            ->where('quotations.semester', current_semester())
            ->first();

         $this->user_id = session()->get('auth_id');
         $this->userModel = User::find($this->quotation->user_id);
         $this->company = optional($this->userModel)->company;
    }

    public function render()
    {                             
        $this->continue('can_view_quote');
        return view('livewire.registration.view-quote',[
            'created_by' => User::get_added_by($this->quotation->created_by),
        ]);
    }

    public function open($state){
        $this->state = $state;
    }

    public function reg_fee_existance(){
        $exists = QuotationModule::where('cell',$this->quotation->user->cell)
                ->where('module_code','REGFEE')
                ->where('quotation_id',$this->quote_id)
                ->where('semester',$this->semester())->exists();

        return ($exists);
    }
    /**DOWNLOAD, EMAIL DATA*/
    public function data(){
        $transactions = Transaction::whereCellNumber($this->quotation->user->cell)
            ->where('transactions.semester_period', current_semester());
        return [
            'quotation_details' => $this->quotation->user,
            'quotation_modules' => $this->quotation->quotation_modules,
            'total' =>  $this->quotation->quotation_modules->sum('price'),
            'transactions' => $transactions,
        ];
    }

    public function statement_data(){
        $quote_m = QuotationModule::join('quotations','quotations.id','=','quotation_modules.quotation_id')
            ->join('users','users.id','=','quotation_modules.added_by')
            ->where('quotation_modules.status','Invoiced')
            ->where('quotation_modules.cell',$this->quotation->user->cell)
            ->where('quotation_modules.semester',$this->semester())
            ->select('users.username','quotation_modules.*','quotations.quotation_number','quotations.invoice_number')->get();
        $transactions = Transaction::whereCellNumber($this->quotation->user->cell)->where('semester_period', $this->semester());
        return [
            'quotation_details' => User::whereCell($this->quotation->user->cell)->first(),
            'quotation_modules' => $quote_m,
            'total' =>  $quote_m->sum('price'),
            'transactions' => $transactions,
            'statement_number' => "S".$this->quotation->user->cell
        ];
    }

    public function download_statement($id){
        return (new StatementsRepository($id))->download_statement();
    }

    public function email_statement($id){
        (new StatementsRepository($id))->email_statement();
    }
    /**DOWNLOADS*/

    public function updatedRegfee($state){
        if($state){
            QuotationModule::create([
                'quotation_id' => $this->quote_id,
                'module_code'=>"REGFEE",
                'service_type'=>'reg-fee',
                'price'=>$this->get_reg_fee(),
                'cell'=>$this->quotation->user->cell,
                'semester' => $this->semester(),
                'added_by'=>$this->user_id,
            ]);
        }else{
            QuotationModule::where('cell',$this->quotation->user->cell)
                ->where('module_code','REGFEE')
                ->where('semester',$this->semester())->delete();
        }
    }

    /**DOWNLOADS*/
    public function download_quotation()
    {
        $this->continue('can_download_quote');
        $data = $this->data();
        return response()->streamDownload(function () use($data){
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('livewire.registration.quotation-template',$data);
            echo $pdf->stream();
        }, 'Q-'.$this->quotation->quotation_number.'.pdf');

    }


    public function download_invoice()
    {
        $this->continue('can_download_invoice');
        $data = $this->data();
        //dd($data['quotation_details']['user']);
        return response()->streamDownload(function () use($data){
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('livewire.registration.invoice-template',$data);
            echo $pdf->stream();
        }, 'I-'.$this->quotation->invoice_number.'.pdf');

    }



    /**EMAILS */
    public function email_invoice(){
        $this->continue('can_email_invoice');
        if(Setting::get('send_emails')=="1"){
            $data = $this->data();
            $instance = $this->quotation_instance();
            Mail::send('emails.invoice-mail', $data, function($message) use ($data,$instance){
                //libxml_use_internal_errors(true);
                $pdf = PDF::loadView('livewire.registration.invoice-template',$data);
                //libxml_use_internal_errors(false);
                $message->to($this->quotation->user->email,$this->quotation->user->name.' '.$this->quotation->user->surname)->subject('Invoice Report');
                $message->from($this->from_email,$this->company_name);
                $message->attachData($pdf->output(), 'I-'.$this->quotation->quotation_number.'.pdf');
            });
            $this->success('Email sent successfully!![Invoice]');
        }else{
            $this->success('Sending of emails is currently disabled by the admin.');
        }

    }

    public function email_quotation(){
        $this->continue('can_email_quote');
        if(Setting::get('send_emails')=="1"){
            $data = $this->data();
            $instance = $this->quotation_instance();
            Mail::send('emails.quotation-mail', $data, function($message) use ($data,$instance){
                //libxml_use_internal_errors(true);
                $pdf = PDF::loadView('livewire.registration.quotation-template',$data);
                //libxml_use_internal_errors(false);
                $message->to($this->quotation->user->email,$this->quotation->user->name.' '.$this->quotation->user->surname)->subject('Quotation Report');
                $message->from($this->from_email,$this->company_name);
                $message->attachData($pdf->output(), 'I-'.$this->quotation->quotation_number.'.pdf');
            });
            $this->success('Email sent successfully!![Quotation]');
        }else{
            $this->success('Sending of emails is currently disabled by the admin.');
        }
    }



    /**MODULE QUOTATION*/
    public function invoice_quotation()
    {
        $this->check_if_invoiced();
        $this->continue('can_invoice_quote');
        $invoice_number = $this->generate_invoice_number();
        DB::transaction(function() use ($invoice_number){
            /**Update the Quotation*/
            $quote = Quotation::find($this->quote_id);
            $quote->update([
                'status' => 'Invoiced',
                'invoice_number' => $invoice_number,
                'invoiced_by' => $this->user_id,
            ]);

            $this->quotation->quotation_modules
                ->each(function($data) {
                    /**Add to registration table*/
                    $model = \App\Models\QuotationModule::find($data->id);
                    $model->update([
                        'status' => 'Invoiced'
                    ]);

                });
                $this->quotation->quotation_modules
                ->each(function($data) use($quote){
                    /**Add to registration table*/
                    \App\Models\Registration::create([
                        'user_id' => $quote->user_id,
                        'module_code' => $data->module_code,
                        'service_type' => $data->service_type,
                        'price' => $data->price,
                        //'status' => $data->status,
                        'quotation_number' => $quote->quotation_number,
                        'invoice_number' => $quote->invoice_number,
                        'semester' => $data->semester,
                        'attendance' => $data->attendance,
                        'quote_created_by' => $quote->created_by,
                        'invoiced_by' => $quote->invoiced_by,
                        'referred_by' => $quote->referred_by,
                        'lecturer_id_default' => $data->lecturer_id_default,
                        'lecturer_id_alt' => $data->lecturer_id_alt,
                        'module_added_by' => $data->added_by,
                        'note' => $data->note,
                    ]);

                });

            $this->emit('refresh');
            $this->success('Quotation Successfully Invoiced!!');
        });

    }

    public function update_details_quotation(){
        //dd(Quotation::find($this->quote_id));
        //$this->check_if_invoiced();
        $this->continue('can_edit_quote');

        $this->userModel->update([
            'title' => $this->userModel->title,
            'name' => $this->userModel->name,
            'surname' => $this->userModel->surname,
            'cell' => $this->userModel->cell,
            'email' => $this->userModel->email,
            'username' => $this->userModel->username,
            'myit_pwd' => $this->userModel->myit_pwd,
            'myunisa_pwd' => $this->userModel->myunisa_pwd,
        ]);
        //Quotation::find($this->quote_id)->update();
        $this->emitSelf('refresh');
        $this->success('Quotation Details Successfully Added!!');
    }

    public function add_payment_arrangement(){
        $this->check_if_invoiced();
        $this->continue('can_invoice_quote');
        $this->validate([
            'when'=>'required|date|after:yesterday',
            'how_much' => 'required|numeric'
        ]);
        PaymentArrangment::create([
            'quotation_id' => $this->quote_id,
            'when' => $this->when,
            'how_much' => $this->how_much,
            'added_by' => $this->user_id,
            'status' => 'Pending',
            'approval' => false,
            'send_statement' => $this->send_statement,
            'user_id' => $this->quotation->user_id
        ]);

        $this->reset(['when','how_much']);
        $this->emitSelf('refresh');
        $this->success('PA Successfully Added!!');

    }

    public function add_cash_receipt(){
        $this->check_if_invoiced();
        $this->continue('can_crud_cash_receipts');
        $this->validate([
            'amount'=>'required|integer',
            'gl_account'=>'required',
            'campus'=>'required',
        ]);
        Transaction::create([
            'user_id'=>Auth::id(),
            'amount'=>$this->amount,
            'item'=>$this->quotation->user->cell,
            'gl_account'=>$this->gl_account,
            'campus'=>$this->campus,
            'transaction_date'=>Carbon::now(),
            'transaction_type'=>'Cash Receipt',
            'fs_accounts'=> 'Sales',
            'tax_period'=> $this->tax_period(),
            'allocation_period' => $this->allocation_period(),
            'short_description'=>$this->description,
            'receipt_number'=>'RCT-'.date('YmdHis'),
            'semester_period'=>$this->semester(),
            'internal_transaction_number'=>'T'.date('YmdHis'),
            'cell_number'=>$this->quotation->user->cell,
        ]);
        $this->state = "t";
        $this->emitSelf('refresh');
        $this->success('Cash Receipt Successfully Added!!');

    }

    public function add_to_temp_quotation_modules($service_type){
        $this->check_if_invoiced();
        $this->continue('can_request_quote');
        if(! (new QuotationModule)->exist($this->quote_id, $this->module, $service_type)){
            QuotationModule::create([
                'quotation_id'=> $this->quote_id,
                'module_code'=>$this->module,
                'service_type'=>$service_type,
                'price'=>Pricing::price($this->module,$service_type),
                'semester'=>$this->semester(),
                'cell'=>$this->quotation->user->cell,
                'added_by'=>$this->user_id,
                'lecturer_id_default' => $this->get_default_lect($this->module)
            ]);
            $this->state = "cm";
            $this->success('Additional module successfully added!!');
            $this->emitSelf('refresh');
        }else{
            $this->error('Module with the same service type exists for this quotation.');
        }
    }

    /** Get service types on module input update*/
    public function updatedModule(){
        //$this->check_if_invoiced();
        $this->continue('can_edit_quote');
        /**Retrieve Service types from pricing.*/
        $this->service_types =
            Pricing::serviceTypesPerModule($this->module);
        $this->emit('refresh');
    }

    public function updatedTempLecturerUpdate($state){
        $this->check_if_invoiced();
        $this->continue('can_assign_modules_to_lecturers');
        $id = array_key_first($this->temp_lecturer_update);
        QuotationModule::find($id)->update([
            'lecturer_id_default'=>$this->temp_lecturer_update[$id]
        ]);
        $this->reset('temp_lecturer_update');
        $this->emitSelf('refresh');
        $this->success('Lecturer updated successfully');
    }

    public function updatedTempAttendanceUpdate($state){
        $this->check_if_invoiced();
        $this->continue('can_edit_quote');
        $id = array_key_first($this->temp_attendance_update);
        QuotationModule::find($id)->update([
            'attendance'=>$this->temp_attendance_update[$id]
        ]);
        $this->reset('temp_attendance_update');
        $this->emitSelf('refresh');
        $this->success('Attendance updated successfully');
    }

    public function updatedTempSemesterUpdate($state){
        $this->check_if_invoiced();
        $this->continue('can_edit_quote');
        $id = array_key_first($this->temp_semester_update);
        QuotationModule::find($id)->update([
            'semester'=>$this->temp_semester_update[$id]
        ]);
        $this->reset('temp_semester_update');
        $this->emitSelf('refresh');
        $this->success('Semester updated successfully');
    }

    public function tempPriceUpdate(){
        $this->check_if_invoiced();
        $this->continue('can_edit_quote');
        $id = array_key_first($this->temp_price_update);
        QuotationModule::find($id)->update([
            'price'=>$this->temp_price_update[$id]
        ]);
        $this->reset('temp_price_update');
        $this->emitSelf('refresh');
        $this->success('Price updated successfully');
    }

    public function updatedTempServiceTypeUpdate($state){
        $this->check_if_invoiced();
        $this->continue('can_edit_quote');
        $id = array_key_first($this->temp_service_type_update);
        $quotation = QuotationModule::find($id);
        $quotation->update([
            'service_type'=>$state,
            'price' => Pricing::price($quotation->module_code,$this->temp_service_type_update)
        ]);
        $this->reset('temp_service_type_update');
        $this->emitSelf('refresh');
        $this->success('Service type updated successfully');
        
    }

    public function get_default_lect($module_code){
        $id = LecturerAllocation::where('module_code',$module_code)
            ->where('default',1)->first();

        return (is_null($id))? null : $id->lecturer_id;
    }

    public function updatedTempModuleCodeUpdate($state){
        //dd($state);
        $this->check_if_invoiced();
        $this->continue('can_edit_quote');
        $id = array_key_first($this->temp_module_code_update);
        $quotation = QuotationModule::find($id);
        $quotation->update([
            'module_code'=>$state,
            'price' => Pricing::price($this->temp_module_code_update[$id], $quotation->service_type),
            'lecturer_id_default' => $this->get_default_lect($state)
        ]);
        $this->reset('temp_module_code_update');
        $this->emitSelf('refresh');
        $this->success('Module updated successfully');
    }

    public function get_quote_transactions(){
        return (new Transaction)->user_transactions($this->quotation->user->cell)->get();
    }

    public function get_payment_arrangments(){
        return (new PaymentArrangment)
            ->payment_arrangements_with_addedby($this->quote_id);
    }

    public function restore_deleted_module_quotation($id)
    {
        $this->check_if_invoiced();
        $this->continue('can_edit_quote');
        QuotationModule::withTrashed()
            ->where('id', $id)->restore();
        $this->emit('refresh');
    }

    public function permanent_delete_module_quotation($id)
    {
        $this->check_if_invoiced();
        if(cn('can_permanent_delete')){
            QuotationModule::withTrashed()
                ->where('id', $id)->forceDelete();
            $this->emit('refresh');
        }else{ $this->auth_error(); }
        
    }

    public function get_deleted_quotation_modules()
    {
        //return QuotationModule::onlyTrashed()->where('quotation_id', $this->quote_id)->get();
        return  QuotationModule::onlyTrashed()
            ->join('users','users.id','=','quotation_modules.added_by')
            ->where('quotation_id', 2)->select(['quotation_modules.*','users.username'])->get();
    }

    public function delete_quote_module($id){
        $this->check_if_invoiced();
        $this->continue('can_delete_quote');
        $this->delete_row($id);
        $this->emit('refresh');
    }

    public function delete_payment_arrangement($id){
        $this->check_if_invoiced();
        $this->continue('can_delete_payment_arrangement');
        PaymentArrangment::destroy($id);
        $this->emit('refresh');
    }

    public function get_quotation_modules(){

        return (new QuotationModule)->allWithUserQuotationCurrentSemester()
            ->where('quotation_modules.quotation_id',$this->quote_id)->select([
                'quotation_modules.id',
                'module_code',
                'lecturer_id_default',
                'service_type',
                'attendance',
                'quotation_modules.semester',
                'price',
                'users.username as addedby',
                'uC.username as lect',
            ])->get();
    }

    public function check_if_invoiced(){
        if($this->quotation->status === "Invoiced" ){

            if(cn('can_edit_invoice')){

            }
            else{
                $this->error('Oooooops, Already Invoiced!') ;
                exit;
            }
        }
    }
}
