<?php

namespace App\Http\Livewire;

use App\Models\Quotation;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class StudentDashboard extends Component
{

    public $page_title = "Dashboard";
    public $my_quotations, $my_quotations_count;
    public $my_invoices, $my_invoices_count, $my_referrals;

    public const INVOICED = "Invoiced";
    public const QUOTATIONS = "Quotations";

    public function mount(){
        //$this->my_quotations = $this->qi(self::QUOTATIONS)->get();
        $this->my_quotations_count = $this->qi(self::QUOTATIONS)->count();

        //$this->my_invoices = $this->qi(self::INVOICED)->get();
        $this->my_invoices_count = $this->qi(self::INVOICED)->count();
    }

    public function render()
    {
        return view('livewire.student-dashboard');
    }

    public function get_my_qi($status){
        return Quotation::with(['user','payment_arrangments','quotation_modules'])
            ->where('quotations.user_id', Auth::id())
            ->where('quotations.status',$status)
            ->get();
    }

    public function qi($status){
        return Quotation::where('user_id', Auth::id())
            ->where('status',$status);
    }

}
