<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RequestQuote extends Component
{
    public function render()
    {
        return view('livewire.request-quote');
    }
}
