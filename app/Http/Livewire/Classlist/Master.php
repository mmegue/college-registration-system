<?php

namespace App\Http\Livewire\Classlist;

use App;
use App\Exports\ClasslistMaster;
use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use App\Models\Quotation;
use App\Models\QuotationModule;
use App\Models\Transaction;
use Auth;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class Master extends Component
{
    use Settings, Helper;
    public $page_title = "Master Class List";
    public $module_code = null, $service_type = null, $lecturer = null, $attendance = null, $lists=[];

    public function mount(){
        //$this->semester = $this->semester();
    }

    public function render()
    {
        $this->continue('can_request_classlist_master');
        return view('livewire.classlist.master');
    }

    public function export_pdf(){
        $this->continue('can_export_master_classlist');
        $data = $this->data();
        $this->log_classlist_generated([
            'initiated_by' => Auth::id(),
            'details' => Auth::user()->username.": Downloaded [PDF] Master Classlist | ".$this->module_code." | ".$this->service_type." | ".$this->lecturer." | ".$this->attendance,
        ]);
        return response()->streamDownload(function () use($data){
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('livewire.classlist.pdf-classlist-template',['data'=>$data, 'title' =>  'Master ClassList']);
            $pdf->setPaper('A4', 'landscape');
            echo $pdf->stream();
        }, 'ClassList-'.$this->module_code.'-'.$this->service_type.'.pdf');

    }

    public function reset_($property){
        if($property === 'module_code'){ $this->reset('module_code'); $this->data();}
        if($property === 'service_type'){ $this->reset('service_type'); $this->data();}
        if($property === 'lecturer'){ $this->reset('lecturer'); $this->data();}
        if($property === 'attendance'){ $this->reset('attendance'); $this->data();}
    }

    public function data(){
        //$this->continue('can_request_classlist_master');
        if(is_null($this->module_code) && is_null($this->service_type) && is_null($this->lecturer) && is_null($this->attendance)){
            $this->reset('lists'); return [];
        }
        
        $this->reset('lists');
        (new QuotationModule)->AllQuoteModulesWithAssociatedData()
            ->when(!is_null($this->module_code),function($query){
                return $query->where('quotation_modules.module_code',$this->module_code);
            })
            ->when(!is_null($this->service_type),function($query){
                return $query->where('quotation_modules.service_type',$this->service_type);
            })
            ->when(!is_null($this->lecturer),function($query){
                return $query->where('quotation_modules.lecturer_id_default',$this->lecturer);
            })
            ->when(!is_null($this->attendance),function($query){
                return $query->where('quotation_modules.attendance',$this->attendance);
            })
            ->each(function($query) {
                $paid = (new Transaction)->amount_paid($query->cell);
                $invoiced = (new QuotationModule)->total_invoiced($query->cell);
                $balance = $invoiced-$paid;
                $pmt = ($invoiced == 0) ? 0 : round(($paid/$invoiced)*100,0);
                $array = [
                    'modules' => implode(',', array_unique(array_values(Quotation::find($query->quotation_id)->quotation_modules->pluck('module_code')->toArray()))),
                    'invoiced' => $invoiced,
                    'paid' => $paid,
                    'balance' => $balance,
                    'pmt' => $pmt,
                    'module_code' => $query->module_code,
                    'service_type' => $query->service_type,
                    'price' => $query->price,
                    'attendance' => $query->attendance,
                    'semester' => $query->semester,
                    'cell' => $query->cell,
                    'status' => $query->status,
                    'quotation_number' => $query->quotation_number,
                    'invoice_number' => $query->invoice_number,
                    'title' => $query->title,
                    'username' => $query->username,
                    'name' => $query->name,
                    'surname' => $query->surname,
                    'email' => $query->email,
                    'myunisa_pwd' => $query->myunisa_pwd,
                    'quote_module_added_by' => $query->quote_module_added_by,
                    'quote_created_by' => $query->quote_created_by,
                    'lecturer_default' => $query->lecturer_default,
                ];

                $this->lists[]  = $array;
            });
        return $this->lists;
    }

    public function export()
    {
        $this->continue('can_export_master_classlist');
        $this->log_classlist_generated([
            'initiated_by' => Auth::id(),
            'details' => Auth::user()->username.": Downloaded [Excel] | ".$this->module_code." | ".$this->service_type." | ".$this->lecturer." | ".$this->attendance,
        ]);
        return Excel::download(new ClasslistMaster($this->data()), 'Classlist-Master.xlsx');
    }
}
