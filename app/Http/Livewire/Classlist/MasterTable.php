<?php

namespace App\Http\Livewire\Classlist;

use App;
use App\Exports\ClasslistMaster;
use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use App\Models\Quotation;
use App\Models\QuotationModule;
use App\Models\Transaction;
use App\Models\User;
use Auth;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Facades\Excel;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Exports\DatatableExport;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class MasterTable extends LivewireDatatable
{
    use Helper, Settings;
    
    public $hideable = 'select';
    public $perPage = 10;
    public $exportable = true;
    public $list = [];

    public function builder()
    {
       //dd((new QuotationModule)->AllQuoteModulesWithAssociatedDataClassList()->get());
        $this->continue('can_request_classlist_master');
        return (new QuotationModule)->AllQuoteModulesWithAssociatedData();
    }

    public function export_pdf(){
        $data = $this->data();
        $this->log_classlist_generated([
            'initiated_by' => Auth::id(),
            'details' => Auth::user()->username.": Downloaded [PDF] Master Classlists "
        ]);
        return response()->streamDownload(function () use($data){
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('livewire.classlist.pdf-classlist-template',['data'=>$data, 'title' =>  'Master ClassList']);
            $pdf->setPaper('A4', 'landscape');
            echo $pdf->stream();
        }, 'Master ClassList.pdf');

    }

    public function data(){
        $list = [];
        (new QuotationModule)->AllQuoteModulesWithAssociatedData()->each(function($query) use($list){
            $paid = (new Transaction)->amount_paid($query->cell);
            $invoiced = (new QuotationModule)->total_invoiced($query->cell);
            $balance = $invoiced-$paid;
            $modules = array_unique(
                array_values(
                    Quotation::find($query->quotation_id)
                        ->quotation_modules
                        ->pluck('module_code')
                        ->toArray()
                )
            );

            $array = [
                'modules' => implode(', ', Arr::sort($modules)),
                'invoiced' => $invoiced,
                'paid' => $paid,
                'balance' => $balance,
                'pmt' => round(($paid/$invoiced)*100,0),
                'module_code' => $query->module_code,
                'service_type' => $query->service_type,
                'price' => $query->price,
                'attendance' => $query->attendance,
                'semester' => $query->semester,
                'cell' => $query->cell,
                'status' => $query->status,
                'quotation_number' => $query->quotation_number,
                'invoice_number' => $query->invoice_number,
                'title' => $query->title,
                'name' => $query->name,
                'surname' => $query->surname,
                'email' => $query->email,
                'myunisa_pwd' => $query->myunisa_pwd,
                'quote_module_added_by' => $query->quote_module_added_by,
                'quote_created_by' => $query->quote_created_by,
                'lecturer_default' => $query->lecturer_default,
            ];

            $list[]  = $array;
        });
        return $list;
    }

    public function export()
    {
        $this->log_classlist_generated([
            'initiated_by' => Auth::id(),
            'details' => Auth::user()->username.": Downloaded [Excel] Master Classlists "
        ]);

        $this->forgetComputed();
        return Excel::download(new ClasslistMaster($this->data()), 'Classlist-MasterN.xlsx');
    }

    public function columns()
    {
        return [
            
            Column::name('U.title')
                ->label('Title')
                ->searchable(),

            Column::name('U.name')
                ->label('Name')
                ->searchable(),

            Column::name('U.surname')
                ->label('Surname')
                ->searchable(),

            Column::name('U.email')
                ->label('Email')
                ->searchable(),

            Column::name('U.cell')
                ->label('Cell')
                ->searchable(),

            Column::name('quotation_modules.module_code')
                ->label('Module')
                ->searchable()->filterable($this->modules->pluck('module_code')),

            Column::name('uC.username')
                ->label('Lecturer')
                ->searchable()->filterable(User::role('lecturer')->pluck('username')),

            Column::name('quotation_modules.service_type')
                ->label('Service')
                ->searchable()->filterable($this->servicetypes->pluck('slug')),

            Column::name('quotation_modules.attendance')
                ->label('Attendance')
                ->searchable()->filterable($this->attendance),

            Column::callback(['Q.id'], function ($id) {
                return implode(', ', array_unique(array_values(Quotation::find($id)->quotation_modules->pluck('module_code')->toArray())));
            })->label(' Modules'),

            Column::callback(['quotation_modules.cell'], function ($cell) {
                $paid = (new Transaction)->amount_paid($cell);
                $invoiced = (new QuotationModule)->total_invoiced($cell);
                $balance = $invoiced-$paid;
                return view('livewire.classlist.payments',[
                    'paid'=> $paid, 'invoiced'=>$invoiced, 'balance'=>$balance
                ]);
            })->label('Payments')->hide(),

            Column::callback(['quotation_modules.cell','quotation_modules.id'], function ($cell) {
                return (new QuotationModule)->total_invoiced($cell);
            })->label('Invoiced(R)'),

            Column::callback(['quotation_modules.cell','uC.username'], function ($cell) {
                return (new Transaction)->amount_paid($cell);
            })->label('Paid(R)'),

            Column::callback(['quotation_modules.cell','Q.invoice_number'], function ($cell) {
                $paid = (new Transaction)->amount_paid($cell);
                $invoiced = (new QuotationModule)->total_invoiced($cell);
                return $invoiced-$paid;
            })->label('Balanace(R)'),

            Column::callback(['quotation_modules.cell','U.myunisa_pwd'], function ($cell) {
                $paid = (new Transaction)->amount_paid($cell);
                $invoiced = (new QuotationModule)->total_invoiced($cell);
                $pmt = round(($paid/$invoiced)*100,0);
                if($pmt > 100){
                    return '<span class="text-danger font-weight-bold">'.$pmt.'%</span>';
                }
                return $pmt.'%';
            })->label('PMT(%)'),


            Column::name('Q.quotation_number')
                ->label('Quote#')
                ->searchable(),

            Column::name('Q.invoice_number')
                ->label('Inv#')
                ->searchable(),

            Column::name('U.myunisa_pwd')
                ->label('MyUniPswd')
                ->searchable(),

            Column::name('quotation_modules.status')
                ->label('Status')
                ->searchable()->filterable(['Quotation','Invoiced']),

            Column::name('quotation_modules.price')
                ->label('Price'),

            Column::name('uA.username')
                ->label('RaisedBy'),

        ];
    }
}