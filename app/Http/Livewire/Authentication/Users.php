<?php

namespace App\Http\Livewire\Authentication;

use App\Http\Traits\Helper;
use App\Models\User;
use App\Notifications\NewUserRegistered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Livewire\Component;


class Users extends Component
{
    use Helper;

    public $page_title = 'Users';
    public $auth;
    public $title;
    public $username;
    public $name;
    public $surname;
    public $cell;
    public $email;
    public $role;

    public function mount(){
        $this->continue('can_view_users');
        $this->auth = auth()->user();
    }

    public function render()
    {
        return view('livewire.authentication.users');
    }

    public function get_user_data(){
        if($this->auth->hasrole('super-admin')){
            return User::all_users_with_roles();
        }
        if($this->auth->hasrole('admin')){
            return User::get_users_without_roles(['admin','super-admin']);
        }
        return [];
    }

    public function register_new_user(){
        $validated_data = $this->validate([
            'title' => 'required | max:150',
            'username' => 'required | max:150',
            'name' => 'required | max:150',
            'surname' => 'required | max:150',
            'cell' => 'required|integer|digits_between:10,14',
            'email' => 'required | email',
        ]);
        $this->or_continue(['can_crud_users','can_create_users']);
        $user = User::create([
            'title' => $this->title,
            'username' => $this->username,
            'name' => $this->name,
            'surname' => $this->surname,
            'cell' => $this->cell,
            'email' => $this->email,
            'password' => Hash::make($this->cell),
        ]);
        $user->assignRole($this->role);
        $users = User::role('super-admin')->get();
        Notification::send($users, new NewUserRegistered($user));
        $this->success('New user successfully created.');
    }

}
