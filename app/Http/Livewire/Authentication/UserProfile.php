<?php

namespace App\Http\Livewire\Authentication;

use App\Http\Traits\Helper;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserProfile extends Component
{

    use Helper;
    public $page_title = "User Profile";
    public $user;
    public $user_id;

    public $update_user_info=false;

    public $password,$password_confirmation,$current_password;

    protected $rules = [
        'user.title' => 'required|string|max:80',
        'user.username' => 'required|string|max:35',
        'user.name' => 'required|string|max:80',
        'user.surname' => 'required|string|max:80',
        'user.email' => 'required|email|max:150',
        'user.cell' => 'required|integer',
    ];


    public function mount(){
        $this->user_id = $this->check_url_id(Auth::id());
        $this->user = Auth::user();
        if(empty($this->user)){
            abort(404);
        }
        $this->userroles = implode(',',$this->user->roles->pluck('name')->toArray());

    }

    public function render()
    {
        return view('livewire.authentication.user-profile');
    }


    public function update_personal_info(){
        $this->validate();
        $this->user->save();
        return $this->success("User Info successfully updated!");;
    }

    public function update_user_password(){
        $this->validate([
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'current_password' => 'required',
        ]);
        $hashedPassword = Auth::user()->getAuthPassword();
        if (Hash::check($this->current_password, $hashedPassword)) {
            $this->user->forceFill([
                'password' => Hash::make($this->password),
            ])->save();
            return $this->success("User Password successfully updated!");
        }else{
            return $this->error("Current password does not match!");
        }
    }
}
