<?php

namespace App\Http\Livewire\Authentication;

use App\Http\Traits\Helper;
use App\Models\Setting;
use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesPermissions extends Component
{
    use Helper;

    public $page_title = "Roles & Permissions";
    public $expand = true;

    public function render()
    {
        $this->continue('can_update_role_permissions');
        return view('livewire.authentication.roles-permissions');
    }

    public function getRolesProperty(){
        return Role::with('permissions')->get();
    }

    public function getPermissionsProperty(){
        return Permission::all();
    }

    public function give_permission_to_role($state,$role,$permission){
        $roleInst = Role::findByName($role);
        if(auth()->user()->can('can_update_role_permissions')){
            if($state === '0'){
                $roleInst->givePermissionTo($permission);
                regenerate_staff_cached_users();
                return $this->success("Role Permission Successfully Updated!");
            }else{
                $roleInst->revokePermissionTo($permission);
                regenerate_staff_cached_users();
                return $this->success("Role Permission Successfully Removed!");
            }

        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }
}
