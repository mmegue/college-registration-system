<?php

namespace App\Http\Livewire\Authentication;

use App\Http\Traits\Helper;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ViewUser extends Component
{
    use Helper;
    public $page_title = "View User";
    public $user;
    public $user_id;

    public $roles, $userroles;
    public $role;
    public $direct_permissions=[];
    public $role_permissions=[];
    public $update_user_info=false;
    public $update_roles = false;
    public $update_permissions = false;
    public $password;
    public $password_confirmation;
    public $permissions = [];
    public $all_user_permissions;

    public $all_roles, $all_permissions;

    protected $rules = [
        'user.title' => 'required|string|max:80',
        'user.username' => 'required|string|max:35',
        'user.name' => 'required|string|max:80',
        'user.surname' => 'required|string|max:80',
        'user.email' => 'required|email|max:150',
        'user.cell' => 'required|integer',
    ];


    public function mount($id){
        $this->continue('can_view_user');
        $this->user_id = $this->check_url_id($id);
        $this->user = User::find($this->user_id);
        if(empty($this->user)){
            abort(404);
        }
        $this->userroles = implode(',',$this->user->roles->pluck('name')->toArray());
        $this->all_roles = Role::all();
        $this->roles = $this->user->roles->pluck('name');

        $this->role_permissions = $this->user->getPermissionsViaRoles()->pluck('name');
        $this->direct_permissions = $this->user->getDirectPermissions()->pluck('name');
        $this->all_user_permissions = $this->user->getAllPermissions()->pluck('name');
        $this->all_permissions = Permission::all();
        //dd($this->permissions);
    }

    public function render()
    {
        return view('livewire.authentication.view-user');
    }

    public function toggle($state){
        if($state === "update_roles"){
            $this->update_roles = true;
            $this->update_permissions = false;
        }

        if($state === "update_permissions"){
            $this->update_permissions = true;
            $this->update_roles = false;
        }
    }

    public function update_roles_(){

        if(cn('can_change_roles')){
            $this->user->syncRoles($this->roles);
            regenerate_staff_cached_users();
            return $this->success("User Role successfully updated!");
        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));

    }

    public function give_permission($state,$permission){
        if(cn('can_change_permissions')){
            if($state === '0'){
                $this->user->givePermissionTo($permission);
                return $this->success("User Permission Successfully Updated!");
            }else{
                $this->user->revokePermissionTo($permission);
                return $this->success("User Permission Successfully Removed!");
            }

        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }

    public function update_permissions_(){
        if(cn('can_change_permissions')){
            $this->user->syncPermissions($this->permissions);
            return $this->success("User Permission successfully updated!");
        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }

    public function remove_all_permissions(){
        $this->user->syncPermissions([]);
        return $this->success("All User Permission successfully cleared!");
    }

    public function update_personal_info(){
        //$this->validate();
        if(cn('can_crud_users')){
            $this->user->save();
            return $this->success("User Info successfully updated!");
        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }

    public function update_user_password(){
        if(cn('can_crud_users') || cn('can_update_password')){
            $this->validate([
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
            ]);
            $this->user->forceFill([
                'password' => Hash::make($this->password),
            ])->save();
            return $this->success("User Password successfully updated!");
        }
        return $this->error(Setting::get_single_setting('unauthorised_error_message'));
    }
    
}
