<?php

namespace App\Http\Livewire\Authentication;

use App\Http\Traits\Helper;
use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class StudentsTable extends LivewireDatatable
{
    use Helper;

    public $hideable = 'select';

    public $exportable = true;

    public function builder()
    {
        $this->continue('can_view_users');
        return User::query()->role('student');
    }

    public function columns()
    {
        if(cn('can_crud_users') && cn('can_view_user')){
            return [

                Column::callback(['id'], function ($id) {
                    return '<a href="view-user/'.$this->hash($id).'" class="table-btn-action btn btn-sm btn-secondary"><i class="bi bi-gear"></i>&nbsp;Manage</a>';
                })->label('Action'),

                Column::name('users.username')
                    ->label('Student#')
                    ->filterable()
                    ->searchable(),

                Column::name('users.title')
                    ->label('Title')
                    ->filterable(['Mr','Mrs','Ms','Miss'])
                    ->searchable(),

                Column::name('users.name')
                    ->label('Name')
                    ->filterable()
                    ->searchable(),

                Column::name('users.surname')
                    ->label('Surname')
                    ->filterable()
                    ->searchable(),

                Column::name('users.cell')
                    ->label('Cell Number')
                    ->filterable()
                    ->searchable(),

                Column::name('users.email')
                    ->label('Email Address')
                    ->filterable(),

                DateColumn::name('created_at')
                    ->filterable()->hide(),

                DateColumn::name('updated_at')
                    ->filterable()->hide(),
            ];
        } elseif(cn('can_view_user')){
            return [

                Column::callback(['id'], function ($id) {
                    return '<a href="view-user/'.$this->hash($id).'" class="table-btn-action btn btn-sm btn-secondary"><i class="bi bi-gear"></i>&nbsp;Manage</a>';
                })->label('Action'),

                Column::name('users.username')
                    ->label('Student#')
                    ->filterable()
                    ->searchable(),

                Column::name('users.title')
                    ->label('Title')
                    ->filterable(['Mr','Mrs','Ms','Miss'])
                    ->searchable(),

                Column::name('users.name')
                    ->label('Name')
                    ->filterable()
                    ->searchable(),

                Column::name('users.surname')
                    ->label('Surname')
                    ->filterable()
                    ->searchable(),

                Column::name('users.cell')
                    ->label('Cell Number')
                    ->filterable()
                    ->searchable(),

                Column::name('users.email')
                    ->label('Email Address')
                    ->filterable(),

                DateColumn::name('created_at')
                    ->filterable()->hide(),

                DateColumn::name('updated_at')
                    ->filterable()->hide(),
            ];
        }else{
            return [

                Column::name('users.username')
                    ->label('Student#')
                    ->filterable()
                    ->searchable(),

                Column::name('users.title')
                    ->label('Title')
                    ->filterable(['Mr','Mrs','Ms','Miss'])
                    ->searchable(),

                Column::name('users.name')
                    ->label('Name')
                    ->filterable()
                    ->searchable(),

                Column::name('users.surname')
                    ->label('Surname')
                    ->filterable()
                    ->searchable(),

                Column::name('users.cell')
                    ->label('Cell Number')
                    ->filterable()
                    ->searchable(),

                Column::name('users.email')
                    ->label('Email Address')
                    ->filterable(),

                DateColumn::name('created_at')
                    ->filterable()->hide(),

                DateColumn::name('updated_at')
                    ->filterable()->hide(),
            ];
        }

    }

    private function get_current_user_roles($id){
        return User::find($id)->roles->pluck('name');
    }
}