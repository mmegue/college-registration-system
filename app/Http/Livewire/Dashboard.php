<?php

namespace App\Http\Livewire;

use App\Http\Traits\Helper;
use App\Models\Todo;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Dashboard extends Component
{
    use Helper,\App\Http\Traits\Settings;
    
    public function render()
    {
        //dd($this->count_invoices_with_reg_fee());
        return view('livewire.dashboard',[
            'page_title' => 'Dashboard'
        ]);
    }

    public function query(){
        return Auth::user()->quotation_modules
            ->where('status','Invoiced')->where('semester',$this->semester());
    }

    public function my_notifications(){
        return Auth::user()->unreadNotifications()
            ->get()->take(10);
    }

    public function total_modules(){
        return Auth::user()->quotation_modules->where('semester',$this->semester())
            ->sum('price');
    }
    

    public function invoices(){
        return Auth::user()->quotations_->where('status','Invoiced')->where('semester',$this->semester());
    }

    public function total_invoices(){
        return $this->query()->sum('price');
    }

    public function total_reg_fees(){
        return $this->query()
            ->where('service_type','reg-fee')->sum('price');
    }

    public function count_invoices(){
        return count(array_unique($this->query()->pluck('quotation_id')->toArray()));
    }

    public function count_invoices_with_reg_fee(){
        return $this->query()
            ->where('service_type','reg-fee')->count();
    }

    public function count_invoices_without_reg_fee(){
        return $this->query()
            ->where('service_type','!=','reg-fee')->count();
    }

    public function my_todos(){
        return Todo::my_todos();
    }
}
