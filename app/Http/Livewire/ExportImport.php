<?php

namespace App\Http\Livewire;

use App\Exports\Export;
use App\Http\Traits\Helper;
use App\Imports\Import;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class ExportImport extends Component
{
    use Helper, WithFileUploads;

    public $filename;
    public $file;
    public $upload;

    public $collection;
    public $headings;
    public $unique_by;
    public $model;
    public $import_column_fields;

    public $show_upload = false;

    public function render()
    {
        return view('livewire.export-import');
    }

    public function download_file(){
        return Excel::download(
            new Export($this->collection, $this->headings),
            $this->filename.'.xlsx'
        );
    }

    /* xport Model data to excel */
    public function upload_file(){
        $this->validate([
            'file' => 'required|max:512|mimes:xlsx', // 1MB Max
        ]);
        /*Import data to categories table*/
        Excel::import(
            new Import(
                $this->import_column_fields,
                $this->unique_by,
                $this->model
            ),
            $this->file
        );
        /*Close the modal box*/
        $this->close_upload_form();
        return back();
    }

    public function close_upload_form(){
        $this->show_upload = false;
    }
}
