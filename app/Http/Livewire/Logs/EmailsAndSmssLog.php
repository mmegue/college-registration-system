<?php

namespace App\Http\Livewire\Logs;

use Livewire\Component;

class EmailsAndSmssLog extends Component
{
    public $page_title = "Emails & SMSs Logs";

    public function render()
    {
        return view('livewire.logs.emails-and-smss-log');
    }
}
