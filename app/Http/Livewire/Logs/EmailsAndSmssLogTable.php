<?php

namespace App\Http\Livewire\Logs;

use App\Http\Traits\Helper;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class EmailsAndSmssLogTable extends LivewireDatatable
{
    use Helper;
    public $hideable = 'select';
    public function builder()
    {
        $this->continue('can_view_smsemail_logs');
        return \App\Models\EmailsAndSmssLog::query();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->defaultSort('desc'),
            Column::name('cell_number')->label('Cell #')->filterable()->searchable(),
            Column::name('sent_by')->label('Initiated By'),
            Column::name('subject')->label('Subject')->searchable(),
            Column::name('message')->label('Message'),
            Column::name('log_type')->label('Type')->searchable(),
            DateColumn::name('created_at')->filterable(),
        ];
    }
}