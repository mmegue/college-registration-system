<?php

namespace App\Http\Livewire\Logs;

use Livewire\Component;

class GeneratedClassListLogs extends Component
{
    public $page_title = "Generated Classlist";

    public function render()
    {
        return view('livewire.logs.generated-class-list-logs');
    }
}
