<?php

namespace App\Http\Livewire\Logs;

use App\Http\Traits\Helper;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class GeneratedClassListLogsTable extends LivewireDatatable
{
    use Helper;
    public $hideable = 'select';

    public function builder()
    {
        $this->continue('can_view_classlists_logs');
        return \App\Models\GeneratedClassListLog::query()
            ->join('users','users.id','=','generated_class_list_logs.initiated_by');
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->defaultSort('desc'),
            Column::name('users.username')->label('Initiated By')->filterable()->searchable(),
            Column::name('details')->label('Details'),
            DateColumn::name('created_at')->filterable(),
        ];
    }
}