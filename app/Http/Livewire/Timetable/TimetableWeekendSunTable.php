<?php

namespace App\Http\Livewire\Timetable;

use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class TimetableWeekendSunTable extends LivewireDatatable
{
    /*** Datatable Configurations ***/
    public $hideable = 'select';
    public $exportable = true;
    public $model = \App\Models\TimetableWeekendSun::class;

    protected $listeners = ['refresh' => '$refresh'];

    public function builder()
    {
        return \App\Models\TimetableWeekendSun::query();
    }

    public function columns()
    {
        return [
            Column::delete()->label('delete')->alignRight()->hide(),
            NumberColumn::name('id')->filterable()->defaultSort('asc')->editable()->hide(),
            Column::name('day')->label('Day')->editable(),
            Column::name('time')->label('Time')->editable(),
            Column::name('slot0')->label('slot0')->editable(),
            Column::name('slot1')->label('slot1')->editable(),
            Column::name('slot2')->label('slot2')->editable(),
            Column::name('slot3')->label('slot3')->editable(),
            Column::name('slot4')->label('slot4')->editable(),
            Column::name('slot5')->label('slot5')->editable(),
            Column::name('slot6')->label('slot6')->editable(),
            Column::name('slot7')->label('slot7')->editable(),
            Column::name('slot8')->label('slot8')->editable(),
            Column::name('slot9')->label('slot9')->editable(),
            Column::name('slot10')->label('slot10')->editable(),
            Column::name('slot11')->label('slot11')->editable(),
            Column::name('slot12')->label('slot12')->editable(),
            Column::name('slot13')->label('slot13')->editable(),
            Column::name('slot14')->label('slot14')->editable(),
            Column::name('slot15')->label('slot15')->editable(),
        ];
    }

}