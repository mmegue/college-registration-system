<?php

namespace App\Http\Livewire\Timetable;

use Illuminate\Support\Facades\App;
use Livewire\Component;

class TimetableWeekdaySun extends Component
{
    public function render()
    {
        return view('livewire.timetable.timetable-weekday-sun');
    }

    public $page_title = "Timetable Weekday Sun";
    protected $listeners = ['refresh' => '$refresh'];

    public function download_this_timetable(){
        $data = \App\Models\TimetableWeekdaySun::all();
        $title = 'Noble Tutors Weekday Sun Timetable';

        return response()->streamDownload(function () use($data,$title){
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('livewire.timetable.timetable-pdf-template',['data'=>$data, 'title' =>  $title]);
            $pdf->setPaper('A4', 'landscape');
            echo $pdf->stream();
        }, $title.'.pdf');
        //$this->download_timetable('Noble Tutors Weekday Timetable', \App\Models\Timetable::all());
    }

    public function add_row(){
        \App\Models\TimetableWeekdaySun::create([
            'day'=>'dummy',
            'time' => 'dummy',
            'slot0' => 'dummy',
            'slot1' => 'dummy',
            'slot2' => 'dummy',
            'slot3' => 'dummy',
            'slot4' => 'dummy',
            'slot5' => 'dummy',
            'slot6' => 'dummy',
            'slot7' => 'dummy',
            'slot8' => 'dummy',
            'slot9' => 'dummy',
            'slot10' => 'dummy',
            'slot11' => 'dummy',
            'slot12' => 'dummy',
            'slot13' => 'dummy',
            'slot14' => 'dummy',
            'slot15' => 'dummy',
        ]);

        $this->emit('refresh');
    }
}
