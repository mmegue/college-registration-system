<?php

namespace App\Http\Livewire\Todos;

use App\Http\Traits\Helper;
use App\Models\Todo;
use App\Models\User;
use App\Notifications\TodoAdded;
use Auth;
use Livewire\Component;
use Livewire\WithPagination;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Notification;

class Todos extends Component
{
    use Helper, WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $page_title = "Todos",$state = "All";

    public $title,$priority, $start_date, $end_date, $description, $status, $important,$searchTerm;
    public $roles=[],$users=[],$create = false, $edit_todo = false,$model_id;
    public $test;

    public function render()
    {
        return view('livewire.todos.todos');
    }

    public function SendTodoNotificationx(){
         //dd("$this->test");
    }

    public function show_edit_form($id){
        if(cn('can_crud_todo')  || cn('can_edit_todo')){
            $this->model_id = $id;
            $this->create = true;
            $this->edit_todo = true;
            $model = Todo::find($id);
            $this->title = $model->title;
            $this->priority = $model->priority;
            $this->start_date = $model->start_date;
            $this->end_date = $model->end_date;
            $this->description = $model->description;
        }
    }

    public function create_todo(){
        if(cn('can_crud_todo')  || cn('can_create_todo')){
            $validated_data = $this->validate([
                'title' => 'required|max:100',
                'priority' => 'required',
                'start_date' => 'required|date|after:yesterday',
                'end_date' => 'required|date|after:start_date',
                'description' => 'required|max:500',
            ]);
            $validated_data['created_by']=Auth::id();
            $batch_code = date('YmdHis');
            $validated_data['batch_code']=$batch_code;
            $count = 0;

            if(!empty($this->roles)){
                foreach($this->roles as $role){
                    $role_name = Role::findByName($role)->name;
                    $users = User::get_users_with_roles([$role_name]);
                    foreach($users as $user){
                        $validated_data['assigned_to']=$user->id;
                        $exists = $this->check_if_todo_exists($batch_code,$user->id);
                        if(!$exists){
                            $todo = Todo::create($validated_data);
                            User::find($user->id)->notify(new TodoAdded($todo));
                            $count++;
                        }
                        unset($validated_data['assigned_to']);
                    }
                }
            }

            if(!empty($this->users)){
                foreach($this->users as $user){
                    $validated_data['assigned_to']=$user;
                    $exists = $this->check_if_todo_exists($batch_code,$user);
                    if(!$exists){
                        $todo = Todo::create($validated_data);
                        User::find($user)->notify(new TodoAdded($todo));
                        $count++;
                    }
                    unset($validated_data['assigned_to']);
                }
            }
            $this->create = false;
            $this->success("Todo Successfully created..".$count);
        }
    }

    public function check_if_todo_exists($batch_code,$assigned_to){
        return  Todo::check_to_existance($batch_code,$assigned_to);
    }

    public function is_trashed() {
        return ($this->state === 'Trashed');
    }

    public function my_todos(){
        switch ($this->state) {
            case 'Pending':
                return Todo::get_status('Pending');
                break;
            case 'Completed':
                return Todo::get_status('Completed');
                break;
            case 'Trashed':
                return Todo::get_trashed();
                break;
            case 'Important':
                return Todo::get_important();
                break;
            case 'High':
                return Todo::get_priority('High');
                break;
            case 'Medium':
                return Todo::get_priority('Medium');
                break;
            case 'Low':
                return Todo::get_priority('Low');
                break;
            default:
                $searchTerm = '%'.$this->searchTerm.'%';
                return Todo::all_todos($searchTerm);
                break;
        }

    }

    public function get_state($state){
        $this->state = $state;
    }

    public function assign_priority($id, $priority){
        if(!$this->is_trashed()) {
            Todo::assign_priority($id, $priority);
            $this->success("Priority Assigned Successfully");
        }
    }

    public function update_todo(){
        if(cn('can_crud_todo') || cn('can_edit_todo')){
            $model = Todo::find($this->model_id);
            $model->title = $this->title;
            $model->priority = $this->priority;
            $model->start_date = $this->start_date;
            $model->end_date = $this->end_date;
            $model->description = $this->description;
            $model->save();
            $this->create = false;
            $this->success("Todo List Updated Successfully");
            return ;
        }
         $this->auth_error();
    }

    public function update_status($id){
        if (!$this->is_trashed()) {
            $status = Todo::find($id)->status;
            if ($status === "Pending") {
                Todo::assign_status($id, 'Completed');
            } else {
                Todo::assign_status($id, 'Pending');
            }
            $this->success("Priority Assigned Successfully");
        }
    }

    public function assign_importance($id){
        if(!$this->is_trashed()){
            $todo =  Todo::find($id);
            if(Todo::where('importance',1)->where('id',$id)->exists()){
                $todo->importance = false;
            }else{
                $todo->importance = true;
            }
            $todo->save();
        }
    }

    public function assigned_to($id){
        $user = User::find($id);
        if(!is_null($user)){
            //return $user->username;
            return $user->name." ".$user->surname;
        }
        return "UnKnown";
    }

    public function delete_todo($id){
        if(Todo::check_auth()) {
            Todo::soft_delete_todo($id);
            $this->success("Todo Successfully Removed!");
        }
    }

    public function permanently_delete_todo($id){
        if(Todo::check_auth()) {
            Todo::permanent_delete_todo($id);
            $this->success("Todo Permanently Removed!");
        }
    }

    public function restore_todo($id){
        if(Todo::check_auth()) {
            Todo::restore_deleted_todo($id);
            $this->success("Todo Successfully Restored!");
        }
    }


}
