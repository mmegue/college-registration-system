<?php

namespace App\Http\Livewire\BulkAllocation;

use App\Models\QuotationModule;
use Auth;
use Livewire\Component;

class BulkAllocations extends Component
{
    public $page_title = "Bulk Allocations";
    public $bulkallocation;

    protected $listeners = ['refresh' => '$refresh'];

    public function render()
    {
        return view('livewire.bulk-allocation.bulk-allocations');
    }

    public function updatedBulkallocation($value){
        (new BulkAllocationTable)->connection($value);
    }

}
