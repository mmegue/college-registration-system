<?php

namespace App\Http\Livewire\BulkAllocation;

use App\Http\Traits\Helper;
use App\Models\QuotationModule;
use App\Models\ServiceType;
use App\Models\User;
use Auth;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class BulkAllocationTable extends LivewireDatatable
{
    use Helper;

    public $hideable = 'select';
    public array $array = [];
    public $lecturer,$attendance;
    public $bulkallocation;
    public array $lecturerbulk = [];
    protected $listeners = ['refresh'=>'$refresh'];

    public function builder()
    {
        if(auth_user_has_role('super-admin')){
           return (new QuotationModule)->AllQuoteModulesWithAssociatedData();
        }

        return (new QuotationModule)->AllQuoteModulesWithAssociatedData()
           ->where('uB.username',cached_auth_user_id());
    }
    
    public function columns()
    {

        return [
            Column::callback(['quotation_modules.lecturer_id_default','quotation_modules.module_code','quotation_modules.id','uC.username'],
                function ($lecturer_id_default,$module, $quotation_modules_id,$username) {
                    return view('livewire.bulk-allocation.allocations',
                        ['module'=>$module, 'lecturer_id_default'=>$lecturer_id_default, 'username'=>$username,'quotation_modules_id'=>$quotation_modules_id]
                    );
                })->label('AllocatedTo'),

            Column::callback(['quotation_modules.id','quotation_modules.attendance'],
                function ($quotation_modules_id,$attendance) {
                    return view('livewire.bulk-allocation.attendance',
                        ['quotation_modules_id'=>$quotation_modules_id, 'attendance'=>$attendance]
                    );
                })->label('Attendance'),

            Column::name('U.title')->label('Title'),
            Column::name('U.name')->label('Name')->searchable()->filterable(),
            Column::name('U.surname')->label('Surname')->searchable()->filterable(),
            Column::name('Q.quotation_number')->label('Quote#')->searchable()->filterable(),
            Column::name('Q.invoice_number')->label('Invoice#')->searchable()->filterable(),
            Column::name('quotation_modules.module_code')->label('ModuleCode')->filterable($this->modules->pluck('module_code'))->searchable(),
            Column::name('quotation_modules.service_type')->label('ServiceT')->filterable($this->servicetypes->pluck('slug'))->searchable(),
            Column::name('uB.username')->label('QCreatedBy')->filterable($this->users->pluck('username')),
            Column::name('uA.username')->label('MAddedBy')->filterable($this->users->pluck('username')),
            Column::name('uC.username')->label('DefaultLect')->filterable($this->users->pluck('username')),
            Column::name('quotation_modules.attendance')->label('Attendance')->filterable()->searchable(),
        ];
    }



    public function updatedLecturer($value){
        $array = explode('*',$value);
        $this->this_update($array[1],'lecturer_id_default',$array[0]);
        $this->emit('refresh');
    }

    public function updatedAttendance($value){
        $array = explode('*',$value);
        $this->this_update($array[1],'attendance',$array[0]);
        $this->emit('refresh');
    }

    public function this_update($id,$item,$value){
        QuotationModule::find($id)
            ->update([$item=>$value]);
    }
}