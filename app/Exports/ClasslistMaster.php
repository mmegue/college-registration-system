<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ClasslistMaster implements FromArray, Withheadings
{
    public $data=[];

    public function __construct($data){
        $this->data = $data;
        //dd($this->data);
    }

    /**
     * @return array
     */
    public function array():array
    {
        return $this->data;
    }

    /** For Headings */
    public function headings(): array
    {
        return [
            "Modules" ,
            "Invoiced" ,
            "Paid" ,
            "Balance",
            "PMT",

            "Module" ,
            "ServiceType",
            "Price" ,
            "Attendance" ,
            "Semester" ,
            "Cell" ,
            "Status" ,

            "Quotation #" ,
            "Invoice #" ,
            
            "Title" ,
            "Username" ,
            "Name" ,
            "Surname" ,
            "Email" ,
            "Myunisa PWD" ,
            "Quote Module Added By" ,
            "Quote Created By" ,
            "Lecturer Default" ,
        ];
    }
}
    