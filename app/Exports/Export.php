<?php

namespace App\Exports;

use App\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Export implements FromCollection, WithHeadings
{
    public $collection;
    public array $headings;

    public function __construct($collection, array $headings){
        $this->collection = $collection;
        $this->headings = $headings;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->collection;
    }

    /** For Headings */
    public function headings(): array
    {
        return $this->headings;
    }

}
