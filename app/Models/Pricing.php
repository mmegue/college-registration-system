<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pricing extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'module_code',
        'service_type',
        'price',
        'created_by',
    ];

    /**
     * Scope a query to only modules.
     * @param $query
     * @param $module_code
     * @return mixed
     */
    public function scopeModule($query, $module_code){
        return $query->where('module_code',$module_code);
    }

    /**
     * Scope a query to only service types.
     * @param $query
     * @param $service_type
     * @return mixed
     */
    public function scopeServiceType($query, $service_type){
        return $query->where('service_type',$service_type);
    }

    /**
     * scope a query with modules and service types
     * @param $query
     * @param $module_code
     * @param $service_type
     * @return mixed
     */
    public function scopeModuleServiceType($query, $module_code,$service_type){
        return $query->module($module_code)
            ->serviceType($service_type);
    }

    /**
     * Get the module and service type price
     * @param $query
     * @param $module_code
     * @param $service_type
     * @return int
     */
    public function scopePrice($query, $module_code,$service_type){
        $price = $query->moduleServiceType($module_code, $service_type)
                       ->first();
        if(empty($price)){
            return  0;
        }
       return $price->price;
    }

    /**
     * check record existence with module and service type
     * @param $query
     * @param $module_code
     * @param $service_type
     * @return mixed
     */
    public function scopeExists($query, $module_code,$service_type){
        return $query->moduleServiceType($module_code, $service_type)
                     ->exists();
    }

    /**
     * @param $module_code
     * @return mixed
     */
    public function scopeServiceTypesPerModule($query, $module_code){
        return $query->module($module_code)->get()->sortBy('service_type');
    }

    
}
