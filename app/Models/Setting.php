<?php

namespace App\Models;

use App\Helper\Cached;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function store($key,$value){
        self::create([
            'key' => $key,
            'value' => json_encode($value),
        ]);
        //return $model;
    }

     public static function edit($key, $value){
         $model = self::where('key',$key)->first();
         $model->value = json_encode($value);
         $model->save();
         //return $model;
     }

    /**
     * check if system setting exists
     * @param $key
     * @return boolean
     */
     public static function check($key){
        return !empty(
            collect(cached_settings())
            ->firstWhere('key',$key)
        );
     }

     public static function get_single_setting($key){
         $check_setting_key = self::check($key);
         if($check_setting_key){
            $json = get_setting($key);
            return $json[$key];
         }
         //return false;
     }

    public static function get($key){
        $check_setting_key = self::check($key);
        if($check_setting_key){
            return json_decode(
                        json_encode(
                            get_setting($key)
                        )
                  );
        }
        //return false;

    }

}
