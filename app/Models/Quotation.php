<?php

namespace App\Models;

use App\Http\Livewire\Transaction\Transactions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quotation extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];


    public function quotation_modules(){
        return $this->hasMany(QuotationModule::class);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }


    public function payment_arrangments(){
        return $this->hasMany(PaymentArrangment::class);
    }

    public function transactions(){
        return $this->hasManyThrough( Transaction::class,User::class,'id','user_id');
    }

    public function scopeQuotationRelatedData($query){
        return $query->Quotation::with(['user','quotation_modules','payment_arrangments']);
    }

}
