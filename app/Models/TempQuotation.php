<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempQuotation extends Model
{
    use HasFactory;

    protected $fillable = [
        'module_code','service_type', 'attendance', 'price', 'cell', 'added_by',
    ];

    public function scopeTempQuoteModule($query, $cell){
        return $query->where('cell',$cell)->where('added_by',Auth::id());
    }

    public function scopeAllTempVariables($query,$module_code, $service_type, $cell, $by){
        return $query->where('module_code',$module_code)
            ->where('service_type',$service_type)->where('cell', $cell)
            ->where('added_by', $by);
    }

    public function scopeExist($query, $module_code, $service_type, $cell, $by){
        return $query->allTempVariables($module_code, $service_type, $cell, $by)
            ->exists();
    }

    public function scopeCellModule($module_code,$cell){

    }

    public function scopeTotal($query, $cell){
         return $query->tempQuoteModule($cell)->sum('price');
    }

    public function scopeRegfee($query, $cell){
        return $query->tempQuoteModule($cell)->where('service_type', 'reg-fee');
    }
}
