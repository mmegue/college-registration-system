<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Livewire\WithPagination;

class Todo extends Model
{
    use HasFactory, softDeletes;

    const PAGINATE_VALUE = 5;

    protected $guarded = [];

    public static function check_to_existance($batch_code,$assigned_to){
        return self::where('assigned_to',$assigned_to)
            ->where('batch_code',$batch_code)
            ->exists();
    }

    public static function all_todos($search=null){
        if(self::check_auth()){
           return self::latest()->where('title','like',$search)
               ->orWhere('description','like',$search)->paginate(self::PAGINATE_VALUE);
        }
        //dd(self::latest()->where('assigned_to',Auth::id())->get());
        return self::latest()->where('assigned_to',Auth::id())->where('description','like',$search)
            ->paginate(self::PAGINATE_VALUE);
    }

    public static function my_todos(){
        return self::latest()->where('assigned_to',Auth::id())
            ->paginate(self::PAGINATE_VALUE);
    }

    public static function get_status($status){
        if(self::check_auth()) {
            return self::where('status', $status)
                ->paginate(self::PAGINATE_VALUE);
        }
        return self::where('status', $status)->where('assigned_to',Auth::id())
            ->paginate(self::PAGINATE_VALUE);

    }

    public static function get_trashed(){
       if(self::check_auth()){
           return self::onlyTrashed()
               ->paginate(self::PAGINATE_VALUE);
       }

        return self::onlyTrashed()->where('assigned_to',Auth::id())
            ->paginate(self::PAGINATE_VALUE);
    }

    public static function get_important(){
        if(self::check_auth()){
            return self::where('importance',true)
                ->paginate(self::PAGINATE_VALUE);
        }
        return self::where('importance',true)->where('assigned_to',Auth::id())
            ->paginate(self::PAGINATE_VALUE);
    }

    public static function check_auth():bool{
        return  (Auth::user()->can('can_crud_todo') || Auth::user()->hasRole('super-admin'));
    }

    public static function get_priority(string $string){
        if(self::check_auth()){
            return self::where('priority',$string)
                ->paginate(self::PAGINATE_VALUE);
        }

        return self::where('priority',$string)->where('assigned_to', Auth::id())
            ->paginate(self::PAGINATE_VALUE);
    }

    public static function soft_delete_todo($id){
        return  self::find($id)->delete();
    }

    public static function permanent_delete_todo($id){
        return  self::onlyTrashed()->find($id)->forceDelete();
    }

    public static function restore_deleted_todo($id){
        return  self::onlyTrashed()->find($id)->restore();
    }

    public static function get_deleted_todos(){
        if(self::check_auth()){
            return self::onlyTrashed()
                ->paginate(self::PAGINATE_VALUE);
        }
        return self::onlyTrashed()->where('created_by',Auth::id())
            ->paginate(self::PAGINATE_VALUE);
    }

    public static function assign_priority($id,$priority){
        $todo =  self::find($id);
        $todo->priority = $priority;
        $todo->save();
    }

    public static function assign_status($id,$status){
        $todo =  self::find($id);
        $todo->status = $status;
        $todo->save();
    }

}
