<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'category_name', 'category_description', 'sequence', 'created_by',
    ];

     public function modules(){
         return $this->hasMany(Module::class);
     }

     public function created_by_users(){
         return $this->belongsTo(User::class, 'created_by');
     }
     
}