<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteGenerator extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getRolloverAttribute($value)
    {
        return str_pad($value, 3, '0', STR_PAD_LEFT);
    }
}
