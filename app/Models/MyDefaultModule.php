<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class MyDefaultModule extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'module_id',
        'category_id',
        'user_id',
    ];

    public function scopeMydefaultCategoryModules($query, $category_id){
        return $query->join('modules','modules.id','=','my_default_modules.module_id')
            ->where('my_default_modules.user_id',Auth::id())
            ->where('my_default_modules.category_id',$category_id);
    }

    public function scopeMydefaultCategories($query){
       return $query->join('categories','categories.id','=','my_default_modules.category_id')
            ->where('my_default_modules.user_id',Auth::id())->distinct()->get(['category_id','category_name']);
    }


}
