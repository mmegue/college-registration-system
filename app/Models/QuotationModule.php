<?php

namespace App\Models;

use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuotationModule extends Model
{
    use HasFactory, SoftDeletes, Settings, Helper;

    protected $guarded = [];

    public function AllQuoteModulesWithAssociatedDataNoSem(){
        return $this->join('quotations AS Q','Q.id','=','quotation_modules.quotation_id')
            ->join('users AS U','U.id','=','Q.user_id')
            ->join('users AS uA','uA.id','=','quotation_modules.added_by')
            ->join('users AS uB','uB.id','=','Q.created_by')
            ->leftjoin('users AS uC','uC.id','=','quotation_modules.lecturer_id_default')
            ->select(
                'quotation_modules.*','Q.*','uC.username',
                'U.username','U.title','U.name','U.surname','U.cell','U.email','U.myunisa_pwd',
                'uA.username as quote_module_added_by',
                'uB.username as quote_created_by',
                'uC.username as lecturer_default'
            );
    }

    public function AllQuoteModulesWithAssociatedData(){
        return $this->AllQuoteModulesWithAssociatedDataNoSem()
            ->where('quotation_modules.semester',$this->semester());
    }

    public function AllQuoteModulesWithAssociatedDataClassList(){
        return $this->AllQuoteModulesWithAssociatedDataNoSem();
    }

    public function total_invoiced($cell){
        return $this->join('quotations','quotations.id','=','quotation_modules.quotation_id')
            ->where('quotation_modules.semester',$this->semester())
            ->where('quotation_modules.status','Invoiced')
            ->where('quotation_modules.cell',$cell)->sum('price');
    }

    public function exist($quote_id, $module, $service_type):bool{
        return $this->where('quotation_id',$quote_id)
            ->where('module_code',$module)
            ->where('service_type', $service_type)->exists();
    }

    public function quotation(){
        return $this->belongsTo(Quotation::class);
    }

    public function user(){
        return $this->quotation->user();
    }

    public function scopeAllWithUserQuotationCurrentSemester($query){
        return $query->join('quotations','quotations.id','=','quotation_modules.quotation_id')
            ->join('users','users.id','=','quotation_modules.added_by')
            ->leftjoin('users AS uC','uC.id','=','quotation_modules.lecturer_id_default')
            ->where('quotation_modules.semester',$this->semester())
            ->select(
                'users.*','quotation_modules.*','quotations.*','uC.username as lect'
            );
    }

    public function scopeAllWithUserAndQuotataion($query, $cell){
        return $query->allWithUserQuotationCurrentSemester()
            ->where('quotation_modules.cell',$cell);
    }
}
