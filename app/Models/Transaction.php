<?php

namespace App\Models;

use App\Http\Traits\Helper;
use App\Http\Traits\Settings;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory, Helper, Settings;

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class,'cell','cell_number');
    }

    public function user_transactions($cell){
        return $this->join('users','users.cell','=','transactions.cell_number')
                    ->join('users as u','u.id','=','transactions.user_id')
            ->where('transactions.cell_number',$cell)
            ->where('semester_period',$this->semester())
            ->select(['users.*','transactions.*','u.username']);
    }

    public function amount_paid($cell){
        return $this->whereCellNumber($cell)
            ->whereSemesterPeriod($this->semester())
            ->sum('amount');
    }
}
