<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Note extends Model
{
    use HasFactory, softDeletes;

    protected $guarded = [];


    public static function get_tag($state){
         return self::where('user_id',Auth::id())->where('tag',$state)->paginate(12);
    }

    public static function get_fav(){
        return self::where('user_id',Auth::id())->where('favourite',true)->paginate(12);
    }

    public static function all_notes(){
        return self::where('user_id',Auth::id())->paginate(5);
    }

    public static function create_note($array){
        return self::create([
            'user_id' => Auth::id(),
            'title' => $array->title,
            'description' => $array->description,
        ]);
    }
}
