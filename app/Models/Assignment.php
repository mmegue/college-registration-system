<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function student(){
        return $this->belongsTo(User::class, 'id','student_id');
    }

    public function lecturer(){
        return $this->belongsTo(User::class, 'id','lecturer_id');
    }

    public function get_assignment($id){
        return  $this->assignment()
            ->where('assignments.id',$id)->first();
    }

    public function scopeAssignment($query){
        return $query->join('users as usersLecturer', 'usersLecturer.id','=','assignments.lecturer_id')
            ->join('users as usersStudent', 'usersStudent.id','=','assignments.student_id');
            //->join('quotation_modules', 'quotation_modules.id','=','assignments.quotation_modules_id');
    }
}
