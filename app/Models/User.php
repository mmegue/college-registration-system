<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','name','surname','cell',
        'email','username','myunisa_pwd',
        'password','myit_pwd',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class,'created_by');
    }

    public function quotations_(){
        return $this->hasMany(Quotation::class);
    }

    public function quotation_modules_(){
        return $this->hasManyThrough(QuotationModule::class, Quotation::class, 'user_id','quotation_id');
    }

    public function transactions(){
        return $this->hasMany(Transaction::class,'cell_number','cell');
    }

    public static function get_added_by($id){
        return  optional(User::find($id))->username;
    }

    public function tempquotations(){
        return $this->hasMany(TempQuotation::class, 'added_by');
    }

    public function quotation_modules(){
        return $this->hasManyThrough(QuotationModule::class, Quotation::class, 'created_by','quotation_id');
    }

    public function chats(){
        return $this->hasMany(Chat::class);
    }

    public function categories_created(){
        return $this->hasMany(Category::class, 'created_by');
    }

    public static function all_users_with_roles(){
        return self::with('roles')->get();
    }

    public static function get_user_and_roles($id){
        return self::find($id)->with('roles');
    }

    public static function get_users_with_roles(array $roles){
        return self::role($roles)->get();
    }

    public static function get_users_without_roles_(array $roles){
        return Role::all()->whereNotIn('name', $roles)->get();
    }

    public static function get_users_without_roles($roles){
       return self::notRole($roles)->get();
    }

    public function scopeNotRole(Builder $query, $roles, $guard = null): Builder
    {
        if ($roles instanceof Collection) {
            $roles = $roles->all();
        }

        if (! is_array($roles)) {
            $roles = [$roles];
        }

        $roles = array_map(function ($role) use ($guard) {
            if ($role instanceof Role) {
                return $role;
            }

            $method = is_numeric($role) ? 'findById' : 'findByName';
            $guard = $guard ?: $this->getDefaultGuardName();

            return $this->getRoleClass()->{$method}($role, $guard);
        }, $roles);

        return $query->whereHas('roles', function ($query) use ($roles) {
            $query->where(function ($query) use ($roles) {
                foreach ($roles as $role) {
                    $query->where(config('permission.table_names.roles').'.id', '!=' , $role->id);
                }
            });
        });
    }

    public function get_username($id){
        $user = optional($this->find($id));
        return ($user->username === "" || is_null($user->username)) ?
               substr($user->name,0,1)." ".$user->surname
            :  ucfirst($user->username);
    }

    public function get_fullnames($id){
        $user = optional($this->find($id));
       return $user->name." ".$user->surname;
    }

    public function student_assignments(){
       return $this->hasMany(Assignment::class,'student_id');
    }

    public function lecturer_assignments(){
        return $this->hasMany(Assignment::class,'lecturer_id');
    }

    public function company(){
        return $this->hasOne(CompanyDetail::class);
    }

}
