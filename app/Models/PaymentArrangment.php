<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentArrangment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function quotation(){
        return $this->belongsTo(Quotation::class);
    }

    public function payment_arrangements_with_addedby($quote_id){
        return $this->join('users','users.id','=','payment_arrangments.added_by')
            ->where('payment_arrangments.quotation_id',$quote_id)
            ->get([ 'payment_arrangments.*','users.username' ]);
    }
}
