@props([
    'type' => 'text',
    'disabled' => false,
    'label' => '',
    'required' => false,
    'autocomplete' => false,
])

@php
    $model = $model = $attributes->wire('model')->value()
@endphp

<div class="form-group row">
    <div class="col-sm-6">
        <input
                type="{{$type}}"
                {{ $disabled ? 'disabled' : '' }}
                {!! $attributes->merge(['class' => 'form-control form-control-md']) !!}
                @if ($required) required @endif
                value="{{ old($model,  isset($model) ? $model : null) }}"
             
        />
    </div>

    <label for="{{$model}}" class="col-sm-6 col-form-label col-form-label-md">{{ $slot }} &nbsp;<br>
        @include('components.error',['name'=>$model])</label>
</div>
