@props([
    'disabled' => false,
    'label' => '',
    'required' => false,

    'autocomplete' => false,
])

@php
    $model = $attributes->whereStartsWith('wire:model')->first()
@endphp

<div class="form-group row">
    <div class="col-sm-6">
        <textarea
                {{ $disabled ? 'disabled' : '' }}
                {!! $attributes->merge(['class' => 'form-control form-control-md']) !!}
                @if ($required) required @endif
                value="{{ old($model,  isset($model) ? $model : null) }}"
          
        ></textarea>
    </div>
    <label for="{{$model}}" class="col-sm-6 col-form-label col-form-label-md">{{ $slot }} &nbsp;<br>
        @include('components.error',['name'=>$model])</label>
</div>
