
@props([
    'type' => 'button',
])

<button  {!! $attributes->merge(['class' => 'btn']) !!}>{{ $slot }}</button>

