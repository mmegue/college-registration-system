<div>
    @props(['id' => null, 'maxWidth' => null])

    <x-modal :id="$id" :maxWidth="$maxWidth" {{ $attributes }}>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-danger"><i class="fa fa-times"></i></span>
                </button>
            </div>
            <div class="modal-body">
                {{ $content }}
            </div>
            <div class="modal-footer bg-light">
                {{ $footer }}
            </div>
        </div>
    </x-modal>
</div>