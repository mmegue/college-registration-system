@props([
    'disabled' => false,
    'required' => false,
    'autocomplete' => false,
])

@php
    $model = $attributes->whereStartsWith('wire:model')->first()
@endphp

<div class="form-group row mb-1">
    <div class="col-sm-12">
        <textarea
                {{ $disabled ? 'disabled' : '' }}
                {!! $attributes->merge(['class' => 'form-control form-control-md']) !!}
                @if ($required) required @endif
                value="{{ old($model,  isset($model) ? $model : null) }}"

        ></textarea>
    </div>
</div>
