@props([
    'type' => 'text',
    'disabled' => false,
    'required' => false,
    'autocomplete' => false,
])

@php
    $model = $attributes->wire('model')->value()
@endphp

<div class="form-group row mb-1">
    <div class="col-sm-12">
        <input
                type="{{$type}}"
                {{ $disabled ? 'disabled' : '' }}
                {!! $attributes->merge(['class' => 'form-control form-control-md']) !!}
                @if ($required) required @endif
                value="{{ old($model,  isset($model) ? $model : null) }}"
        />
        @include('components.error',['name'=>$model])
    </div>
</div>
