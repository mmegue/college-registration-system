<!--  BEGIN SIDEBAR  -->
<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">

        <ul class="navbar-nav theme-brand flex-row  text-center">
            <li class="nav-item theme-text">
                <a href="{{route('admin.dashboard')}}" class="nav-link"> {{config('app.name')}} </a>
            </li>
            <li class="nav-item toggle-sidebar">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather sidebarCollapse feather-chevrons-left"><polyline points="11 17 6 12 11 7"></polyline><polyline points="18 17 13 12 18 7"></polyline></svg>
            </li>
        </ul>
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu @if(request()->segment(2)==='dashboard') active @endif">
                @if(auth()->user()->hasrole('student'))
                    <a href="{{route('student.dashboard')}}" @if(request()->segment(2)==='dashboard') aria-expanded="true" @endif class="dropdown-toggle">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            <span>Dashboard</span>
                        </div>
                    </a>
                @else
                    <a href="{{route('admin.dashboard')}}" @if(request()->segment(2)==='dashboard') aria-expanded="true" @endif class="dropdown-toggle">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            <span>Dashboard</span>
                        </div>
                    </a>
                @endif
            </li>

            {{-- REGISTRATION --}}
            @if(cn('can_request_quote') || cn('can_view_quote'))
                @php $registration_status = (
                                                request()->segment(2)==='request-quote' ||
                                                request()->segment(2)==='quotations' ||
                                                request()->segment(2)==='registrations' ||
                                                request()->segment(2)==='view-quote'
                                            )
                @endphp
                <li class="menu @if($registration_status) active @endif" >

                    <a href="#registration" data-toggle="collapse" @if($registration_status) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.book')
                            <span>Registrations</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>

                    <ul class="collapse submenu list-unstyled @if($registration_status) recent-submenu mini-recent-submenu show @endif" id="registration" data-parent="#accordionExample">
                        @can('can_request_quote')
                            <li @if(request()->segment(2)==='request-quote') class="active" @endif>
                                @if(auth()->user()->hasRole('student')) <a href="{{route('student.requestquote')}}"> Request Quotation </a> @else
                                    <a href="{{route('requestquote')}}"> Request Quotation </a>
                                @endif
                            </li>
                        @endcan

                        @can('can_view_quote')
                            <li @if(request()->segment(2)==='quotations' || request()->segment(2)==='view-quote') class="active" @endif>
                                @if(auth()->user()->hasRole('student'))<a href="{{route('student.quotations')}}"> My Quotations </a>@else<a href="{{route('quotations')}}"> All Quotations </a>@endif
                            </li>
                        @endcan

                        @can('can_view_registration')
                            <li @if(request()->segment(2)==='registrations') class="active" @endif>
                                <a href="{{route('registrations')}}"> All Registrations </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endif

            {{-- AUTH & SECURITY --}}
            @if(cn('can_view_users') || cn('can_change_roles') || cn('can_change_permissions') || cn('can_update_role_permissions') || cn('can_crud_users'))
                @php $auth = (request()->segment(2)==='users' || request()->segment(2)==='students' || request()->segment(2)==='roles-permissions'); @endphp
                <li class="menu @if($auth) active @endif" >

                    <a href="#auth" data-toggle="collapse" @if($auth) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.lock')
                            <span>Auth & Security</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    @if(cn('can_view_users') || cn('can_crud_users'))
                        <ul class="collapse submenu list-unstyled @if($auth) recent-submenu mini-recent-submenu show @endif" id="auth" data-parent="#accordionExample">
                            <li @if(request()->segment(2)==='users') class="active" @endif>
                                <a href="{{route('users')}}"> Staff</a>
                            </li>

                            <li @if(request()->segment(2)==='students') class="active" @endif>
                                <a href="{{route('students')}}"> Students</a>
                            </li>

                        </ul>
                    @endif

                    @if(cn('can_update_role_permissions') || cn('can_change_roles') || cn('can_change_permissions'))
                        <ul class="collapse submenu list-unstyled @if($auth) recent-submenu mini-recent-submenu show @endif" id="auth" data-parent="#accordionExample">
                            <li @if(request()->segment(2)==='roles-permissions') class="active" @endif>
                                <a href="{{route('roles.permissions')}}"> Roles & Permissions</a>
                            </li>
                        </ul>
                    @endif
                </li>
            @endif

            {{-- CLASS LISTS --}}
            @if(cn('can_request_classlist_master'))
                @php $classlists = (
                                        request()->segment(2)=== 'classlist-master'

                                    );
                @endphp
                <li class="menu @if($classlists) active @endif" >

                    <a href="#classlists" data-toggle="collapse" @if($classlists) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.users')
                            <span>Classlists</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    @can('can_request_classlist_master')
                        <ul class="collapse submenu list-unstyled @if($classlists) recent-submenu mini-recent-submenu show @endif" id="classlists" data-parent="#accordionExample">
                            <li @if(request()->segment(2)==='classlist-master') class="active" @endif>
                                <a href="{{route('classlist.master')}}">Classlist-Master</a>
                            </li>
                        </ul>
                    @endif
                </li>
            @endif

            {{-- FINANCE --}}
            @if(cn('can_view_transactions') || cn('can_dump_transactions') || cn('can_view_deleted_transactions')
                || cn('can_view_cash_receipts') || cn('can_view_payment_arrangment') || cn('can_view_statements'))
                @php $finance = (
                                        request()->segment(2)=== 'transactions' ||
                                        request()->segment(2)=== 'dump-transactions' ||
                                        request()->segment(2)=== 'deleted-transactions' ||
                                        request()->segment(2)=== 'cash-receipts' ||

                                        request()->segment(2)=== 'statements' ||
                                        request()->segment(2)=== 'payment-arrangement'

                                    );
                @endphp
                <li class="menu @if($finance) active @endif" >

                    <a href="#finance" data-toggle="collapse" @if($finance) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.finance')
                            <span>Finance</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    <ul class="collapse submenu list-unstyled @if($finance) recent-submenu mini-recent-submenu show @endif" id="finance" data-parent="#accordionExample">
                        @if(cn('can_view_transactions'))
                            <li @if(request()->segment(2)==='transactions') class="active" @endif>
                                <a href="{{route('transactions')}}"> Transactions</a>
                            </li>
                        @endif

                        @if(cn('can_dump_transactions'))
                            <li @if(request()->segment(2)==='dump-transactions') class="active" @endif>
                                <a href="{{route('dump.transactions')}}"> Dump Transactions</a>
                            </li>
                        @endif

                        @if(cn('can_view_deleted_transactions'))
                            <li @if(request()->segment(2)==='deleted-transactions') class="active" @endif>
                                <a href="{{route('deleted.transactions')}}"> Deleted Transactions</a>
                            </li>
                        @endif

                        @if(cn('can_view_cash_receipts'))
                            <li @if(request()->segment(2)==='cash-receipts') class="active" @endif>
                                <a href="{{route('cash.receipts')}}"> Cash Receipts</a>
                            </li>
                        @endif

                        @if(cn('can_view_payment_arrangment'))
                            <li @if(request()->segment(2)==='payment-arrangement') class="active" @endif>
                                <a href="{{route('payment.arrangement')}}">Payment Arrangments</a>
                            </li>
                        @endif

                        @if(cn('can_view_statements'))
                            <li @if(request()->segment(2)==='statements') class="active" @endif>
                                <a href="{{route('statements')}}"> Statements</a>
                            </li>
                        @endif

                        @if(cn('can_view_transactions'))
                            <li @if(request()->segment(2)==='outstanding-debtors') class="active" @endif>
                                <a href="{{route('outstanding.debtors')}}"> Outstanding Debtors</a>
                            </li>
                        @endif

                    </ul>

                </li>
            @endif

            {{-- MY ADMINISTRATION --}}
            @if(cn('can_request_quote') && !auth()->user()->hasRole('student'))
                @php $myadmin = (request()->segment(2)==='mydefault-modules'); @endphp
                <li class="menu @if($myadmin) active @endif" >

                    <a href="#myadmin" data-toggle="collapse" @if($myadmin) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.airplay')
                            <span>My Admin</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    <ul class="collapse submenu list-unstyled @if($myadmin) recent-submenu mini-recent-submenu show @endif" id="myadmin" data-parent="#accordionExample">
                        <li @if(request()->segment(2)==='mydefault-modules') class="active" @endif>
                            <a href="{{route('mydefault-modules')}}"> MyDefault Module</a>
                        </li>
                    </ul>
                </li>
            @endif

            {{-- MASTER DATA --}}
            @if(cn('can_view_masterdata') || cn('can_crud_masterdata'))
                @php $master_data = (
                                        request()->segment(2)=== 'categories' ||
                                        request()->segment(2)=== 'modules' ||
                                        request()->segment(2)=== 'service-types' ||
                                        request()->segment(2)=== 'pricing'

                                    );
                @endphp
                <li class="menu @if($master_data) active @endif" >

                    <a href="#masterdata" data-toggle="collapse" @if($master_data) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.table')
                            <span>Master Data</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    <ul class="collapse submenu list-unstyled @if($master_data) recent-submenu mini-recent-submenu show @endif" id="masterdata" data-parent="#accordionExample">
                        <li @if(request()->segment(2)==='categories') class="active" @endif>
                            <a href="{{route('categories')}}"> Categories</a>
                        </li>
                        <li @if(request()->segment(2)==='modules') class="active" @endif>
                            <a href="{{route('modules')}}"> Modules</a>
                        </li>
                        <li @if(request()->segment(2)==='service-types') class="active" @endif>
                            <a href="{{route('servicetypes')}}"> Service Types</a>
                        </li>
                        <li @if(request()->segment(2)==='pricing') class="active" @endif>
                            <a href="{{route('pricing')}}"> Pricing</a>
                        </li>
                    </ul>
                </li>
            @endif

            {{-- ALLOCATIONS --}}
            @if(cn('can_do_allocations'))
                @php
                    $allocations = (
                                request()->segment(2)==='lecturer-allocations' || request()->segment(2)==='bulk-allocations'
                              );
                @endphp
                <li class="menu @if($allocations) active @endif" >

                    <a href="#allocations" data-toggle="collapse" @if($allocations) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.lock')
                            <span>Allocations</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    <ul class="collapse submenu list-unstyled @if($allocations) recent-submenu mini-recent-submenu show @endif" id="allocations" data-parent="#accordionExample">
                        <li @if(request()->segment(2)==='lecturer-allocations') class="active" @endif>
                            <a href="{{route('lecturer.allocations')}}"> Lect Allocations</a>
                        </li>
                        <li @if(request()->segment(2)==='bulk-allocations') class="active" @endif>
                            <a href="{{route('bulk.allocations')}}"> Bulk Allocations</a>
                        </li>
                    </ul>


                </li>
            @endif


            @if(cn('can_view_reports'))
                @php $reports = (
                                request()->segment(2)==='reports' ||
                                request()->segment(2)==='tax-reports'
                            );
                @endphp
                <li class="menu @if($reports) active @endif" >

                    <a href="#reports" data-toggle="collapse" @if($reports) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.piechart')
                            <span>Reports</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    <ul class="collapse submenu list-unstyled @if($reports) recent-submenu mini-recent-submenu show @endif" id="reports" data-parent="#accordionExample">
                        <li @if(request()->segment(2)==='reports') class="active" @endif>
                            <a href="{{route('reports')}}"> Admin Reports</a>
                        </li>

                        <li @if(request()->segment(2)==='tax-reports') class="active" @endif>
                            <a href="{{route('taxreports')}}"> Tax Reports</a>
                        </li>

                    </ul>
                </li>
            @endif

            @if(cn('can_do_allocations'))
                @php $timetable = (
                                        request()->segment(2)=== 'timetable-weekend-sat' ||
                                        request()->segment(2)=== 'timetable-weekend-sun' ||
                                        request()->segment(2)=== 'timetable-weekday-sat' ||
                                        request()->segment(2)=== 'timetable-weekday-sun'

                                    );
                @endphp

                <li class="menu @if($timetable) active @endif" >

                    <a href="#timetable" data-toggle="collapse" @if($timetable) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.finance')
                            <span>TimeTable</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    <ul class="collapse submenu list-unstyled @if($timetable) recent-submenu mini-recent-submenu show @endif" id="timetable" data-parent="#accordionExample">
                        <li @if(request()->segment(2)==='timetable-weekend-sat') class="active" @endif>
                            <a href="{{route('timetable.weekend.sat')}}">Weekend Sat</a>
                        </li>
                        <li @if(request()->segment(2)==='timetable-weekend-sun') class="active" @endif>
                            <a href="{{route('timetable.weekend.sun')}}">Weekend Sun</a>
                        </li>
                        <li @if(request()->segment(2)==='timetable-weekday-sat') class="active" @endif>
                            <a href="{{route('timetable.weekday.sat')}}">Weekday Sat</a>
                        </li>
                        <li @if(request()->segment(2)==='timetable-weekday-sun') class="active" @endif>
                            <a href="{{route('timetable.weekday.sun')}}">Weekday Sun</a>
                        </li>

                    </ul>
                </li>
            @endif

            @php $online = (
                                request()->segment(2)==='assignments' ||
                                request()->segment(2)==='assignment' ||
                                request()->segment(2)==='assignment-allocations'
                            );
            @endphp
            <li class="menu @if($online) active @endif" >

                <a href="#online" data-toggle="collapse" @if($online) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                    <div class="">
                        @include('components.svg.piechart')
                        <span>Online</span>
                    </div>
                    <div> @include('components.svg.forward-arrow') </div>
                </a>
                <ul class="collapse submenu list-unstyled @if($online) recent-submenu mini-recent-submenu show @endif" id="online" data-parent="#accordionExample">
                    <li @if(request()->segment(2)==='assignments' || request()->segment(2)==='assignment') class="active" @endif>
                        <a href="{{route('admin.assignments')}}"> Assignments</a>
                    </li>
                    @if(cn('can_allocate_assignment'))
                        <li @if(request()->segment(2)==='assignment-allocations') class="active" @endif>
                            <a href="{{route('assignment.allocations')}}"> Assignments Allocations</a>
                        </li>
                    @endif
                </ul>
            </li>

            {{--LOGS--}}
            @if(cn('can_view_smsemail_logs') || cn('can_view_system_logs') || cn('can_view_classlists_logs'))
                @php $logs = (
                                    request()->segment(1)==='logs' ||
                                    request()->segment(2)==='sms-emails-logs' ||
                                    request()->segment(2) === 'generated-classlist'
                                );
                @endphp
                <li class="menu @if($logs) active @endif" >

                    <a href="#logs" data-toggle="collapse" @if(true) aria-expanded="true" @else aria-expanded="false" @endif class="dropdown-toggle">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity"><polyline points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline></svg>
                            <span>Logs</span>
                        </div>
                        <div> @include('components.svg.forward-arrow') </div>
                    </a>
                    <ul class="collapse submenu list-unstyled @if($logs) recent-submenu mini-recent-submenu show @endif" id="logs" data-parent="#accordionExample">
                        @if(cn('can_view_system_logs'))
                            <li @if(request()->segment(1)==='logs') class="active" @endif>
                                <a href="{{route('logs')}}">System Error Logs</a>
                            </li>
                        @endif

                        @if(cn('can_view_smsemail_logs'))
                            <li @if(request()->segment(2)==='sms-emails-logs') class="active" @endif>
                                <a href="{{route('sms.emails.logs')}}"> SMS & Email Logs</a>
                            </li>
                        @endif

                        @if(cn('can_view_classlists_logs'))
                            <li @if(request()->segment(2)==='generated-classlist') class="active" @endif>
                                <a href="{{route('generated.classlist')}}"> Generated Classlist Logs</a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            {{--SETTINGS--}}
            @can('can_view_settings')
                <li class="menu @if(request()->segment(2)==='settings') active @endif">
                    <a href="{{route('admin.settings')}}" @if(request()->segment(2)==='settings') aria-expanded="true" @endif class="dropdown-toggle">
                        <div class="">
                            @include('components.svg.setting')
                            <span>Settings</span>
                        </div>
                    </a>
                </li>
            @endcan


            <li class="menu menu-heading">
                <div class="heading"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-circle"><circle cx="12" cy="12" r="10"></circle></svg><span>APPS</span></div>
            </li>

            <li class="menu @if(request()->segment(2)==='notes') active @endif">
                <a href="{{route('notes')}}" @if(request()->segment(2)==='notes') aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-square"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>
                        <span>Notes</span>
                    </div>
                </a>
            </li>

            <li class="menu @if(request()->segment(2)==='todos') active @endif">
                <a href="{{route('todos')}}" @if(request()->segment(2)==='todos') aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                        <span>Todos</span>
                    </div>
                </a>
            </li>

            <li class="menu @if(request()->segment(2)==='chat') active @endif">
                <a href="{{route('chat')}}" @if(request()->segment(2)==='chat') aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span>Chat</span>
                    </div>
                </a>
            </li>


        </ul>

    </nav>

</div>
<!--  END SIDEBAR  -->