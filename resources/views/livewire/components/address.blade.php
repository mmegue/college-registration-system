<h1 class="name"> {{(new Helper)->contact()->company_name}}</h1>
<address style="font-size:x-small">
    {{(new Helper)->address()->address1}} <br>
    {{(new Helper)->address()->address2}} <br>
    {{(new Helper)->address()->city}} <br>
    <a href="">{{(new Helper)->contact()->email}}</a><br>
    <a href="">{{(new Helper)->contact()->cell_1}} | {{(new Helper)->contact()->cell_3}}</a>
</address>