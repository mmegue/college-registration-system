<div class="row layout-top-spacing mt-2">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div style="padding: 20px;" class="statbox widget box box-shadow">
            <div class="custom-file-container" data-upload-id="fileUpload">
                <label>Upload (Single File) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                <input type="file" wire:model="file" class=""
                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                <button wire:click="upload_file()" wire:loading.remove class="btn btn-dark"  type="button">
                    <i class="fa fa-cloud-upload-alt my-floats"></i>&nbsp;Start Import Data</button>

                @include('components.error',['name'=>'file'])
            </div>
        </div>
    </div>
</div>