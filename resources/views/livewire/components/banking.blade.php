<table id="table"  style="color:white;background-color: #00c0c3de;border-radius: 5px;padding:2px">
    <tr>
        {{--<td><img src="https://www.fnb.co.za/00Assets/v2.2/images/global/logo_full.png" alt=""></td>--}}
        <td><img src="{{public_path().'/images/fnb-logo.png'}}" alt=""></td>
        <td style="padding:5px;text-align: right;">
            Bank: <span style="color:#000000">{{(new Helper)->banking()->bank_name}}</span> <br>
            Account Name: <span style="color:#000000">{{(new Helper)->banking()->account_name}}</span> <br>
            Account Number: <span style="color:#000000">{{(new Helper)->banking()->account_number}}</span> <br>
            Account Type: <span style="color:#000000">{{(new Helper)->banking()->account_type}}</span><br>
            Branch Code: <span style="color:#000000">{{(new Helper)->banking()->branch_code}}</span> <br>
            <span style="background-color: yellow;color:red; padding:5px;border-radius: 5px">Reference: [{{$quotation_details->cell." ".$quotation_details->surname}}]</span> <br>
        </td>

    </tr>

</table>