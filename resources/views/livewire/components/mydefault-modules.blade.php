<div id="toggleAccordion">
    @foreach($this->categorygroups as $group)
        <div class="card">
            <div class="card-header" id="{{$group->id}}">
                <section class="mb-0 mt-0">
                    <div role="menu" class="collapsed text-primary" data-toggle="collapsed" data-target="#defaultAccordionOne" aria-expanded="true" aria-controls="defaultAccordionOne">
                        {{$group->category_name}}  <div class="icons"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></div>
                    </div>
                </section>
            </div>

            <div id="defaultAccordionOne" class="collapsed" aria-labelledby="{{$group->id}}" data-parent="#toggleAccordion">
                <div class="card-body">
                    <div class="row">
                        @foreach($group->modules as $module)
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                <div class="n-chk">
                                    <label class="new-control new-checkbox checkbox-outline-success" wire:ignore>
                                        <input type="checkbox"  @if(in_array(Auth::id().'-'.$module->id.'-'.$group->id,$modules)) checked @endif
                                        wire:click="add_my_default_module({{$module->id}},{{$group->id}})" class="new-control-input">
                                        <span class="new-control-indicator"></span>{{$module->module_code}}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>