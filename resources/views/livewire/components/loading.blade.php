<div wire:loading.delay>
    <div class="d-flex justify-content-between mx-5 mt-3 mb-5">
        <div class="spinner-grow text-danger align-self-center ml-5"></div> <span class="mt-1">Loading Please Wait....</span>
    </div>
</div>