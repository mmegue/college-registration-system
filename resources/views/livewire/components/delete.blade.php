<div x-data="{ open: false }">
    <div class="btn-group" role="group" >
        <div><button type="button" class="btn btn-sm btn-success px-1 py-0"><i class="fa fa-eye"></i></button></div>
        <div x-show="!open" ><button @click="open=true" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-trash"></i></button></div>
        <div x-show="open" ><button  type="button" class="btn btn-sm btn-success px-1 py-0"><i class="fa fa-check"></i></button></div>
        <div x-show="open" ><button @click="open=false" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-times"></i></button></div>
    </div>
</div>