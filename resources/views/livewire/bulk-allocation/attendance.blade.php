<select wire:model="attendance" class="module w-full">
    <option value="">
        {{$attendance}}
    </option>

    @foreach(\App\Http\Traits\Status::attendance() as $s)
        <option value="{{$s."*".$quotation_modules_id}}">{{$s}}</option>
    @endforeach
</select>
