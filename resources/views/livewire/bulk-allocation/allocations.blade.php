<select wire:model="lecturer" class="module w-full">
    <option value="">
        {{$username}}
    </option>
    @foreach(cached_lecturers() as $value)
        <option value="{{$value->id."*".$quotation_modules_id}}">{{$value->username}}</option>
    @endforeach
</select>
