<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}} | Weekend Sunday Timetable</title>
    <style>
        .styled-table {
            border-collapse: collapse;
            margin: 25px 0;
            font-size: 0.9em;
            font-family: sans-serif;
            min-width: 100%;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
        }
        .styled-table thead tr {
            background-color: #040e7d;
            color: #ffffff;
            text-align: left;
        }
        .styled-table th,
        .styled-table td {
            padding: 5px 10px;
            text-align: left;
        }
        .styled-table tbody tr {
            border-bottom: 1px solid #dddddd;
            text-align: left;
        }

        .styled-table tbody tr:nth-of-type(even) {
            background-color: #f3f3f3;
            text-align: left;
        }

        .styled-table tbody tr:last-of-type {
            border-bottom: 2px solid #040e7d;
        }
        .styled-table tbody tr.active-row {
            font-weight: bold;
            color: #040e7d;
            text-align: left;
        }

        .watermark{
            display: block;
            text-align:center;
            margin-left: auto;
            margin-right: auto;
            margin-top: 10%;
            width: 100%;
            /* height: 100%; */
            opacity: 0.1;
            position: absolute;
        }

        #footer {
            position: fixed;
            bottom: 0px;
            width: 100%;
        }

    </style>
</head>
<body style="min-width: 100%;">
<div class="watermark"><img class="watermarks" src="{{ asset('images/logo.png') }}"></div>
<h1> Weekend Sunday Timetable </h1>
<a href="{{route('timetable-weekday')}}">Weekday Timetable</a> &nbsp;|&nbsp;  <a href="{{route('timetable-weekend-sat')}}">Weekend Saturday Timetable</a>
<table class="styled-table">
    <thead>
    <tr>
        <th>Date</th>
        <th>Time</th>
        <th>SLOT0</th>
        <th>SLOT1</th>
        <th>SLOT2</th>
        <th>SLOT3</th>
        <th>SLOT4</th>
        <th>SLOT5</th>
        <th>SLOT6</th>
        <th>SLOT7</th>
        <th>SLOT8</th>
        <th>SLOT9</th>
        <th>SLOT10</th>
        <th>SLOT11</th>
        <th>SLOT12</th>
    </tr>
    </thead>
    <tbody>
    @foreach(\App\Models\TimetableWeekendSun::all() as $timetable)
        <tr>
            <td>{!! $timetable->day !!}</td>
            <td>{!! $timetable->time !!}</td>
            <td>{!! $timetable->slot0 !!}</td>
            <td>{!! $timetable->slot1 !!}</td>
            <td>{!! $timetable->slot2 !!}</td>
            <td>{!! $timetable->slot3 !!}</td>
            <td>{!! $timetable->slot4 !!}</td>
            <td>{!! $timetable->slot5 !!}</td>
            <td>{!! $timetable->slot6 !!}</td>
            <td>{!! $timetable->slot7 !!}</td>
            <td>{!! $timetable->slot8 !!}</td>
            <td>{!! $timetable->slot9 !!}</td>
            <td>{!! $timetable->slot10 !!}</td>
            <td>{!! $timetable->slot11 !!}</td>
            <td>{!! $timetable->slot12 !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<blockquote> Date generated:{{date('Y-m-d H:i:s')}} </blockquote>
<footer id="footer" >
    <table style="font-size: small; width: 100%; vertical-align: text-top;background-color: #cccccc;padding: 10px;">
        <tr style="vertical-align: text-top">
            <td style="vertical-align: text-top">
                <div id="invoice-left">
                    <strong>Banking Details</strong>
                    <div class="date">Bank {{config('app.bankname')}}</div>
                    <div class="date">Account Name {{config('app.accname')}}</div>
                    <div class="date">Account Number {{config('app.accnumber')}}</div>
                    <div class="date">Account Type {{config('app.acctype')}}</div>
                    <div class="date">Branch Code {{config('app.branchcode')}}</div>
                    <div>Reference: <span style="background-color: yellow; border-radius: 2px">Cell# & Surname</span></div>
                </div>
            </td>
            <td style="vertical-align: text-top">
                <strong>Physical Tutorials Campus</strong> <br>
                266 Johannes Ramokhoase Str, <br>
                PTA CBD, Christian Progressive College <br>
            </td>
            <td style="vertical-align: text-top">
                <strong>Office Address</strong> <br>
                373 Johannes Ramokhoase Str, <br>
                Downies Building, PTA CBD, BOISA <br>
            </td>
        </tr>
    </table>
</footer>
</body>
</html>
