
    @section('page_title',$page_title)

    @section('links')
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
        <link href="{{asset('assets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/widgets/modules-widgets.css')}}">
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    @endsection

    @section('breadcrumbs')
        <ol class="breadcrumb">
            <!--
            <li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Components</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="javascript:void(0);">UI Kit</a></li>
             -->
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item active"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        </ol>
    @endsection

    <div>
        <div class="row layout-top-spacing mt-4">
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-activity-five">

                    <div class="widget-heading">
                        <h5 class="">Notifications</h5>
                        {{--
                        <button wire:click="exc">run</button>
                        <div wire:loading>Loading Wait.............</div>
                        --}}
                    </div>

                    <div class="widget-content">

                        <div class="w-shadow-top"></div>

                        <div class="mt-container mx-auto">
                            <div class="timeline-line">
                                @foreach($this->my_notifications() as $notification)
                                    <div class="item-timeline timeline-new">
                                        <div class="t-dot">
                                            <div class="t-secondary"><i class="@if($notification->type === 'App\Notifications\NewUserRegistered')fa fa-user @endif"></i></div>
                                        </div>
                                        <div class="t-content">
                                            <div class="t-uppercontent">
                                                <h5>{{$notification['data']['heading']}}</h5>
                                                <small>{{$notification['created_at']->diffForHumans()}}</small>
                                            </div>
                                            <p>{{$notification['data']['message']}}</p>
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>
                        </div>

                        <div class="w-shadow-bottom"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                <div class="widget widget-four">
                    <div class="widget-heading">
                        <h5 class="">Performance</h5>
                    </div>
                    <div class="widget-content">
                        <div class="order-summary">
                            <div class="summary-list">
                                <div class="summery-info">
                                    <div class="w-summary-details">
                                        <div class="w-summary-info">
                                            <h6> Amount Invoice <span class="summary-count summary-average">Total Amount Invoiced</span></h6>
                                            <p class="summary-average font-weight-bold text-info">R{{$this->total_invoices()}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="summary-list" style="background-color: rgba(124,190,225,0.34);">
                                <div class="summery-info">
                                    <div class="w-summary-details">
                                        <div class="w-summary-info">
                                            <h6> Total Invoice <span class="summary-count summary-average">Total Number Of Invoices</span></h6>
                                            <p class="summary-average font-weight-bold text-info">{{$this->count_invoices()}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="summary-list" style="background-color: rgba(124,225,137,0.34);">
                                <div class="summery-info">
                                    <div class="w-summary-details">
                                        <div class="w-summary-info">
                                            <h6> Total Reg Fees <span class="summary-count summary-average">Total Number Of Reg Fees</span></h6>
                                            <p class="summary-average font-weight-bold text-info">{{$this->count_invoices_with_reg_fee()}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="summary-list" style="background-color: rgba(225,139,124,0.34);">
                                <div class="summery-info">
                                    <div class="w-summary-details">
                                        <div class="w-summary-info">
                                            <h6> Amount Reg Fees <span class="summary-count summary-average">Total Amount Of Reg Fees</span></h6>
                                            <p class="summary-average font-weight-bold text-info">R{{$this->total_reg_fees()}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-activity-five">

                    <div class="widget-heading">
                        <h5 class="">Todos</h5>
                        
                    </div>

                    <div class="widget-content">

                        <div class="w-shadow-top"></div>

                        <div class="mt-container mx-auto">
                            <div class="timeline-line">

                               @foreach($this->my_todos() as $todo)
                                <div class="item-timeline timeline-new">
                                    <div class="t-dot">
                                        <div class="t-{{$this->class($todo->priority)}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg></div>
                                    </div>
                                    <div class="t-content">
                                        <div class="t-uppercontent">
                                            <h5>{{$todo->title}}</h5>
                                        </div>
                                        <p>{{$todo->priority}}</p>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>

                        <div class="w-shadow-bottom"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @section('scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
        <script src="{{asset('assets/js/dashboard/dash_1.js')}}"></script>
        <script src="{{asset('assets/js/widgets/modules-widgets.js')}}"></script>

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    @endsection
