
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('classlist.master')}}">Master Class List</a></li>
    </ol>
@endsection

<div>

    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                @if(cn('can_request_classlist_master'))
                    <div class="widget-content widget-content-area">
                        <div class="seperator-header layout-top-spacing d-inline-flex">
                            <select wire:model="module_code" wire:change="data" class="form-controls form-control-sm">
                                <option value="">Module</option>
                                @include('livewire.components.modules-select')
                            </select>

                            <select wire:model="service_type" wire:change="data" class="form-controls form-control-sm">
                                <option value="">Service Type</option>
                                @include('livewire.components.servicetypes-select')
                            </select>

                            <select wire:model="lecturer" wire:change="data" class="form-controls form-control-sm">
                                <option value="">Lecturer</option>
                                @include('livewire.components.lecturer-select')
                            </select>

                            <select wire:model="attendance" wire:change="data" class="form-controls form-control-sm">
                                <option value="">Attendance</option>
                                @include('livewire.components.attendances')
                            </select> <hr>

                            @if(!empty($lists))
                                <button wire:click="export" class="btn btn-success btn-sm ml-3"  type="button"><i class="fa fa-file-excel my-floats"></i>&nbsp;Export Excel</button>
                                <button wire:click="export_pdf" class="btn btn-danger btn-sm ml-3"  type="button"><i class="fa fa-file-pdf my-floats"></i>&nbsp;Export PDF</button>
                            @endif
                        </div>
                        @if(!is_null($module_code))
                            <span class="badge badge-success">{{$module_code}}
                            <a wire:click="reset_('module_code')">&nbsp;<i class="fa fa-trash text-danger"></i></a>
                        </span>
                        @endif
                        @if(!is_null($service_type))
                            <span class="badge badge-success">{{$service_type}}
                            <a wire:click="reset_('service_type')">&nbsp;<i class="fa fa-trash text-danger"></i></a>
                        </span>
                        @endif
                        @if(!is_null($lecturer))
                            <span class="badge badge-success">{{$lecturer}}
                            <a wire:click="reset_('lecturer')">&nbsp;<i class="fa fa-trash text-danger"></i></a>
                        </span>
                        @endif
                        @if(!is_null($attendance))
                            <span class="badge badge-success">{{$attendance}}
                            <a wire:click="reset_('attendance')">&nbsp;<i class="fa fa-trash text-danger"></i></a>
                        </span>
                        @endif
                        <hr>
                        @if(!empty($lists))
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped table-highlight-head mb-4 nowrap" >
                                    <thead>
                                    <tr>
                                        <th>Module</th>
                                        <th>Semester</th>
                                        <th>ServiceType</th>
                                        <th>Attendance</th>

                                        <th>Title</th>
                                        <th>Name</th>
                                        <th>Surname</th>
                                        <th>Email</th>
                                        <th>Stud#</th>
                                        <th>MyUNISAPwd#</th>
                                        <th>Cell#</th>
                                        <th>Quote#</th>
                                        <th>Inv#</th>
                                        <th>Price</th>
                                        <th>Modules</th>
                                        <th>Invoiced</th>
                                        <th>Paid</th>
                                        <th>Balance</th>
                                        <th>PMT%</th>
                                        <th>AddedBy</th>
                                        <th>CreatedBy</th>
                                        <th>Lecturer</th>
                                    </tr>
                                    </thead>
                                    @foreach($lists as $list)
                                        <tbody>
                                        <tr>
                                            <td>{{$list['module_code']}}</td>
                                            <td>{{$list['semester']}}</td>
                                            <td>{{$list['service_type']}}</td>
                                            <td>{{$list['attendance']}}</td>

                                            <td>{{$list['title']}}</td>
                                            <td>{{$list['name']}}</td>
                                            <td>{{$list['surname']}}</td>
                                            <td>{{$list['email']}}</td>
                                            <td>{{$list['username']}}</td>
                                            <td>{{$list['myunisa_pwd']}}</td>
                                            <td>{{$list['cell']}}</td>
                                            <td>{{$list['quotation_number']}}</td>
                                            <td>{{$list['invoice_number']}}</td>
                                            <td>{{"R".$list['price']}}</td>
                                            <td>{{$list['modules']}}</td>
                                            <td>{{"R".$list['invoiced']}}</td>
                                            <td>{{"R".$list['paid']}}</td>
                                            <td>{{"R".$list['balance']}}</td>
                                            <td>{{$list['pmt']."%"}}</td>
                                            <td>{{$list['quote_module_added_by']}}</td>
                                            <td>{{$list['quote_created_by']}}</td>
                                            <td>{{$list['lecturer_default']}}</td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('scripts')

@endpush
