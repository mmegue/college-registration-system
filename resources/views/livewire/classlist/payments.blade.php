<span class="text-secondary d-inline-flex">Invoiced(R): <strong>{{$invoiced}}</strong></span><br>
<span class="text-success d-inline-flex">Paid(R): &nbsp; <strong>{{$paid}}</strong></span> <br>
<span class="text-danger d-inline-flex">Balance(R): <strong>{{$balance}}</strong></span> <br>
<span class="text-lg-center d-inline-flex">PMT: {{round(($paid/$invoiced)*100,0)}}%</span>