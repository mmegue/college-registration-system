
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('modules')}}">Pricing</a></li>
    </ol>
@endsection

<div>
    @if(cn('can_crud_masterdata'))
        <div class="seperator-header layout-top-spacing">
            <button wire:click="show_create" class="btn btn-dark ml-3"  type="button"><i class="fa fa-plus my-floats"></i>&nbsp;Add New Pricing</button>
            @include('livewire.components.import_and_export_buttons')
            @include('livewire.components.loading')
            @if($upload) @include('livewire.components.import_and_export_form')@endif
        </div>
    @endif
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                @if(($create || $edit)&& cn('can_crud_masterdata'))
                    <div class="widget-content widget-content-area">
                        @if($create)<h4>Create Pricing</h4>@endif
                        @if($edit)<h4>Edit Pricing</h4>@endif
                        <small class="mb-2">Fields with * are required.</small>

                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <select class="form-control form-control-sm" wire:model.defer="module_code">
                                        <option value="">Choose Module Code</option>
                                        @include('livewire.components.modules-select')
                                    </select>
                                </div>

                                <label for="module_code" class="col-sm-6 col-form-label col-form-label-md">Module Code &nbsp;<br>
                                    @include('components.error',['name'=>'module_code'])</label>
                            </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <select class="form-control form-control-sm" wire:model.defer="service_type">
                                    <option value="">Choose Service Type</option>
                                    @foreach($this->servicetypes as $servicetype)
                                        <option value="{{$servicetype->slug}}">{{$servicetype->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label for="service_type" class="col-sm-6 col-form-label col-form-label-md">Service Type &nbsp;<br>
                                @include('components.error',['name'=>'service_type'])</label>
                        </div>
                        <x-input wire:model.defer="price" placeholder="Price">Price</x-input>

                        @if($create) <x-button wire:click="create_pricing" class="btn-dark">Create Service type</x-button>@endif
                        @if($edit) <x-button wire:click="update_pricing" class="btn-secondary">Update Service type</x-button>@endif
                        <x-button wire:click="reset_booleans" class="ml-2 btn-danger">Cancel</x-button>
                    </div>
                @endif
                @if(cn('can_crud_masterdata') || cn('can_view_masterdata'))
                    <div class="widget-content widget-content-area">
                        <livewire:pricing.pricing-table/>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
@endpush
