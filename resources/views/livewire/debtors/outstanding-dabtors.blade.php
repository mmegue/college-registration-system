
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('outstanding.debtors')}}">Debtors</a></li>
    </ol>
@endsection

<div>
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                    <table class="table table-bordered table-hover table-striped table-highlight-head mb-4 nowrap">
                        <thead>
                            <tr>
                                <th>Module</th>
                                <th>Semester</th>
                                <th>ServiceType</th>
                                <th>Attendance</th>
                                <th>Title</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Stud#</th>
                                <th>MyUNISAPwd#</th>
                                <th>Cell#</th>
                                <th>Quote#</th>
                                <th>Inv#</th>
                                <th>Price</th>
                                <th>Modules</th>
                                <th>Invoiced</th>
                                <th>Paid</th>
                                <th>Balance</th>
                                <th>PMT%</th>
                                <th>AddedBy</th>
                                <th>CreatedBy</th>
                                <th>Lecturer</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>AIN1501</td>
                            <td>2022S2</td>
                            <td>assignment-01</td>
                            <td></td>

                            <td>Mr</td>
                            <td>Student</td>
                            <td>System</td>
                            <td>student@nobletutors.co.za</td>
                            <td>Student</td>
                            <td></td>
                            <td>27123456785</td>
                            <td>202303080003</td>
                            <td></td>
                            <td>R100</td>
                            <td>AIN1501</td>
                            <td>R0</td>
                            <td>R0</td>
                            <td>R0</td>
                            <td>0%</td>
                            <td>Student</td>
                            <td>Student</td>
                            <td></td>
                        </tr>
                        </tbody>
                        <tbody>
                        <tr>
                            <td>AIN1501</td>
                            <td>2022S2</td>
                            <td>assignment-02</td>
                            <td></td>

                            <td>Mr</td>
                            <td>Student</td>
                            <td>System</td>
                            <td>student@nobletutors.co.za</td>
                            <td>Student</td>
                            <td></td>
                            <td>27123456785</td>
                            <td>202303080003</td>
                            <td></td>
                            <td>R300</td>
                            <td>AIN1501</td>
                            <td>R0</td>
                            <td>R0</td>
                            <td>R0</td>
                            <td>0%</td>
                            <td>Student</td>
                            <td>Student</td>
                            <td></td>
                        </tr>
                        </tbody>
                        <tbody>
                        <tr>
                            <td>AIN1501</td>
                            <td>2022S2</td>
                            <td>online-zoom-class</td>
                            <td></td>

                            <td>Mr</td>
                            <td>Student</td>
                            <td>System</td>
                            <td>student@nobletutors.co.za</td>
                            <td>Student</td>
                            <td></td>
                            <td>27123456785</td>
                            <td>202303080003</td>
                            <td></td>
                            <td>R1300</td>
                            <td>AIN1501</td>
                            <td>R0</td>
                            <td>R0</td>
                            <td>R0</td>
                            <td>0%</td>
                            <td>Student</td>
                            <td>Student</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')

@endpush
