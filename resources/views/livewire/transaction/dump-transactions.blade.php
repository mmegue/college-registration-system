
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('dump.transactions')}}">Dump Transaction</a></li>
    </ol>
@endsection

<div>

    <div class="row layout-top-spacing text-white bg-dark">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow p-4 mt-4">
                <div class="container">
                    <div class="mt-4">
                        <div class="form-group pl-6">
                            <h4 class="text-white">Dump Transactions</h4>
                        </div>
                        <hr>

                        <div class="form-group row mb-1">

                            <div class="col-xl-12 col-lg-12 col-sm-12" >
                                 <textarea rows="10"
                                         wire:model.lazy="transactions"
                                         class="formatted-content h-4 form-control"
                                         placeholder="Paste all transactions here...."
                                 ></textarea>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group row mb-1">
                            <button type="button" wire:click="dump_transactions" class="btn btn-success">Dump Transactions</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')

@endpush
