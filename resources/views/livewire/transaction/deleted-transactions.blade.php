
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('deleted.transactions')}}">Deleted Transactions</a></li>
    </ol>
@endsection

<div>

    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            @if(cn('can_crud_transactions'))
                <button wire:click="truncate_transactions" class="btn btn-danger ml-3 mb-3"  type="button"><i class="fa fa-plus my-floats"></i>&nbsp;Truncate All transactions</button>
            @endif
            <div class="statbox widget box box-shadow mt-3">
                <livewire:transaction.deleted-transactions-table />
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
@endpush
