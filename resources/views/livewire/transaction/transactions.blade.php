
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('transactions')}}">Transactions</a></li>
    </ol>
@endsection

<div>

        <div class="seperator-header layout-top-spacing">
            @if(cn('can_crud_transactions'))
                <button wire:click="truncate_transactions" class="btn btn-danger ml-3"  type="button"><i class="fa fa-plus my-floats"></i>&nbsp;Delete All transactions</button>
                <button wire:click="restore_all_truncate_transactions" class="btn btn-success ml-3"  type="button"><i class="fa fa-plus my-floats"></i>&nbsp;Restore Truncate All transactions</button>
            @endif
            @if(cn('can_export_transactions'))
                @include('livewire.components.import_and_export_buttons')

                @if($upload) @include('livewire.components.import_and_export_form')@endif
            @endif
            @include('livewire.components.loading')
        </div>

    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <livewire:transaction.transactions-table />
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
@endpush
