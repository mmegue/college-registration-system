
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('transactions')}}">Transactions</a></li>
        <li class="breadcrumb-item active"><a href="#">View Transactions</a></li>
    </ol>
@endsection

<div>
   
    <div class="row layout-top-spacing text-white bg-dark">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow p-4 mt-4">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td><strong>Name:</strong></td>
                                    <td class="text-blue-400">{{optional($quotation)->name}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Surname:</strong></td>
                                    <td class="text-blue-400" >{{optional($quotation)->surname}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Email:</strong></td>
                                    <td class="text-blue-400" >{{optional($quotation)->email}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Cell:</strong></td>
                                    <td class="text-blue-400" >{{optional($quotation)->cell}}</td>
                                </tr>
                                <tr>
                                    <td><hr></td>
                                    <td><hr></td>
                                </tr>
                                <tr>
                                    <td><strong>Invoice #:</strong></td>
                                    <td class="text-blue-400" ></td>
                                </tr>
                                <tr>
                                    <td><strong>Price:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->amount}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Item:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->item}}</td>
                                </tr>
                                <tr>
                                    <td><strong>GL Account:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->gl_account}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Campus:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->campus}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Transaction Date:</strong></td>
                                    <td class="text-blue-400" > {{$transaction->transaction_date}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Transaction Type:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->transaction_type}}</td>
                                </tr>
                                <tr>
                                    <td><strong>FS Accounts:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->fs_accounts}}</td>
                                </tr>
                                <tr>
                                    <td><strong>TAX Period:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->tax_period}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Allocation Period:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->allocation_period}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Short Description:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->short_description}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Receipt Number:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->receipt_number}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Semester Period:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->semester_period}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Internal Transaction #:</strong></td>
                                    <td class="text-blue-400" >{{$transaction->internal_transaction_number}}</td>
                                </tr>

                            </table>
                        </td>
                        <td><span class="px-10"></span></td>
                        <td class="mt-1">
                            <table class="table bg-dark table-striped ml-4">
                                <thead class="bg-blue-500 py-0 rounded">
                                <tr>
                                    <th>Amount</th>
                                    <th>Created By</th>
                                    <th>Item</th>
                                    <th>Semester</th>
                                    <th>Cell</th>
                                    <th>Invoice #</th>
                                </tr>
                                </thead>
                                <thead>
                                @foreach($transactions as $tran)
                                    <tr>
                                        <td>{{$tran->amount}}</td>
                                        <td>{{optional(\App\Models\User::find($tran->user_id))->username}}</td>
                                        <td>{{$tran->item}}</td>
                                        <td>{{$tran->semester_period}}</td>
                                        <td>{{$tran->cell_number}}</td>
                                        <td>{{$tran->invoice_number}}</td>
                                    </tr>
                                @endforeach

                                </thead>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
@endpush
