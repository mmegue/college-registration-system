
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('cash.receipts')}}">Cash Receipts</a></li>
    </ol>
@endsection

<div>

    @if(cn('can_crud_cash_receipts'))
        <div class="row layout-top-spacing text-white bg-dark">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow p-4 mt-4">
                    <div x-data="{ open:false }">
                        <button @click="open=!open" class="btn btn-sm btn-secondary">Create New Cash Receipt</button>
                        <div class="container" x-show="open">
                            <div class="mt-4" x-data="{ edit: @entangle('edit_todo') }">
                                <div class="form-group pl-6">
                                    <h4 class="text-white">Add New Cash Receiptst</h4>
                                </div>
                                <hr>
                                <div class="form-group row mb-1">
                                    <label for="amount" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label text-white">Amount
                                        @include('components.error',['name'=>'amount'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12">
                                        <input type="text" class="form-control" wire:model.lazy="amount" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row mb-1">
                                    <label for="cell_number" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label text-white">Cell Number
                                        @include('components.error',['name'=>'cell_number'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12">
                                        <input type="text" class="form-control" wire:model.lazy="cell_number" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row mb-1">
                                    <label for="gl_account" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label text-white">GL Account
                                        @include('components.error',['name'=>'gl_account'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12">

                                        <select class="form-control form-control-sm" wire:model.lazy="gl_account" >
                                            <option value=""></option>
                                            @include('livewire.components.glaccounts')
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-1">
                                    <label for="campus" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label text-white">Campus

                                        @include('components.error',['name'=>'campus'])</label>

                                    <div class="col-xl-8 col-lg-8 col-sm-12">
                                        <select class="form-control form-control-sm roles" wire:model.lazy="campus">
                                            <option value=""></option>
                                            @include('livewire.components.campus')
                                        </select>
                                    </div>
                                </div>



                                <div class="form-group row mb-1">
                                    <label for="short_description" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label text-white">Short Description
                                        @include('components.error',['name'=>'short_description'])
                                    </label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12" >
                                        <div >
                                            <input id="body" name="body" type="hidden" />
                                            <textarea
                                                    wire:model.lazy="short_description"
                                                    class="formatted-content h-4 form-control"
                                            ></textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <div class="form-group row mb-1">
                                    <button type="button" wire:click="create_cash_receipt" class="btn btn-success">Create Cash Receipt</button>
                                </div>
                                <div class="mt-2"><button type="button" class="btn btn-danger">Cancel</button></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="statbox widget box box-shadow p-4 mt-4">
        @if(cn('can_crud_cash_receipts') || cn('can_view_cash_receipts'))
           <livewire:transaction.cash-receipts-table />
        @endif
    </div>
</div>

@push('scripts')

@endpush
