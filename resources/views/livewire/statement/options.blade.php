
    <div class="relative inline-block text-left">
        <div>
            @if(cn("can_email_statement"))
                <button wire:click="email_statement({{$id}})" type="button"  id="menu-button" aria-expanded="true" aria-haspopup="true">
                    Email Statement
                </button>
            @endif

            @if(cn("can_export_statement"))
                <button wire:click="download_statement({{$id}})" type="button" id="menu-button" aria-expanded="true" aria-haspopup="true">
                    Download Statement
                </button>
            @endif
        </div>

    </div>
