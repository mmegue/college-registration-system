@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/tables/table-basic.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/forms/theme-checkbox-radio.css')}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"/>
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{route('requestquote')}}">My Admin</a></li>
    </ol>
@endsection

<div>
    <div class="seperator-header layout-top-spacing"></div>
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            {{--STEP 1 CELL--}}
            @if(is_null(session('cell_key')))
                <div>
                    <div class="statbox widget box box-shadow p-4">

                        @if($this->get_quote_modules()->count() > 0)
                            <h5 class="text-danger">You have an existing quotation, to continue please click on the quotation number</h5>
                            <div class="statbox widget box box-shadow mt-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-highlight-head mb-1">
                                                <thead>
                                                <tr>
                                                    <th class="">Quotation</th>
                                                    <th class="">Module</th>
                                                    <th class="">Service</th>
                                                    <th class="">Price</th>
                                                    <th class="">Status</th>
                                                    <th class="">Semester</th>
                                                    <th class="">Cell</th>
                                                    <th class="">Added By</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($this->get_quote_modules() as $temp)
                                                    <tr>
                                                        <td><a class="text-info" href="view-quote/{{$this->hash($temp->quotation_id)}}">{{$temp->quotation_number}}</a></td>
                                                        <td>{{$temp->module_code}}</td>
                                                        <td>{{Str::title(str_replace('-', ' ', $temp->service_type))}}</td>
                                                        <td>{{$temp->price}}</td>
                                                        <td><span class="badge @if($temp->status==='Quotation') badge-warning @else badge-success @endif">{{$temp->status}}</span></td>
                                                        <td>{{$temp->semester}}</td>
                                                        <td>{{$temp->cell}}</td>
                                                        <td>{{$temp->username}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif

            {{--STEP 2 CHOOSING MODULES--}}
            @if($this->get_quote_modules()->count() == 0 )
                <div class="statbox widget box box-shadow pb-4 pl-4 pr-4 pt-1" x-data="{ open:true }">
                    <div class="row mt-1 mb-2">
                        <div class="col-12 float-left">
                            <a href="#" wire:click="reset_all" class="text-danger">Reset Form</a>
                        </div>
                    </div>
                    <hr>
                    <div x-show="open">
                        <div class="row mb-4">
                            <div class="col-md-3 col-sm-12">
                                <span class="text-lg-left text-secondary font-weight-bold">Select Modules</span>
                                <div wire:ignore>
                                    <select class="module form-control form-control-sm mb-2"  wire:model="module">
                                        <option value="">Choose Module</option>
                                        @include('livewire.components.modules-select')
                                    </select>
                                </div>
                                @foreach($service_types as $styp)
                                    <div class="col-md-12 col-sm-12">
                                        <label class="mb-0 text-sm new-control new-checkbox checkbox-outline-success">
                                            <input type="checkbox"
                                                   wire:click="add_to_temp_quotation_modules('{{$styp->service_type}}')" class="new-control-input">
                                            <span class="new-control-indicator"></span>{{Str::title(str_replace('-', ' ', $styp->service_type))}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>

                            <div class="col-md-9 col-sm-12">
                                @foreach($this->my_categories() as $category)
                                    <span class="text-primary font-weight-bold text-sm-left">{{$category->category_name}}</span>
                                    <div class="row">
                                        @foreach($this->get_my_default_category_modules($category->category_id) as $module)
                                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                                                <div >
                                                    <label class="mb-0 text-sm new-control new-checkbox checkbox-outline-success" wire:ignore>
                                                        <input type="checkbox"  @if($check_temp_quote_existance) disabled @endif wire:click="add_to_temp_quotation_modules('online-zoom-class','{{$module->module_code}}')"
                                                               class="new-control-input">
                                                        <span class="new-control-indicator"></span>{{$module->module_code}}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <hr>
                    @if($this->get_temp_quote_modules($cell)->count() > 0)
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-highlight-heads mb-1">
                                        <thead>
                                        <tr>
                                            <th class="">Icons</th>
                                            <th class="">Module</th>
                                            <th class="">Service</th>
                                            <th class="">Attendance</th>
                                            <th class="">Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($this->get_temp_quote_modules($cell) as $temp)
                                            <tr>
                                                <td class="">
                                                    <ul class="table-controls">
                                                        <li>
                                                            <a href="javascript:void(0);" wire:click="delete_temp({{$temp->id}})" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i class="fa fa-trash text-danger"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    @if($temp->service_type !== 'reg-fee')
                                                        <select wire:model.lazy="temp_module_code_update.{{$temp->id}}" >
                                                            <option value="{{$temp->module_code}}">{{$temp->module_code}}</option>
                                                            @include('livewire.components.modules-select')
                                                        </select>
                                                    @else
                                                        {{$temp->module_code}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($temp->service_type !== 'reg-fee')
                                                        <select wire:model.lazy="temp_service_type_update.{{$temp->id}}" >
                                                            <option value="{{$temp->service_type}}">{{$temp->service_type}}</option>
                                                            @include('livewire.components.servicetypes-select')
                                                        </select>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($temp->service_type !== 'reg-fee')
                                                        <select wire:model.lazy="temp_attendance_update.{{$temp->id}}">
                                                            <option value="{{$temp->attendance}}">{{$temp->attendance}}</option>

                                                            @include('livewire.components.attendances')
                                                        </select>
                                                    @endif
                                                </td>
                                                <td>{{$temp->price}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot class="font-weight-bold">
                                        <tr class="text-white border-none">
                                            <td>.</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr class="bg-transparent">

                                            <td>REG FEE</td>
                                            <td><input type="checkbox" wire:model="regfee"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>

                                            <td>TOTAL</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{get_setting('registration')['currency_symbol']}}{{$this->total_temp()}}</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <div class="p-2">
                                    <h5>Personal Details</h5>
                                    <x-input-no-label class="mb-0" wire:model.lazy="username" placeholder="Student Number"></x-input-no-label>
                                    <x-input-no-label wire:model.lazy="title" placeholder="Title"></x-input-no-label>
                                    <x-input-no-label wire:model.lazy="name" placeholder="Name"></x-input-no-label>
                                    <x-input-no-label wire:model.lazy="surname" placeholder="Surname"></x-input-no-label>
                                    <x-input-no-label wire:model.lazy="cell" placeholder="Cellphone"></x-input-no-label>
                                    <x-input-no-label type="email" wire:model.lazy="email" placeholder="Email"></x-input-no-label>
                                    <label class="mb-1 text-sm new-control new-checkbox checkbox-outline-success" wire:ignore>
                                        <input type="checkbox" wire:click="$toggle('company_details')" class="new-control-input">
                                        <span class="new-control-indicator"></span>Add Company Details777
                                          </label>
                                </div>
                                @if($company_details)
                                    <div class="p-2">
                                        <h5>Company Details</h5>

                                        <x-input-no-label wire:model.lazy="company_name" placeholder="Company Name"></x-input-no-label>
                                        <x-input-no-label wire:model.lazy="address_1" placeholder="Address 1"></x-input-no-label>
                                        <x-input-no-label wire:model.lazy="address_2" placeholder="Address 2"></x-input-no-label>
                                        <x-input-no-label wire:model.lazy="suburb" placeholder="Suburb"></x-input-no-label>
                                        <x-input-no-label wire:model.lazy="city" placeholder="City"></x-input-no-label>
                                        <x-input-no-label wire:model.lazy="company_cell" placeholder="Company Cell"></x-input-no-label>
                                        <x-input-no-label type="email" wire:model.lazy="company_email" placeholder="Company Email"></x-input-no-label>
                                        <x-input-no-label wire:model.lazy="vat_number" placeholder="VAT Number"></x-input-no-label>
                                        <x-textarea-no-label class="w-100" wire:model.lazy="note" placeholder="Reg Note"></x-textarea-no-label>
                                    </div>
                                @endif
                                <x-button class="btn-dark" wire:click="create_quotation">Proceed >></x-button>
                            </div>


                        </div>
                    @endif
                </div>
            @endif
            {{--STEP 3 PERSONAL INFO--}}
            @if($personal_info)
                <div class="statbox widget box box-shadow">

                </div>
            @endif

        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('assets/js/ie11fix/fn.fix-padStart.js')}}"></script>
    <script src="{{asset('assets/js/scrollspyNav.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".module").select2({
                tags: true
            });
            $(".module").on('change',function(e){
                var data = $('.module').select2("val");
                @this.set('module', data);
            });



        });

    </script>
@endpush
