
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('categories')}}">Categories</a></li>
    </ol>
@endsection

<div>
    @if(cn('can_crud_masterdata'))
        <div class="seperator-header layout-top-spacing">
            <button wire:click="show_create" class="btn btn-dark ml-3"  type="button"><i class="fa fa-plus my-floats"></i>&nbsp;Add New Category</button>
            @include('livewire.components.import_and_export_buttons')
            @include('livewire.components.loading')
            @if($upload) @include('livewire.components.import_and_export_form')@endif
        </div>
    @endif
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                @if(($create || $edit) && cn('can_crud_masterdata'))
                    <div class="widget-content widget-content-area">
                        @if($create)<h4>Create Category</h4>@endif
                        @if($edit)<h4>Edit Category</h4>@endif
                        <small class="mb-2">Fields with * are required.</small>

                        <x-input wire:model.defer="category_name" placeholder="Category Name">Category name</x-input>
                        <x-input wire:model.defer="sequence" placeholder="Sequence">Sequence</x-input>
                        <x-textarea wire:model.defer="category_description" placeholder="Category Description">Category Description</x-textarea>

                        @if($create) <x-button wire:click="create_category" class="btn-dark">Create Category</x-button>@endif
                        @if($edit) <x-button wire:click="update_category" class="btn-secondary">Update Category</x-button>@endif
                        <x-button wire:click="reset_booleans" class="ml-2 btn-danger">Cancel</x-button>
                    </div>
                @endif
                @if(cn('can_crud_masterdata') || cn('can_view_masterdata'))
                    <div class="widget-content widget-content-area">
                          <livewire:categories.category-table/>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>

    <script>
        //var fileUpload = new FileUploadWithPreview('fileUpload')
    </script>
@endpush
