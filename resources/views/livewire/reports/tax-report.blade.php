
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('taxreports')}}">Tax Reports</a></li>
    </ol>
@endsection

<div>
    <div class="seperator-header layout-top-spacing"></div>
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                    {{$page_title}}
                </div>
            </div>

        </div>
    </div>
</div>


@section('scripts')

@endsection
