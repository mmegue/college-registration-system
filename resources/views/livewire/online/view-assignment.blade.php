
@section('page_title',$page_title)

@section('links')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/forms/switches.css')}}">
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('admin.assignments')}}">Assignments</a></li>
        <li class="breadcrumb-item active"><a>View Assignment</a></li>
    </ol>
@endsection

<div>
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div x-data="{ open: false }">
                   <div class="row px-3" x-data="{ note: false }">
                       <div class="col-md-3 col-sm-12 p-3">
                           <div x-show="!open" >
                                <button type="button" @click="open = true" class="btn-secondary">Update Assignment</button>
                           </div>

                           <div x-show="open" x-data="{opena:false}">
                               <button type="button" @click="open = false" class="btn-dark">View Assignment</button>
                               <button type="button" @click="note=!note" class="btn-info">Lecturer Note</button>
                               <br class="mt-2">
                               <button type="button" @click="opena=true" class="btn-secondary">Publish</button>
                               <button type="button" wire:click="save_to_draft" class="btn-success">Save Draft</button>

                               <div x-show="opena" class="mt-3 bg-gradient-info p-4" style="border-radius: 5px; background-color: #b1b1b1; color: #0e1726">
                                   <p class="text-danger">Are you sure you want to publish. The assignment will be marked as Completed and the student will receive the notification that the assignment is completed.</p>
                                   <button type="button" wire:click="publish_assignment" class="btn-secondary">Publish</button>
                                   <button type="button" @click="opena=false" class="btn-danger">Cancel</button>
                               </div>
                           </div>

                           <table class="mt-3">
                               <tr>
                                   <td>Student:</td>
                                   <td  class="text-secondary font-weight-bold">{{$student_name}}</td>
                               </tr>
                               <tr>
                                   <td>Student#:</td>
                                   <td  class="text-secondary font-weight-bold">{{$student_username}}</td>
                               </tr>

                               <tr>
                                   <td>Lecturer:</td>
                                   <td  class="text-secondary font-weight-bold">{{$lecturer_name}}</td>
                               </tr>

                               <tr>
                                   <td>Module:</td>
                                   <td  class="text-secondary font-weight-bold">{{$assignment_model->module_code}}</td>
                               </tr>

                               <tr>
                                   <td>Service:</td>
                                   <td  class="text-secondary font-weight-bold">{{$assignment_model->service_type}}</td>
                               </tr>

                               <tr>
                                   <td>Status:</td>
                                   <td  class="@if($assignment_model->status === 'Pending')text-danger @endif
                                   @if($assignment_model->status === 'Draft')text-warning @endif
                                   @if($assignment_model->status === 'Completed')text-success @endif font-weight-bold">{{$assignment_model->status}}</td>
                               </tr>

                               <tr>
                                   <td>Update Status:</td>
                                   <td>
                                       <label class="switch s-icons s-outline  s-outline-success mt-3 ml-2">
                                           <input wire:model="status" type="checkbox">
                                           <span class="slider round"></span>
                                   
                                   </td>
                               </tr>
                           </table>



                       </div>
                       <div class="col-md-9 col-sm-12 p-3">
                           <div x-show="note">
                               <div class="widget-content widget-content-area">

                                   <h4>Add Note To Student</h4>
                                   <hr>
                                   @include('components.error',['name'=>'lecturer_note'])
                                   <div wire:model.debounce.9999ms="lecturer_note" wire:ignore class="mb-3">
                                       <input id="body2" name="body2" autofocus value='{{ $assignment_model->lecturer_note }}' type="hidden" />
                                       <trix-editor
                                               autofocus
                                               class="formatted-content h-auto"
                                               input="body2"
                                               style="overflow-y:auto;height: 20% !important;max-height: 50% !important;"
                                       ></trix-editor>
                                   </div>
                                   <button type="button" wire:click="add_lecturer_note" class="btn-secondary">Add Note</button>
                                   <button type="button" @click="note=false" class="btn-danger">Cancel</button>
                               </div>
                           </div>
                           <div x-show="open">
                               <div class="widget-content widget-content-area">

                                   <h4>Edit Assignment</h4>
                                   <hr>
                                   @include('components.error',['name'=>'assignment'])
                                   <div wire:model.debounce.9999ms="assignment" wire:ignore class="mb-3">
                                       <input id="body" name="body" autofocus value='{{ $assignment_model->assignment }}' type="hidden" />
                                       <trix-editor
                                               autofocus
                                               class="formatted-content h-auto"
                                               input="body"
                                               style="overflow-y:auto;height: 50% !important;max-height: 100% !important;"
                                       ></trix-editor>
                                   </div>

                               </div>
                           </div>
                           <div x-show="!open">
                               <div class="p-3">
                                   {!! $assignment_model->assignment !!}
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/trix.min.css')}}">
    <script type="text/javascript" src="{{asset('assets/js/trix-core.min.js')}}"></script>
    <script src="{{asset('plugins/highlight/highlight.pack.js')}}"></script>
@endpush
