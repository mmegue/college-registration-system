
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('admin.assignments')}}">Assignment Allocations</a></li>
    </ol>
@endsection

<div>
    <div x-data="{ open: false }">
        <div x-show="open" class="seperator-header layout-top-spacing">
            <button @click="open=!open" class="btn btn-dark ml-3"  type="button"><i class="fa fa-plus my-floats"></i>&nbsp;Assign Lecturer Assignment</button>
            @include('livewire.components.loading')
        </div>

        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div x-show="open">
                        <div class="widget-content widget-content-area">
                            <small class="mb-2">Assign all unallocated assignments per module per service type to a Lecturer</small>
                            <hr>
                            <div class="form-row align-items-center">
                                <div class="col-auto">
                                    <label class="sr-only" for="inlineFormInput">Module Code</label>
                                    <select class="form-control-sm mb-2" wire:model="module_code" id="inlineFormInput">
                                        <option value="">Module Code</option>
                                        @include('livewire.components.modules-select')
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <label class="sr-only" for="inlineFormInput">Service Type</label>
                                    <select class="form-control-sm mb-2" id="inlineFormInput" wire:model="service_type">
                                        @foreach($computed_service_types as $module)
                                            <option value="{{$module->service_type}}">{{$module->service_type}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-auto">
                                    <label class="sr-only" for="inlineFormInput">Lecturers</label>
                                    <select class="form-control-sm mb-2" id="inlineFormInput" wire:model="lecturer_id">
                                        @foreach($computed_lecturers as $lecturer)
                                            <option value="{{$lecturer->lecturer_id}}">{{(new App\Models\User)->get_username($lecturer->lecturer_id)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <button wire:click="" class="btn btn-sm btn-primary mb-2">Assign To Lecturer Admin</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(cn('can_view_assignments'))
                        <div class="widget-content widget-content-area">
                            <livewire:online.assignment-allocation-table/>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>

@push('scripts')

@endpush
