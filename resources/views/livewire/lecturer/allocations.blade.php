
@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/tables/table-basic.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('lecturer.allocations')}}">Lecturer Allocations</a></li>
    </ol>
@endsection

<div>
    <div class="row layout-top-spacing">
        <div class="col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                    @livewire('lecturer.allocations-table')
                </div>
            </div>
        </div>
    </div>

</div>


@push('scripts')
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $(".module").select2({
                tags: true
            });
            $(".module").on('change',function(e){
                var data = $('.module').select2("val");
            @this.set('module', data);
            });
        });
    </script>
@endpush
