<div>
    <button type="button" class="btn btn-primary btn-sm" wire:click="open_allocation_module({{$id}})">Allocation</button>
    <ul class="list-unstyled">
        @foreach(App\Models\LecturerAllocation::where('lecturer_id',$id)->get() as $module)
            <li>*{{$module['module_code']}} @if($module['default']) <span class="text-success">{{__('----> Default')}}</span>  @endif </li>
        @endforeach
    </ul>
    <x-dialog-modal maxWidth="lg" wire:model="showModalAllocation">
        <x-slot name="title">
            {{ __('Allocate Module') }}
        </x-slot>
        <x-slot name="content">
            <h6>Default | Module Code | Save</h6><br>
            <div class="input-group mb-4">

                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <input wire:model="default" type="checkbox" class="form-control-sm new-control-input mb-2" id="inlineFormInput" >
                    </div>

                    <div class="col-auto">
                        <select class="form-control-sm mb-2" wire:model="module_code" id="inlineFormInput">
                            <option value="">Module Code</option>
                            @include('livewire.components.modules-select')
                        </select>

                    </div>
                    @include('components.error',['name'=>'module_code'])
                    <div class="col-auto">
                        <button wire:click="save_allocation" wire:loading.attr="disabled" class="btn btn-sm btn-primary mb-2" type="button">Allocate Module</button>
                    </div>
                </div>

            </div>
            <hr>
            <table x-cloak>
                <div class="row">
                @foreach($modules as $module)
                    <div class="col-sm-6">
                        <a href="#" class="mr-1" wire:click="delete_allocation('{{$module->id}}')"><i  class="fa fa-trash text-danger"></i></a>
                        {{$module['module_code']}} @if($module['default']) <span class="text-success">{{__('----> Default')}}</span>  @endif
                    </div>
                @endforeach
                </div>
            </table>
        </x-slot>
        <x-slot name="footer">

            <x-button class="btn-sm btn-danger" wire:click="$toggle('showModalAllocation')" wire:loading.attr="disabled">
            {{ __('Cancel') }}
            </x-button>

        </x-slot>
    </x-dialog-modal>

</div>