@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/apps/notes.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/forms/theme-checkbox-radio.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('notes')}}">Notes</a></li>
    </ol>
@endsection

<div>
    <div class="row layout-spacing mt-4">
        <div class="row app-notes layout-top-spacing" >
            <div class="col-lg-12 mail-box-container">
                <div class="app-hamburger-container">
                    <div class="hamburger"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu chat-menu d-xl-none"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></div>
                </div>

                <div x-data="{ open: @entangle('add_note') }" class="app-container">
                    <div class="app-note-container">
                        <div class="app-note-overlay"></div>
                        <div class="tab-title">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-12 text-center">
                                    <button id="btn-add-notes" x-on:click="open = !open"  class="btn btn-primary">@include('components.svg.plus') Add Note</button>
                                </div>
                                <div class="col-md-12 col-sm-12 col-12 mt-5">
                                    <ul class="nav nav-pills d-block" id="pills-tab3" role="tablist">
                                        <li class="nav-item">
                                            <a wire:click="get_state('all')" class="nav-link list-actions @if($state == 'all') active @endif" ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg> All Notes</a>
                                        </li>
                                        <li class="nav-item">
                                            <a wire:click="get_state('favourite')" class="nav-link list-actions @if($state == 'favourite') active @endif" ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg> Favourites</a>
                                        </li>
                                    </ul>

                                    <hr/>

                                    <p class="group-section"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7" y2="7"></line></svg> Tags</p>

                                    <ul class="nav nav-pills d-block group-list" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a wire:click="get_state('personal')" class="nav-link list-actions g-dot-primary @if($state == 'personal') active @endif" id="note-personal">Personal</a>
                                        </li>
                                        <li class="nav-item">
                                            <a wire:click="get_state('school')" class="nav-link list-actions g-dot-warning @if($state == 'school') active @endif" id="note-work">School</a>
                                        </li>
                                        <li class="nav-item">
                                            <a wire:click="get_state('social')" class="nav-link list-actions g-dot-success @if($state == 'social') active @endif" id="note-social">Social</a>
                                        </li>
                                        <li class="nav-item">
                                            <a wire:click="get_state('important')" class="nav-link list-actions g-dot-danger @if($state == 'important') active @endif" id="note-important">Important</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div x-show="open">
                            <div class="col-lg-12 col-12 layout-spacing">
                                <div class="statbox widget box box-shadow">
                                    <div class="widget-header">
                                        <div class="row">
                                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                <h4>Add New Note</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-content widget-content-area">
                                        <form>
                                            <div class="form-group mb-4">
                                                <label for="exampleFormControlInput2">Title @include('components.error',['name'=>'title'])</label>
                                                <input type="text" wire:model="title" class="form-control">
                                            </div>

                                            <div class="form-group mb-4">
                                                <label for="exampleFormControlTextarea1">Description @include('components.error',['name'=>'description'])</label>
                                                <textarea class="form-control" wire:model="description" rows="7"></textarea>
                                            </div>

                                            <button type="button" wire:click="add_note" class="mt-4 mb-4 btn btn-primary">Add Note</button>
                                            <button type="button" x-on:click="open = !open" class="mt-4 mb-4 btn btn-danger">Cancel</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div x-show="!open">
                            <div class="mt-2" style="padding:10px 0 0 30px;">{{ $this->my_notes()->links() }}</div>
                            <div id="ct" class="note-container note-grid">

                                @foreach($this->my_notes() as $note)
                                    @if($note->tag === 'Personal') <div class="note-item all-notes note-personal"> @endif
                                    @if($note->tag === 'School') <div class="note-item all-notes note-work"> @endif
                                    @if($note->tag === 'Social') <div class="note-item all-notes note-social"> @endif
                                    @if($note->tag === 'Important')
                                    <div class="note-item all-notes note-important"> @endif
                                        <div class="note-inner-content">
                                            <div class="note-content">
                                                <p class="note-title" data-noteTitle="Meeting with Kelly">{{$note->title}}</p>
                                                <p class="meta-time">{{$note->created_at}}</p>
                                                <div class="note-description-content">
                                                    <p class="note-description" data-noteDescription="Curabitur facilisis vel elit sed dapibus sodales purus rhoncus.">
                                                        {{$note->description}}</p>
                                                </div>
                                            </div>
                                            <div class="note-action">
                                                <svg @if($note->favourite) style="fill: #ffbb44;" @endif wire:click="favourite_action({{$note->id}})" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fav-note"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                <svg wire:click="delete_note({{$note->id}})" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 delete-note"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                            </div>
                                            <div class="note-footer">
                                                <div class="tags-selector btn-group">
                                                    <a class="nav-link dropdown-toggle d-icon label-group" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
                                                        <div class="tags">
                                                            <div class="g-dot-personal"></div>
                                                            <div class="g-dot-work"></div>
                                                            <div class="g-dot-social"></div>
                                                            <div class="g-dot-important"></div>
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                                        </div>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right d-icon-menu">
                                                        <a class="note-personal label-group-item label-personal dropdown-item position-relative g-dot-personal"
                                                           wire:click="assign_tag({{$note->id}}, 'Personal')" href="#"> Personal</a>
                                                        <a class="note-work label-group-item label-work dropdown-item position-relative g-dot-work"
                                                           wire:click="assign_tag({{$note->id}}, 'School')" href="javascript:void(0);"> School</a>
                                                        <a class="note-social label-group-item label-social dropdown-item position-relative g-dot-social"
                                                           wire:click="assign_tag({{$note->id}}, 'Social')" href="javascript:void(0);"> Social</a>
                                                        <a class="note-important label-group-item label-important dropdown-item position-relative g-dot-important"
                                                           wire:click="assign_tag({{$note->id}}, 'Important')" href="javascript:void(0);"> Important</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>



@push('scripts')
    <script src="{{asset('assets/js/ie11fix/fn.fix-padStart.js')}}"></script>
    <script>
        $('.hamburger').on('click', function(event) {
            $('.app-note-container').find('.tab-title').toggleClass('note-menu-show')
            $('.app-note-container').find('.app-note-overlay').toggleClass('app-note-overlay-show')
        })
        $('.app-note-overlay').on('click', function(e){
            $(this).parents('.app-note-container').children('.tab-title').removeClass('note-menu-show')
            $(this).removeClass('app-note-overlay-show')
        })
        $('.tab-title .nav-pills a.nav-link').on('click', function(event) {
            $(this).parents('.app-note-container').find('.tab-title').removeClass('note-menu-show')
            $(this).parents('.app-note-container').find('.app-note-overlay').removeClass('app-note-overlay-show')
        })
    </script>

@endpush
