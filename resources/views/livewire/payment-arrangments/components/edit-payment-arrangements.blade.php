<div>

    <div x-data="{ show : false }">
        <button @click="show=!show" type="button" class="btn btn-primary btn-sm"
                wire:click="open_edit_payment_arrangement_form({{$id}})"> Edit </button>

        <div x-show="show">

            <div style="width: 500px;margin-left: 67px;background-color: aliceblue;padding: 44px;"
                 class="input-group mb-4 mt-2 bg-gradient-info">

                <div class="row">

                    <div class="form-group row mb-1">
                        <div class="col-md-6 col-sm-12">
                            <input id="when" wire:model="when" class="form-control"
                                   type="date" placeholder="When Expecting Payment..">
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <label for="when" class="col-form-label">When Payment
                            @include('components.error',['name'=>'start_date'])</label>
                        </div>
                    </div>

                    <div class="form-group row mb-1">
                        <div class="col-md-6 col-sm-12" >
                            <input id="how_much" wire:model.defer="how_much" class="form-control"
                                   type="text" placeholder="How much">
                        </div>
                        <div class="col-md-6 col-sm-12" >
                            <label for="how_much" class="col-form-label">How much
                            @include('components.error',['name'=>'how_much'])</label>
                        </div>
                    </div>

                    <div class="form-group row mb-1">
                        <div class="col-md-6 col-sm-12">
                            <input id="send_statement" wire:model="send_statement" class="form-control"
                                   type="date" placeholder="When To Send Statement..">
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <label for="send_statement" class="col-form-label">Send Statement
                            @include('components.error',['name'=>'send_statement'])</label>
                        </div>

                    </div>

                    <div class="form-group row mb-1">
                        <div class="col-md-8 col-sm-12">
                            <select id="status" wire:model="status" class="form-control form-select"
                                   type="text" placeholder="PA Status">
                                @include('components.payment-arrangement-status')
                            </select>
                        </div>
                        <label for="status" class="col-sm-12 col-md-4 col-form-label">Status
                            @include('components.error',['name'=>'status'])</label>
                    </div>
                    <br>

                    <div class="form-group row mb-1">
                        <div class="col-md-7 col-sm-12">
                            <select id="approval" wire:model="approval" class="form-control form-select"
                                   type="text" placeholder="PA Approval Status">
                                <option value=""></option>
                                <option value="1">Approved</option>
                                <option value="0">Pending</option>
                            </select>
                        </div>
                        <label for="approval" class="col-sm-12 col-md-5 col-form-label">Approval
                            @include('components.error',['name'=>'approval'])</label>
                    </div>

                    <button wire:click="update_payment_arrangement({{$id}})" class="btn btn-sm btn-dark w-100">Save Changes</button>
                </div>

            </div>
        </div>
    </div>
</div>



