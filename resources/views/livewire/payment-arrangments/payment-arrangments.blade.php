@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/tables/table-basic.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/forms/theme-checkbox-radio.css')}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{route('quotations')}}">Quotations</a></li>
    </ol>
@endsection

<div>
    <div class="seperator-header layout-top-spacing"></div>
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <livewire:payment-arrangments.payment-arrangment-table />
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('plugins/flatpickr/flatpickr.js')}}"></script>
    <script>
        flatpickr(document.getElementById('when'));
        flatpickr(document.getElementById('send_statement'));
    </script>
@endpush
