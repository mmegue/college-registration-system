<select wire:model="lecturer" class="module w-full">
    <option value="">
        {{$username}}
    </option>
    @foreach(\App\Models\User::role('lecturer')->get() as $value)
        <option value="{{$value->id."*".$quotation_modules_id}}">{{is_null($value->username)? substr($value->name,0,1)." ".$value->surname : $value->username}}</option>
    @endforeach
</select>
