<div>

    <div x-data="{ show : false }">
        <button @click="show=!show" type="button" class="btn btn-primary btn-sm" wire:click="open_allocation_lecturer({{$id}})"> Allocate {{$module_code}} </button>

        <div x-show="show">

            <div class="input-group mb-4 mt-2 bg-gradient-info">

                <div class="form-row align-items-center">
                    {{--ALLOCATED--}}
                    @if($assignment_lecturer_allocation>0)
                        <span class="ml-1">
                            <i wire:click="remove_allocation({{$id}})" class="text-danger fa fa-trash mr-1"></i>{{$module_code}}
                            <i class="fa fa-arrow-right mr-1"></i>{{$service_type}}
                        </span>
                    @endif

                    {{--UN-ALLOCATED--}}
                    @if($assignment_lecturer_allocation==='')
                        <span class="ml-1">
                           {{$module_code}}
                            <i class="fa fa-arrow-right mr-1"></i>{{$service_type}}
                        </span>

                        <div class="col-auto">
                            <select class="form-control-sm mb-2" id="inlineFormInput" wire:model.lazy="lecturer_id">
                                <option value=""></option>
                                @foreach($computed_lecturers as $lecturer)
                                    <option value="{{$lecturer->lecturer_id}}">{{(new App\Models\User)->get_username($lecturer->lecturer_id)}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            @foreach(\App\Models\ServiceType::all() as $servicetype)
                                <div class="col-6">
                                    <div class="custom-control custom-switch">
                                        <input id="chkbx_{{ $servicetype->slug }}" type="checkbox" wire:model.lazy="selected" value="{{ $servicetype->slug }}" class="custom-control-input" />
                                        <label class="custom-control-label" for="chkbx_{{ $servicetype->slug }}">{{ $servicetype->slug }}</label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button wire:click="add_assignment_to_lecturer({{$id}})" class="btn btn-sm btn-dark">Allocate</button>
                    @endif

                </div>

            </div>
        </div>
    </div>
</div>

