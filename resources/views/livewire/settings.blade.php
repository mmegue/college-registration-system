    @section('page_title',$page_title)

    @section('links')
        <link href="{{asset('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    @endsection

    @section('breadcrumbs')
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('admin.settings')}}">Settings</a></li>
        </ol>
    @endsection

    <div>
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-card-one">
                    <div class="widget-content">
                        <div class="container">

                            <div id="navSection" data-spy="affix" class="nav  sidenav">
                                <div class="sidenav-content">
                                    <a href="#general" class="active nav-link">General</a>
                                    <a href="#address" class="nav-link">Address</a>
                                    <a href="#contact" class="nav-link">Contact Info</a>
                                    <a href="#communication" class="nav-link">Communication</a>
                                    <a href="#registration" class="nav-link">Registration</a>
                                    <a href="#banking" class="nav-link">Banking</a>
                                </div>
                            </div>

                            <div class="row layout-top-spacing">

                                <div id="general" class="col-lg-12 layout-spacing layout-top-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>General</h4>
                                                    <p class="ml-3 text-success">General System Settings</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">
                                            <div class="custom-control custom-checkbox mb-4">
                                                <input type="checkbox" class="custom-control-input" id="customCheck5"
                                                       wire:model.defer="url_hashing" @if(cn('can_update_settings'))  wire:click="update_checkbox('url_hashing')" @else disabled @endif>
                                                <label class="custom-control-label" for="customCheck5">URL Hashing</label>
                                                <small>Allow URL ids to be hashed to prevent users from typing ids directly.</small>
                                            </div>
                                             <div class="mt-4"></div>


                                            <div class="input-group">
                                                <input type="text" wire:model.defer="unauthorised_error_message" class="form-control-sm w-80" placeholder="Permission Error Message" aria-label="Permission Error Message">
                                                <div class="input-group-append">
                                                    <button class="btn btn-sm btn-primary"
                                                            @if(cn('can_update_settings'))wire:click="update('unauthorised_error_message')" @else disabled @endif type="button">Save..</button>
                                                </div>
                                            </div>
                                            <div class=" mt-0">
                                                    <span class="badge badge-dark">
                                                        <small id="sh-text4" class="form-text mt-0">Unauthorised error message</small>
                                                    </span>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div id="address" class="col-lg-12 layout-spacing layout-top-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>Address</h4>
                                                    <p class="ml-3 text-success">This address will appear on quotations, invoice and other places as it is save here.</p>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">

                                            <div class="form-group mb-1">
                                                <label for="inputAddress">Address @include('components.error',['name'=>'address1'])</label>
                                                <input type="text" class="form-control" wire:model.defer="address1">
                                            </div>
                                            <div class="form-group mb-1">
                                                <label for="inputAddress2">Address 2 @include('components.error',['name'=>'address2'])</label>
                                                <input type="text" class="form-control" wire:model.defer="address2">
                                            </div>
                                            <div class="form-row mb-1">
                                                <div class="form-group col-md-6">
                                                    <label for="inputCity">City @include('components.error',['name'=>'city'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="city">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputState">State @include('components.error',['name'=>'state'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="state">
                                                </div>
                                            </div>
                                            <div class="form-row mb-1">
                                                <div class="form-group col-md-4">
                                                    <label for="inputZip">Zip @include('components.error',['name'=>'zip'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="zip">
                                                </div>

                                                <div class="form-group col-md-8">
                                                    <label for="inputCity">Country @include('components.error',['name'=>'country'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="country">
                                                </div>
                                            </div>
                                            @if($address)
                                                <button type="button" @if(cn('can_update_settings')) wire:click="update_address('address')" @else disabled @endif
                                                class="btn btn-primary mt-3">Update Address</button>
                                            @else
                                                <button type="button" @if(cn('can_update_settings')) wire:click="create_address" @else disabled @endif
                                                class="btn btn-primary mt-3">Create Address</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div id="contact" class="col-lg-12 layout-spacing layout-top-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>Contact</h4>
                                                    <p class="ml-3 text-success">Contact details will appear on the quotations, invoices and emails</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">

                                            <div class="form-group mb-1">
                                                <label for="inputAddress">Company Name @include('components.error',['name'=>'company_name'])</label>
                                                <input type="text" class="form-control" wire:model.defer="company_name">
                                            </div>
                                            <div class="form-group mb-1">
                                                <label for="inputAddress2">Cellphone #1 @include('components.error',['name'=>'cell_1'])</label>
                                                <input type="text" class="form-control" wire:model.defer="cell_1">
                                            </div>
                                            <div class="form-row mb-1">
                                                <div class="form-group col-md-6">
                                                    <label for="inputCity">Cellphone #2 @include('components.error',['name'=>'cell_2'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="cell_2">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputState">Cellphone #3 @include('components.error',['name'=>'cell_3'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="cell_3">
                                                </div>
                                            </div>
                                            <div class="form-row mb-1">
                                                <div class="form-group col-md-4">
                                                    <label for="inputZip">email @include('components.error',['name'=>'email'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="email">
                                                </div>

                                                <div class="form-group col-md-8">
                                                    <label for="inputCity">From Email @include('components.error',['name'=>'from_email'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="from_email">
                                                </div>
                                            </div>
                                            @if($contact)
                                                <button type="button" @if(cn('can_update_settings')) wire:click="update_contact('contact')" @else disabled @endif
                                                class="btn btn-primary mt-3">Update Contact Details</button>
                                            @else
                                                <button type="button" @if(cn('can_update_settings')) wire:click="create_contact" @else disabled @endif
                                                class="btn btn-primary mt-3">Create Contact</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div id="communication" class="col-lg-12 layout-spacing layout-top-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>Communication</h4>
                                                    <p class="ml-3 text-success">Communication channels used to interact with students</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                       wire:model.defer="slack_notifications" @if(cn('can_update_settings'))  wire:click="update_checkbox('slack_notifications')" @else disabled @endif>
                                                <label class="custom-control-label" for="customCheck1">Slack Communication</label>
                                                <small>Allow the use of slack when Communicating with students.</small>
                                            </div>

                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck2"
                                                       wire:model.defer="sms_notifications" @if(cn('can_update_settings'))  wire:click="update_checkbox('sms_notifications')" @else disabled @endif>
                                                <label class="custom-control-label" for="customCheck2">SMSs Communication</label>
                                                <small>Allow the use of SMSs when Communicating with students.</small>
                                            </div>

                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck3"
                                                       wire:model.defer="email_notifications" @if(cn('can_update_settings'))  wire:click="update_checkbox('email_notifications')" @else disabled @endif>
                                                <label class="custom-control-label" for="customCheck3">Email Communication</label>
                                                <small>Allow the use of Email to send notifications to users.</small>
                                            </div>
                                            <hr>
                                            <div class="form-group mb-1">
                                                <label for="slack_webhook_prod">Slack WEB Hook URL Production  @include('components.error',['name'=>'slack_webhook_prod'])</label>
                                                <input type="text" class="form-control" wire:model.defer="slack_webhook_prod">
                                            </div>

                                            <div class="form-group mb-1">
                                                <label for="slack_webhook_sandbox"> Slack WEB Hook URL Sandbox @include('components.error',['name'=>'slack_webhook_sandbox'])</label>
                                                <input type="text" class="form-control" wire:model.defer="slack_webhook_sandbox">
                                            </div>

                                            <div class="form-group mb-1">
                                                <button type="button" @if(cn('can_update_settings')) wire:click="update_communication" @else disabled @endif
                                                class="btn btn-primary mt-3">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div id="registration" class="col-lg-12 layout-spacing layout-top-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>Registration</h4>
                                                    <p class="ml-3 text-success">Registrations Quotations Invoice settings</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">
                                            <div class="form-group mb-1">
                                                <label for="inputAddress">Currency Symbol @include('components.error',['name'=>'currency_symbol'])</label>
                                                <input type="text" class="form-control" wire:model.defer="currency_symbol">
                                            </div>
                                            <div class="form-group mb-1">
                                                <label for="inputAddress">Registration Fee @include('components.error',['name'=>'reg_fee'])</label>
                                                <input type="text" class="form-control" wire:model.defer="reg_fee">
                                            </div>
                                            <div class="form-group mb-1">
                                                <label for="inputAddress2"> Semester @include('components.error',['name'=>'semester'])</label>
                                                <input type="text" class="form-control" wire:model.defer="semester">
                                            </div>
                                            <div class="form-row mb-1">
                                                <div class="form-group col-md-6">
                                                    <label for="inputCity">Allocation Period @include('components.error',['name'=>'allocation_period'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="allocation_period">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputState"> TAX Period @include('components.error',['name'=>'tax_period'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="tax_period">
                                                </div>
                                            </div>

                                            <div class="form-group mb-1">
                                                <label for="semesters"> Semesters @include('components.error',['name'=>'semesters'])</label>
                                                <textarea class="form-control" wire:model.defer="semesters" placeholder="The format should be the same as Semester. Separate each semester with a comma E.g 2022S1,2022S2 ..."></textarea>
                                            </div>

                                            @if($registration)
                                                <button type="button" @if(cn('can_update_settings')) wire:click="update_registration('registration')" @else disabled @endif
                                                class="btn btn-primary mt-3">Update</button>
                                            @else
                                                <button type="button" @if(cn('can_update_settings')) wire:click="create_registration" @else disabled @endif
                                                        class="btn btn-primary mt-3">Create</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div id="banking" class="col-lg-12 layout-spacing layout-top-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>Banking Details</h4>
                                                    <p class="ml-3 text-success">Set the banking details here for quotes emails, invoices and statements.</p>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">

                                            <div class="form-row mb-1">
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="bank_name">Bank Name @include('components.error',['name'=>'bank_name'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="bank_name">
                                                </div>
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="account_name">Account Name @include('components.error',['name'=>'account_name'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="account_name">
                                                </div>
                                            </div>

                                            <div class="form-row mb-1">
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="account_number">Account Number @include('components.error',['name'=>'account_number'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="account_number">
                                                </div>
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="account_type">Account Type @include('components.error',['name'=>'account_type'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="account_type">
                                                </div>
                                            </div>

                                            <div class="form-row mb-1">
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="branch_code">Banch Code @include('components.error',['name'=>'branch_code'])</label>
                                                    <input type="text" class="form-control" wire:model.defer="branch_code">
                                                </div>

                                            </div>

                                            <button type="button" @if(cn('can_update_settings')) wire:click="update_banking_details('banking')" @else disabled @endif
                                            class="btn btn-primary mt-3">Update Banking</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
        <script src="{{asset('assets/js/scrollspyNav.js')}}"></script>
    @endsection
