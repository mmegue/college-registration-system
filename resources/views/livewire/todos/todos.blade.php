@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/forms/theme-checkbox-radio.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/apps/todolist.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/flatpickr/flatpickr.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('plugins/flatpickr/custom-flatpickr.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('todos')}}">Todos</a></li>
    </ol>
@endsection

<div>
    <div class="row layout-spacing mt-4">
        <div class="row layout-top-spacing">
            <div  x-data="{ create : @entangle('create') }" class="col-xl-12 col-lg-12 col-md-12">

                <div class="mail-box-container">
                    <div class="mail-overlay">

                    </div>
                    
                    <div class="tab-title">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12 text-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>
                                <h5 class="app-title">Todo List</h5>
                            </div>

                            <div class="todoList-sidebar-scroll">
                                <div class="col-md-12 col-sm-12 col-12 mt-4 pl-0">
                                    <ul class="nav nav-pills d-block mb-4" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a wire:click="get_state('All')" class="nav-link list-actions @if($state === "All") active @endif" href="#" ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-list"><line x1="8" y1="6" x2="21" y2="6"></line><line x1="8" y1="12" x2="21" y2="12"></line><line x1="8" y1="18" x2="21" y2="18"></line><line x1="3" y1="6" x2="3" y2="6"></line><line x1="3" y1="12" x2="3" y2="12"></line><line x1="3" y1="18" x2="3" y2="18"></line></svg> Inbox <span class="todo-badge badge"></span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a wire:click="get_state('Completed')" class="nav-link list-actions @if($state === "Completed") active @endif" href="#" ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg> Done <span class="todo-badge badge"></span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a wire:click="get_state('Important')" class="nav-link list-actions @if($state === "Important") active @endif" id="todo-task-important" data-toggle="pill" href="#" role="tab" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg> Important <span class="todo-badge badge"></span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a wire:click="get_state('Trashed')" class="nav-link list-actions @if($state === "Trashed") active @endif" id="todo-task-trash" data-toggle="pill" href="#" role="tab" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg> Trash</a>
                                        </li>
                                    </ul>

                                    <hr>
                                    <p class="group-section ml-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7" y2="7"></line></svg>
                                        Priority
                                    </p>
                                    <ul class="nav nav-pills d-block group-list" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a href="#" wire:click="get_state('High')" class="nav-link list-actions g-dot-primary text-danger">
                                                <i class="fa fa-circle"></i>High</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" wire:click="get_state('Medium')" class="nav-link list-actions g-dot-warning text-warning">
                                                <i class="fa fa-circle"></i>Medium</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" wire:click="get_state('Low')" class="nav-link list-actions g-dot-success text-primary">
                                                <i class="fa fa-circle"></i>Low</a>
                                        </li>

                                    </ul> 
                                </div>
                            </div>
                            @if(cn('can_create_todo') || cn('can_create_crud') || is_super_admin())
                                <a class="btn btn-dark ml-3" x-on:click="create = !create" href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                    Add New Task
                                </a>
                            @endif
                        </div>
                    </div>

                    @if(cn('can_create_todo') || cn('can_create_crud') || is_super_admin())
                        <div class="container" x-show="create">
                            <div class="todo-inbox mt-4" x-data="{ edit: @entangle('edit_todo') }">
                                <div class="form-group pl-6">
                                    <h4>Add New Todo List</h4>
                                </div>
                                <hr>
                                <div class="form-group row mb-1">
                                    <label for="title" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label">Title
                                        @include('components.error',['name'=>'title'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12">
                                        <input type="text" class="form-control" wire:model="title" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row mb-1">
                                    <label for="priority" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label">Priority
                                        @include('components.error',['name'=>'priority'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12">

                                        <select class="form-control form-control-sm" wire:model="priority" >
                                            <option value=""></option>
                                            @include('components.priorities')
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-1">
                                    <label for="start_date" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label">Start Date
                                        @include('components.error',['name'=>'start_date'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12" wire:ignore>
                                        <input id="start_date" wire:model="start_date" class="form-control" type="text" placeholder="Start Date..">
                                    </div>
                                </div>

                                <div class="form-group row mb-1">
                                    <label for="end_date" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label">End Date
                                        @include('components.error',['name'=>'end_date'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12" wire:ignore>
                                        <input type="text" class="form-control" id="end_date" wire:model="end_date" placeholder="End Date">
                                    </div>
                                </div>

                                <div class="form-group row mb-1" x-show="!edit">
                                    <label for="roles" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label">Roles
                                        @include('components.error',['name'=>'roles'])</label>

                                    <div class="col-xl-8 col-lg-8 col-sm-12" wire:ignore>
                                        <select class="form-control roles" wire:model="roles" multiple="multiple">
                                            @foreach(Helper::all_staff_roles() as $role)
                                                <option value="{{$role->name}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-1" x-show="!edit">
                                    <label for="users" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label">Users
                                        @include('components.error',['name'=>'users'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12" wire:ignore>
                                        <select class="form-control users" wire:model="users" multiple="multiple">
                                            @foreach(Helper::all_staff() as $role)
                                                <option value="{{$role->id}}">{{$role->name." ".$role->surname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-1">
                                    <label for="description" class="col-xl-4 col-sm-12 col-md-4 col-lg-4 col-form-label">Description
                                        @include('components.error',['name'=>'description'])</label>
                                    <div class="col-xl-8 col-lg-8 col-sm-12" >
                                        <div wire:model.debounce.999999ms="description" wire:ignore >
                                            <input id="body" name="body" value='{{ $description }}' type="hidden" />
                                            <trix-editor
                                                    class="formatted-content h-4"
                                                    input="body"
                                                    wire:key="uniqueKey"
                                                    style="overflow-y:auto;height: 150px !important;max-height: 150px !important;"
                                            ></trix-editor>
                                        </div>



                                    </div>
                                </div>

                                <div class="form-group row mb-1">
                                    <div x-show="!edit"><button type="button" wire:click="create_todo" class="btn btn-dark">Create Todo</button></div>
                                    <div x-show="edit"><button type="button" wire:click="update_todo" class="btn btn-success">Update Todo</button></div>
                                </div>
                                <div class="mt-2"><button type="button" x-on:click="create = !create" class="btn btn-danger">Cancel</button></div>

                            </div>
                        </div>
                    @endif

                    <div x-show="!create">
                        <div id="todo-inbox" class="accordion todo-inbox">
                            <div style="padding:0 0 0 30px;">{{ $this->my_todos()->links() }}</div>
                            <div class="search">
                                <input type="text" wire:model="searchTerm" class="form-control input-search" placeholder="Search Todos Here...">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu mail-menu d-lg-none"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                            </div>
                           
                            <div class="todo-box">

                                <div id="ct" class="todo-box-scroll">
                                    @foreach($this->my_todos() as $todo)
                                        <div x-data="{ itemdd: false }">
                                            <div class="todo-item all-list
                                                    @if($todo->status ==='Completed') todo-task-done @endif
                                                    @if($todo->importance) todo-task-important @endif
                                                    @if($status === 'Trashed') todo-task-trash @endif">
                                                <div class="todo-item-inner">
                                                    <div class="n-chk text-center">
                                                        <label class="new-control new-checkbox checkbox-primary">
                                                            <input type="checkbox" wire:click="update_status({{$todo->id}})" class="new-control-input inbox-chkbox" @if($todo->status =='Completed') checked @endif>
                                                            <span class="new-control-indicator"></span>
                                                        </label>
                                                        <svg @if($todo->importance) style="fill: #ffbb44;" @endif wire:click="assign_importance({{$todo->id}})" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fav-note"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>

                                                    </div>

                                                    <div class="todo-content" @click="itemdd = true">
                                                        <h5 class="todo-heading" data-todoHeading="Meeting with Shaun Park at 4:50pm">{{$todo->title}}</h5>
                                                        <p class="meta-date">FROM:{{date('d/m/Y', strtotime($todo->start_date))}} TO:{{date('d/m/Y', strtotime($todo->end_date))}}</p>
                                                        <p class="text-info">Assigned To: {{$this->assigned_to($todo->assigned_to)}}</p>
                                                        <p class="todo-text" >{!! strip_tags($todo->description,"<strong><em>") !!}}</p>
                                                    </div>

                                                   @if(\App\Models\Todo::check_auth())
                                                        @if($state !== 'Trashed')
                                                            <div class="priority-dropdown custom-dropdown-icon">
                                                                <div class="dropdown p-dropdown">
                                                                <a class="dropdown-toggle {{$this->class($todo->priority)}}" href="#" role="button" id="dropdownMenuLink-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12" y2="16"></line></svg>
                                                                </a>

                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-1">
                                                                    <a wire:click="assign_priority({{$todo->id}},'High')" class="dropdown-item danger" href="javascript:void(0);"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12" y2="16"></line></svg> High</a>
                                                                    <a wire:click="assign_priority({{$todo->id}},'Medium')" class="dropdown-item warning" href="javascript:void(0);"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12" y2="16"></line></svg> Middle</a>
                                                                    <a wire:click="assign_priority({{$todo->id}},'Low')" class="dropdown-item primary" href="javascript:void(0);"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12" y2="16"></line></svg> Low</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        <div class="action-dropdown custom-dropdown-icon">
                                                            <div class="dropdown">
                                                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                                                </a>

                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink-2">

                                                                    @if($state === 'Trashed')
                                                                        <a wire:click="permanently_delete_todo({{$todo->id}})" class="dropdown-item" href="javascript:void(0);">Permanent Delete</a>
                                                                        <a wire:click="restore_todo({{$todo->id}})" class="dropdown-item" href="javascript:void(0);">Restore Task</a>
                                                                    @else
                                                                        <a wire:click="show_edit_form({{$todo->id}})" class="edit dropdown-item" href="javascript:void(0);">Edit</a>
                                                                        <a wire:click="delete_todo({{$todo->id}})" class="dropdown-item delete" href="javascript:void(0);">Delete</a>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>
                                            <ul
                                                    x-show="itemdd"
                                                    @click.away="itemdd = false"
                                            >
                                                <div style="background-color:#d5d5d5; color:#000; border-radius: 5px; padding:5px" >
                                                    <p class="ml-2 border border-success">{!! $todo->description !!}</p>
                                                </div>
                                            </ul>
                                        </div>

                                    @endforeach
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script src="{{asset('assets/js/ie11fix/fn.fix-padStart.js')}}"></script>
{{--    <script src="{{asset('assets/js/apps/todoList.js')}}"></script>--}}
    <script src="{{asset('plugins/flatpickr/flatpickr.js')}}"></script>
{{--    <script src="{{asset('plugins/flatpickr/custom-flatpickr.js')}}"></script>--}}
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
{{--    <script src="{{asset('plugins/select2/custom-select2.js')}}"></script>--}}
    <script>
        var f1 = flatpickr(document.getElementById('start_date'));
        var f5 = flatpickr(document.getElementById('end_date'));

        $(document).ready(function() {
            $(".roles").select2({
                tags: true
            });
            $(".roles").on('change',function(e){
                var data = $('.roles').select2("val");
                @this.set('roles', data);
            });

            $(".users").select2({
                tags: true
            });
            $(".users").on('change',function(e){
                var data = $('.users').select2("val");
                @this.set('users', data);
            });

        });

    </script>

    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/trix.min.css')}}">
    <script type="text/javascript" src="{{asset('assets/js/trix-core.min.js')}}"></script>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.css" integrity="sha512-5m1IeUDKtuFGvfgz32VVD0Jd/ySGX7xdLxhqemTmThxHdgqlgPdupWoSN8ThtUSLpAGBvA8DY2oO7jJCrGdxoA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix-core.min.js" integrity="sha512-lyT4F0/BxdpY5Rn1EcTA/4OTTGjvJT9SxWGxC1boH9p8TI6MzNexLxEuIe+K/pYoMMcLZTSICA/d3y0ColgiKg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
@endpush
