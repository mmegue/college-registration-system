
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('timetable.weekend.sat')}}">Timetable Weekend Sat</a></li>
    </ol>
@endsection

<div>
    <div class="seperator-header layout-top-spacing">
        <button wire:click="download_this_timetable" class="btn btn-dark ml-3"  type="button"><i class="fa fa-cloud-download my-floats"></i>&nbsp;Download Timetable</button>
        <button wire:click="add_row" class="btn btn-dark ml-3"  type="button"><i class="fa fa-plus-circle my-floats"></i>&nbsp;Add Row</button>
    </div>

    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                    <livewire:timetable.timetable-weekend-sat-table/>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')

@endpush
