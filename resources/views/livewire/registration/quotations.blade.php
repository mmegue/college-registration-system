
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('quotations')}}">Quotations</a></li>
    </ol>
@endsection

<div>
    <div class="seperator-header layout-top-spacing mt-4 ml-2">
        <a class="btn btn-dark" href="{{route('requestquote')}}">Request Quotation</a>
    </div>

    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">

            <div class="statbox widget box box-shadow">
                <livewire:registration.quotation-table/>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
@endpush
