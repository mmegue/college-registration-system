<div x-data="{ open: false }">
    <div class="btn-group" role="group" >
        @if(cn('can_view_quote'))
            <div><a href="{{route('viewquote',$this->hash($id))}}" type="button" class="btn btn-sm btn-info px-1 py-0"><i class="fa fa-eye"></i></a></div>
        @endif
        @if(cn('can_delete_quote'))
            <div x-show="!open" ><button @click="open=true" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-trash"></i></button></div>
            <div x-show="open" ><button @click="open=false" wire:click="delete_row({{$this->hash($id)}})" type="button" class="btn btn-sm btn-success px-1 py-0"><i class="fa fa-check"></i></button></div>
            <div x-show="open" ><button @click="open=false" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-times"></i></button></div>
        @endif
    </div>
</div>