@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/tables/table-basic.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/forms/theme-checkbox-radio.css')}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('plugins/flatpickr/flatpickr.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('plugins/flatpickr/custom-flatpickr.css')}}" rel="stylesheet" type="text/css"

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{route('quotations')}}">Quotations</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="#">View Quotation</a></li>

    </ol>
@endsection
@php
    $cache_data = $this->data();
    $transactions = $this->get_quote_transactions();
    $p_as = $this->get_payment_arrangments();
    $quotation_modules = $this->get_quotation_modules();
    $deleted_quotation_modules = $this->get_deleted_quotation_modules();
@endphp
<div>
    <div class="row layout-spacing" >
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-top-spacing">
            <div class="row" >
                {{--STUDENT DETAILS --}}

                {{--TABLES--}}
                <div class="col-sm-12">
                     {{--MAIN BUTTONS--}}
                    <div class="my-2">

                        <div class="btn-group mb-1 mr-1" role="group">
                            <button id="btndefault" type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false"> <i class="fa fa-eye"></i>View <i class="fa fa-sort-down"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btndefault">
                                <a wire:click="open('qt')" class="dropdown-item" role="button">Quote</a>
                                @if($this->quotation->status == 'Invoiced')
                                    <a wire:click="open('in')" class="dropdown-item" role="button">Invoice</a>
                                    <a wire:click="open('st')" class="dropdown-item" role="button">Statement</a>
                                @endif
                            </div>
                        </div>

                        <div class="btn-group mb-1 mr-1" role="group">
                            <button id="btndefault" type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false"> <i class="fa fa-download"></i>Download <i class="fa fa-sort-down"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btndefault">
                                @if(cn('can_download_quote'))<a wire:click="download_quotation" role="button" class="dropdown-item"><i class="flaticon-home-fill-1 mr-1"></i>Quotation</a> @endif
                                @if(cn('can_download_invoice') && $this->quotation->status == 'Invoiced')
                                        <a wire:click="download_invoice" role="button" class="dropdown-item"><i class="flaticon-gear-fill mr-1"></i>
                                        Invoice</a>
                                @endif
                                @if(cn('can_download_statement') && $this->quotation->status == 'Invoiced')
                                        <a wire:click="download_statement('{{$this->quotation->user->id}}')" role="button" class="dropdown-item">
                                            <i class="flaticon-bell-fill-2 mr-1"></i>Statement
                                        </a>
                                @endif
                            </div>
                        </div>

                        <div class="btn-group mb-1 mr-1" role="group">
                            <button id="btndefault" type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false"> <i class="fa fa-envelope"></i>Email <i class="fa fa-sort-down"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btndefault">
                                @if(cn('can_email_quote'))<a role="button" class="dropdown-item"><i class="flaticon-home-fill-1 mr-1"></i>Quotation</a> @endif
                                @if(cn('can_email_invoice') && $this->quotation->status === 'Invoiced')<a role="button" class="dropdown-item"><i class="flaticon-gear-fill mr-1"></i>Invoice</a>@endif
                                @if(cn('can_email_statement') && $this->quotation->status === 'Invoiced')<a role="button" class="dropdown-item"><i class="flaticon-bell-fill-2 mr-1"></i>Statement</a>@endif
                            </div>
                        </div>
                        @if($this->quotation->status === "Quotation" || cn('can_edit_invoice'))
                            @if(cn('can_request_quote'))<x-button wire:click="open('create')" class="btn-dark">Add Module</x-button>@endif
                            @if(cn('can_crud_cash_receipts'))<x-button wire:click="open('transaction')" class="btn-dark">Add Cash Receipt</x-button> @endif
                            @if(cn('can_invoice_quote'))<x-button wire:click="open('invoice')" class="btn-dark">{{($quotation->status==='Invoiced')? "Add Payment Arrangements" : "Invoice Quotation"}}</x-button>@endif
                            @if(cn('can_edit_quote'))<x-button wire:click="open('update')" class="btn-dark">Update Details</x-button>@endif
                        @endif

                    </div>

                    {{--SUB LINKS--}}
                    <div class="mb-2">
                        <a wire:click="open('pi')" class="text-info @if($state==='pi') bg-warning text-white @endif" role="button"><u>Personal Information</u></a> |
                        <a wire:click="open('cm')" class="text-info @if($state==='cm') bg-warning text-white @endif" role="button"><u>Modules</u></a> |
                        <a wire:click="open('t')" class="text-info @if($state==='t') bg-warning text-white @endif" role="button"><u>Trans</u></a> |
                        <a wire:click="open('pa')" class="text-info @if($state==='pa') bg-warning text-white @endif" role="button"><u>PMT</u></a> |
                        <a wire:click="open('dm')" class="text-info @if($state==='dm') bg-warning text-white @endif" role="button"><u>Deleted Modules</u></a> |
                    </div>

                    @if($this->quotation->status === "Quotation" || cn('can_edit_invoice'))
                       @if($state === "create")
                            {{--ADD MODULE--}}
                            <div class="row mb-4">
                                <div class="col-md-3 col-sm-12">
                                    @if(!$this->reg_fee_existance())
                                        <input type="checkbox" wire:model="regfee"> Reg Fee <br><br>
                                    @endif

                                    <span class="text-lg-left text-secondary font-weight-bold">Select Modules</span>
                                    <div wire:ignore>
                                        <select class="modulee form-control form-control-sm mb-2"  wire:model="module">
                                            <option value="">Choose Module</option>
                                            @include('livewire.components.modules-select')
                                        </select>
                                    </div>
                                    @foreach($service_types as $styp)
                                        <div class="col-md-12 col-sm-12">
                                            <label class="mb-0 text-sm new-control new-checkbox checkbox-outline-success">
                                                <input type="checkbox"
                                                       wire:click="add_to_temp_quotation_modules('{{$styp->service_type}}')" class="new-control-input">
                                                <span class="new-control-indicator"></span>{{Str::title(str_replace('-', ' ', $styp->service_type))}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                       @endif

                        @if($state === "transaction")
                            {{--ADD TRANSACTION--}}
                        <div>
                            <div class="statbox widget box box-shadow p-4">
                                <div class="col-md-12 col-sm-12">
                                    <x-input wire:model.defer="amount" placeholder="Amount">Amount</x-input>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <select class="form-control form-control-sm mb-2"  wire:model.defer="gl_account">
                                                <option value="">Choose GL Account</option>
                                                @foreach(\App\Http\Traits\Status::gl_acc() as $gl)
                                                    <option value="{{$gl}}">{{$gl}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label for="gl_account" class="col-sm-6 col-form-label col-form-label-md">GL Account &nbsp;<br>
                                            @include('components.error',['name'=>'gl_account'])</label>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <select class="form-control form-control-sm mb-2"  wire:model.defer="campus">
                                                <option value="">Choose Campus</option>
                                                @foreach(\App\Http\Traits\Status::campus() as $campus)
                                                    <option value="{{$campus}}">{{$campus}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label for="campus" class="col-sm-6 col-form-label col-form-label-md">Campus &nbsp;<br>
                                            @include('components.error',['name'=>'campus'])</label>
                                    </div>

                                    <x-textarea wire:model.defer="description" placeholder="Description">Description</x-textarea>
                                    <x-button class="btn-success" wire:click="add_cash_receipt">Create Cash Receipt</x-button>
                                </div>
                            </div>
                        </div>
                        @endif

                        @if($state === "invoice")
                            {{--INVOICE QUOTATION--}}
                            <div>
                                <div>
                                    <div class="widget p-4">
                                        <div class="row">
                                            <div class="col-6">
                                                <h5>Add payment Arrangement</h5>
                                                <div class="form-group row mb-1">
                                                    <div class="col-md-6 col-sm-12" wire:ignore>
                                                        <input id="when" wire:model="when" class="form-control" type="date" placeholder="When Expecting Payment..">
                                                    </div>
                                                    <label for="when" class="col-sm-12 col-md-6 col-form-label">When Expecting Payment
                                                        @include('components.error',['name'=>'start_date'])</label>
                                                </div>
                                                <x-input wire:model.defer="how_much" class="mb-1">How Much</x-input>
                                                <div class="form-group row mb-1">
                                                    <div class="col-md-6 col-sm-12" wire:ignore>
                                                        <input id="send_statement" wire:model="send_statement" class="form-control" type="date" placeholder="When To Send Statement?">
                                                    </div>
                                                    <label for="send_statement" class="col-sm-12 col-md-6 col-form-label">When To Send Statement
                                                        @include('components.error',['name'=>'send_statement'])
                                                    </label>
                                                </div>

                                                {{--
                                                    <div class="form-group row mb-1">
                                                        <div class="col-md-6 col-sm-12" wire:ignore>
                                                            <select wire:model.defer="status" class="form-control">
                                                                @include('components.payment-arrangement-status')
                                                            </select>
                                                        </div>
                                                        <label for="status" class="col-sm-12 col-md-6 col-form-label">Status
                                                            @include('components.error',['name'=>'status'])</label>
                                                    </div>
                                                --}}

                                                <x-button wire:click="add_payment_arrangement" class="btn-secondary">Add Payment Arrangement <i class="fa fa-plus-circle"></i></x-button>
                                                @if($quotation->status==='Quotation')
                                                    <x-button wire:click="invoice_quotation" class="btn-success">
                                                    Continue With Invoicing <i class="fa fa-arrow-alt-circle-right"></i></x-button>
                                                @endif

                                            </div>
                                            <div class="col-6">
                                                 <h5>Amounts</h5>
                                                <table>

                                                    <tr><td>Invoiced Amount:</td> <td> {{get_setting('registration')['currency_symbol']}}{{$cache_data['total']}}</td></tr>
                                                    <tr><td>Paid Amount:</td> <td> {{get_setting('registration')['currency_symbol']}}{{$transactions->sum('amount')}}</td></tr>
                                                    <tr><td>Balance Amount:</td> <td> {{get_setting('registration')['currency_symbol']}}{{$cache_data['total']-$transactions->sum('amount')}}</td></tr>
                                                    <tr @if($p_as->sum('how_much') != $cache_data['total'])style="color:red" @else style="color:green" @endif><td>Balance From PAs:</td>
                                                        <td><strong> {{get_setting('registration')['currency_symbol']}}{{$cache_data['total']-$p_as->sum('how_much')}}</strong></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <hr>
                                        <table class="table table-striped table-hover table-highlight-head mb-1">
                                            <thead>
                                            <tr>
                                                <th class="">Payment Expected</th>
                                                <th class="">How Much</th>
                                                <th>Added By</th>
                                                <th class="">Created At</th>
                                                <th class="">Updated At</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($p_as as $pa)
                                                <tr>
                                                    <td>{{$pa->when}}</td>
                                                    <td>{{$pa->how_much}}</td>
                                                    <td>{{$pa->username}}</td>
                                                    <td>{{$pa->created_at}}</td>
                                                    <td>{{$pa->updated_at}}</td>
                                                    <td class="text-center">
                                                        <div x-data="{ op: false }">
                                                            <div class="btn-group" role="group" >
                                                                <div x-show="!op" ><button @click="op=true" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-trash"></i></button></div>
                                                                <div x-show="op" ><button @click="op=false" wire:click="delete_payment_arrangement({{$pa->id}})" type="button" class="btn btn-sm btn-success px-1 py-0"><i class="fa fa-check"></i></button></div>
                                                                <div x-show="op" ><button @click="op=false" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-times"></i></button></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($state === "update")
                            {{--UPDATE DETAILS--}}
                        <div>
                            <div class="row">
                                <div x-data="{show:false}" class="col-sm-12">

                                    <div x-show="!show">
                                        <div class="col-md-6 col-sm-12">

                                            <h5>Personal Details</h5>
                                            <x-input-no-label class="mb-0" wire:model.defer="user.username" placeholder="Student Number"></x-input-no-label>
                                            <x-input-no-label wire:model.defer="userModel.title" ></x-input-no-label>
                                            <x-input-no-label wire:model.defer="userModel.name" ></x-input-no-label>
                                            <x-input-no-label wire:model.defer="userModel.surname" placeholder="Surname"></x-input-no-label>
                                            <x-input-no-label wire:model.defer="userModel.cell" placeholder="Cellphone"></x-input-no-label>
                                            <x-input-no-label wire:model.defer="userModel.email" placeholder="Email" type="email"></x-input-no-label>


                                        </div>
                                    </div>
                                    <div x-show="show">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="">
                                                <h5>Company Details</h5>
                                                <x-input-no-label wire:model.defer="company.company_name" placeholder="Company Name"></x-input-no-label>
                                                <x-input-no-label wire:model.defer="company.address_1" placeholder="Address 1"></x-input-no-label>
                                                <x-input-no-label wire:model.defer="company.address_2" placeholder="Address 2"></x-input-no-label>
                                                <x-input-no-label wire:model.defer="company.suburb" placeholder="Suburb"></x-input-no-label>
                                                <x-input-no-label wire:model.defer="company.city" placeholder="City"></x-input-no-label>
                                                <x-input-no-label wire:model.defer="company.company_cell" placeholder="Company Cell"></x-input-no-label>
                                                <x-input-no-label wire:model.defer="company.company_email" placeholder="Company Email" type="email"></x-input-no-label>
                                                <x-input-no-label wire:model.defer="company.vat_number" placeholder="VAT Number"></x-input-no-label>
                                                <x-textarea-no-label class="w-100" wire:model.defer="company.note" placeholder="Reg Note"></x-textarea-no-label>
                                            </div>

                                        </div>
                                    </div>
                                    <label class="mb-2 mt-3 ml-3 text-sm new-control new-checkbox checkbox-outline-success">
                                        <input type="checkbox" class="new-control-input" @click="show=!show">
                                        <span class="new-control-indicator"></span><span>Show Company Details</span>
                                    </label>
                                    <x-button class="btn-dark" wire:click="update_details_quotation">Save Changes</x-button>
                                </div>
                            </div>
                        </div>
                        @endif

                    @endif

                    {{--PERSONAL INFORMATION--}}
                    @if($state === "pi")
                        <div class="table-responsive">
                            <h5 class="mt-2 text-info underline">Client Details</h5>
                            <table class="table-striped">
                                <tr>
                                    <td><strong>Title:</strong></td>
                                    <td>{{$this->quotation->user->title}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Name:</strong></td>
                                    <td>{{$this->quotation->user->name}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Surname:</strong></td>
                                    <td>{{$this->quotation->user->surname}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Email:</strong></td>
                                    <td>{{$this->quotation->user->email}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Cell:</strong></td>
                                    <td>{{$this->quotation->user->cell}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Student #:</strong></td>
                                    <td>{{$this->quotation->user->username}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Status:</strong></td>
                                    @if($this->quotation->status==='Invoiced')
                                        <td class="text-success text-left font-weight-bold">{{$this->quotation->status}}</td>
                                    @endif
                                    @if($this->quotation->status==='Quotation')
                                        <td class="text-danger text-left">{{$this->quotation->status}}</td>
                                    @endif
                                    @if($this->quotation->status==='Registered')
                                        <td class="text-info text-left">{{$this->quotation->status}}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><strong>Quote #:</strong></td>
                                    <td>{{$this->quotation->quotation_number}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Invoice #:</strong></td>
                                    <td>{{$this->quotation->invoice_number}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Created By:</strong></td>
                                    <td>{{$created_by}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Date created:</strong></td>
                                    <td>{{$this->quotation->created_at}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Last updated:</strong></td>
                                    <td>{{$this->quotation->updated_at}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Student Notes:</strong></td>
                                    <td>{{$this->quotation->note}}</td>
                                </tr>
                            </table>

                            <h5 class="mt-3 text-info underline">Company Details</h5>
                            <table class="table-striped">
                                <tr>
                                    <td><strong>Company Name:</strong></td>
                                    <td>{{$this->company->company_name}}</td>
                                </tr>

                                <tr>
                                    <td><strong>Company Address 1:</strong></td>
                                    <td>{{$this->company->address_1}}</td>
                                </tr>

                                <tr>
                                    <td><strong>Company Address 2:</strong></td>
                                    <td>{{$this->company->address_2}}</td>
                                </tr>

                                <tr>
                                    <td><strong>Company Suburb:</strong></td>
                                    <td>{{$this->company->suburb}}</td>
                                </tr>

                                <tr>
                                    <td><strong>Company City:</strong></td>
                                    <td>{{$this->company->city}}</td>
                                </tr>

                                <tr>
                                    <td><strong>Company VAT#:</strong></td>
                                    <td>{{$this->company->vat_number}}</td>
                                </tr>

                            </table>
                        </div>
                    @endif

                    @if($state === "cm")
                        {{--SELECTED MODULES--}}
                        <div class="table-responsive" >
                            <table class="table table-hover table-highlight-head mb-1">
                                <thead>
                                <tr>
                                    <th class="">Module</th>
                                    <th class="">Lecturer</th>
                                    <th class="">Service</th>
                                    <th class="">Attendance</th>
                                    <th class="">Semester</th>
                                    <th class="">Price</th>
                                    <th class="">Added By</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($quotation_modules as $quotation_module)
                                    <tr>
                                        <td>
                                            @if(cn('can_edit_invoice') || $this->quotation->status === "Quotation")
                                                <div x-data="{ open: false }">
                                                    <div x-show = "open">
                                                        <select x-on:change="open=false" wire:model="temp_module_code_update.{{$quotation_module->id}}" >
                                                            <option value="{{$quotation_module->module_code}}">{{$quotation_module->module_code}}</option>
                                                            @include('livewire.components.modules-select')
                                                        </select> <span role="button" @click="open = false" class="badge badge-danger">X</span>
                                                    </div>
                                                    <div x-show="!open" data-placement="top" title="Click to edit!" >
                                                        <span role="button" @click="open=true" class="text-info" >{{$quotation_module->module_code}}</span>
                                                    </div>
                                                </div>
                                            @else
                                                {{$quotation_module->module_code}}
                                            @endif
                                        </td>
                                        <td>
                                            @if(cn('can_assign_modules_to_lecturers') || $this->quotation->status === "Quotation")
                                                <div x-data="{ open: false }">
                                                    <div x-show = "open">
                                                        <select x-on:change="open=false" wire:model="temp_lecturer_update.{{$quotation_module->id}}" >
                                                            <option value="{{$quotation_module->lecturer_id_default}}">{{$quotation_module->lect}}</option>
                                                            @include('livewire.components.lecturer-select')
                                                        </select> <span role="button" @click="open = false" class="badge badge-danger">X</span>
                                                    </div>
                                                    <div x-show="!open" data-placement="top" title="Click to edit!" >
                                                    <span role="button" @click="open=true" class="text-info" >
                                                        @if(is_null($quotation_module->lecturer_id_default)) <span class="text-danger">unAllocated</span> @else {{$quotation_module->lect}} @endif
                                                    </span>
                                                    </div>
                                                </div>
                                            @else
                                                @if(is_null($quotation_module->lecturer_id_default)) unAllocated @else {{$quotation_module->lect}} @endif
                                            @endif

                                        </td>
                                        <td>
                                            @if(cn('can_edit_invoice') || $this->quotation->status === "Quotation")
                                                <div x-data="{ open: false }">
                                                    <div x-show = "open">
                                                        <select x-on:change="open=false" wire:model="temp_service_type_update.{{$quotation_module->id}}" >
                                                            <option value="{{$quotation_module->service_type}}">{{Str::title(str_replace('-', ' ', $quotation_module->service_type))}}</option>
                                                            @include('livewire.components.servicetypes-select')
                                                        </select> <span role="button" @click="open = false" class="badge badge-danger">X</span>
                                                    </div>
                                                    <div x-show="!open" data-placement="top" title="Click to edit!" >
                                                        <span role="button" @click="open=true" class="text-info" >{{$quotation_module->service_type}}</span>
                                                    </div>
                                                </div>
                                            @else {{$quotation_module->service_type}} @endif
                                        </td>
                                        <td>
                                            @if(cn('can_edit_invoice') || $this->quotation->status === "Quotation")
                                                <div x-data="{ open: false }">
                                                    <div x-show = "open">
                                                        <select  x-on:change="open=false" wire:model="temp_attendance_update.{{$quotation_module->id}}">
                                                            <option value="{{$quotation_module->attendance}}">{{$quotation_module->attendance}}</option>
                                                            @include('livewire.components.attendances')
                                                        </select> <span role="button" @click="open = false" class="badge badge-danger">X</span>
                                                    </div>
                                                    <div x-show="!open" data-placement="top" title="Click to edit!" >
                                                    <span role="button" @click="open=true" class="text-info" >
                                                        @if(is_null($quotation_module->attendance)) None @else {{$quotation_module->attendance}} @endif
                                                    </span>
                                                    </div>
                                                </div>
                                            @else
                                                @if(is_null($quotation_module->attendance)) None @else {{$quotation_module->attendance}} @endif
                                            @endif
                                        </td>
                                        <td>
                                            @if(cn('can_edit_invoice') || $this->quotation->status === "Quotation")
                                                <div x-data="{ open: false }">
                                                    <div x-show = "open">
                                                        <select  x-on:change="open=false" wire:model="temp_semester_update.{{$quotation_module->id}}">
                                                            <option value="{{$quotation_module->semester}}">{{$quotation_module->semester}}</option>
                                                            @include('livewire.components.semester-select')
                                                        </select> <span role="button" @click="open = false" class="badge badge-danger">X</span>
                                                    </div>
                                                    <div x-show="!open" data-placement="top" title="Click to edit!" >
                                                <span role="button" @click="open=true" class="text-info" >
                                                    @if(is_null($quotation_module->semester)) None @else {{$quotation_module->semester}} @endif
                                                </span>
                                                    </div>
                                                </div>
                                            @else
                                                @if(is_null($quotation_module->semester)) None @else {{$quotation_module->semester}} @endif
                                            @endif
                                        </td>
                                        <td>
                                            @if(cn('can_edit_invoice') || $this->quotation->status === "Quotation")
                                                <div x-data="{ open: false }">
                                                    <div x-show="open" >
                                                    <span class="w-75">
                                                        <input autofocus  wire:model.defer="temp_price_update.{{$quotation_module->id}}"
                                                               type="text" placeholder="{{$quotation_module->price}}">
                                                        <button @click="open=false" wire:click="tempPriceUpdate" type="button" class="btn btn-sm btn-success px-1 py-0"><i class="fa fa-check"></i></button>
                                                        <i @click="open=false" class="fa fa-times text-danger"></i>
                                                    </span>
                                                    </div>
                                                    <div x-show="!open" data-placement="top" title="Click to edit!" >
                                                        <a href="#" @click="open=true" class="text-info" >{{$quotation_module->price}}</a>
                                                    </div>
                                                </div>
                                            @else
                                                {{get_setting('registration')['currency_symbol']}}{{$quotation_module->price}}
                                            @endif
                                        </td>
                                        <td>{{$quotation_module->addedby}}</td>
                                        <td class="text-center">
                                            @if(cn('can_delete_quote') )
                                                <div x-data="{ ope: false }">
                                                    <div class="btn-group" role="group" >
                                                        <div x-show="!ope" ><button @click="ope=true" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-trash"></i></button></div>
                                                        <div x-show="ope" ><button @click="ope=false" wire:click="delete_row({{$quotation_module->id}})" type="button" class="btn btn-sm btn-success px-1 py-0"><i class="fa fa-check"></i></button></div>
                                                        <div x-show="ope" ><button @click="ope=false" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-times"></i></button></div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                    @if($state === "t")
                        {{--TRANSACTIONS--}}
                        <div class="table-responsive" >
                        <table class="table table-hover table-highlight-head mb-1">
                            <thead>
                                <tr>
                                    <th class="">Amount</th>
                                    <th class="">CreatedBy</th>
                                    <th class="">Item</th>
                                    <th class="">Cell</th>
                                    <th class="">Semester</th>
                                    <th class="">Trans Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{$transaction->amount}}</td>
                                    <td>{{$transaction->username}}</td>
                                    <td>{{$transaction->item}}</td>
                                    <td>{{$transaction->cell}}</td>
                                    <td>{{$transaction->semester_period}}</td>
                                    <td>{{$transaction->transaction_date}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif

                   @if($state === "pa")
                        {{--PAYMENT ARRANGMENTS--}}
                        <div class="table-responsive" >
                        <table class="table table-striped table-hover table-highlight-head mb-1">
                            <thead>
                            <tr>
                                <th class="">Payment Expected</th>
                                <th class="">How Much</th>
                                <th class="">Added By</th>
                                <th class="">Created At</th>
                                <th class="">Updated At</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($p_as as $pa)
                                <tr>
                                    <td>{{$pa->when}}</td>
                                    <td>{{$pa->how_much}}</td>
                                    <td>{{$pa->username}}</td>
                                    <td>{{$pa->created_at}}</td>
                                    <td>{{$pa->updated_at}}</td>
                                    <td class="text-center">
                                        @if(cn('can_delete_payment_arrangement') )
                                            <div x-data="{ op: false }">
                                                <div class="btn-group" role="group" >
                                                    <div x-show="!op" ><button @click="op=true" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-trash"></i></button></div>
                                                    <div x-show="op" ><button @click="op=false" wire:click="delete_payment_arrangement({{$pa->id}})" type="button" class="btn btn-sm btn-success px-1 py-0"><i class="fa fa-check"></i></button></div>
                                                    <div x-show="op" ><button @click="op=false" type="button" class="btn btn-sm btn-danger px-1 py-0"><i class="fa fa-times"></i></button></div>
                                                </div>
                                            </div>
                                         @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                   @endif

                    @if($state === "dm")
                        {{--DELETED MODULES--}}
                        <div class="table-responsive" >
                        <table class="table table-hover table-highlight-head mb-1">
                            <thead>
                                <tr>
                                    <th class="">Module</th>
                                    <th class="">Lecturer</th>
                                    <th class="">Service</th>
                                    <th class="">Attendance</th>
                                    <th class="">Semester</th>
                                    <th class="">Price</th>
                                    <th class="">Added By</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($deleted_quotation_modules as $deleted)
                                    <tr>
                                        <td>{{$deleted->module_code}}</td>
                                        <td>{{$deleted->alt_lecturer_id}}</td>
                                        <td>{{$deleted->service_type}}</td>
                                        <td>{{$deleted->attendance}}</td>
                                        <td>{{$deleted->semester}}</td>
                                        <td>{{$deleted->price}}</td>
                                        <td>{{$deleted->username}}</td>
                                        <td class="text-center">
                                            @if(cn('can_permanent_delete') )
                                                <button type="button" class="btn btn-sm btn-success px-1 py-0" data-placement="top" title="Restore"
                                                        wire:click="restore_deleted_module_quotation({{$deleted->id}})"
                                                ><i class="fa fa-redo"></i>
                                                </button>

                                                <button type="button" class="btn btn-sm btn-danger px-1 py-0" data-placement="top" title="Permanent Delete"
                                                        wire:click="permanent_delete_module_quotation({{$deleted->id}})"
                                                ><i class="fa fa-trash"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif

                   @if($state === "qt")
                         {{--VIEW PDF QUOTE--}}
                        <div >
                        <div class="container-md float-left">
                            @include('livewire.registration.view-pdf.quotation',$cache_data)
                        </div>
                    </div>
                   @endif

                    @if($state === "in")
                         {{--VIEW PDF INVOICE--}}
                        <div >
                        <div class="container-md float-left">
                            @include('livewire.registration.view-pdf.invoice',$cache_data)
                        </div>
                    </div>
                    @endif

                    @if($state === "st")
                        {{--VIEW PDF STATEMENT--}}
                        <div >
                            <div class="container-md float-left">
                                @include('livewire.registration.view-pdf.statement',$this->statement_data())
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('assets/js/ie11fix/fn.fix-padStart.js')}}"></script>
    <script src="{{asset('assets/js/scrollspyNav.js')}}"></script>
    <script src="{{asset('plugins/flatpickr/flatpickr.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script>
        f1 = flatpickr(document.getElementById('when'));
        f2 = flatpickr(document.getElementById('send_statement'));
        $(document).ready(function() {
            $(".modulee").select2({
                tags: true
            });
            $(".modulee").on('change',function(e){
                var data = $('.modulee').select2("val");
            @this.set('modulee', data);
            });
        });

    </script>
@endpush


