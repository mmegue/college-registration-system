
@section('page_title',$page_title)

@section('links')

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('registrations')}}">Registrations</a></li>
    </ol>
@endsection

<div>
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">

            <div class="statbox widget box box-shadow">
                <livewire:registration.registration-table/>
            </div>
        </div>
    </div>
</div>

@push('scripts')

@endpush
