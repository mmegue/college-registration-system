<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <style>

        @font-face {
            font-family: SourceSansPro;
        }

        .watermark{
            display: block;
            margin-top:15%;
            margin-bottom:15%;
            margin-left: auto;
            margin-right: auto;
            width: 100%;
            opacity: 0.1;
            position: absolute;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #0087C3;
            text-decoration: none;
        }

        body {
            position: relative;
            width: 100%;
            height: 29.7cm;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-family: poppins, sans-serif;
            font-size: 14px;
            /*font-family: SourceSansPro;*/
        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
            float: left;
            margin-top: 8px;
        }

        #logo img {
            height: 70px;
        }

        #company {
            width: 100%;
            float: right;
            text-align: right;
            padding-right: 6px;
            border-right: 6px solid #070a8c;
        }


        #details {
            margin-bottom: 50px;
        }

        .red {
            color:#070a8c;
            font-weight: 700;
        }

        #client {
            width: 100%;
            padding-left: 6px;
            border-left: 6px solid #070a8c;
            float: left;
        }

        #client .to {
            color: #777777;
        }

        h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        h1.name{
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {
            width: 100%;
            float: right;
            text-align: right;
            padding-right: 6px;
            border-right: 6px solid #070a8c;

        }

        #invoice-left {
            width: 100%;
            float: left;
            text-align: left;
            padding-left: 6px;
            border-left: 6px solid #070a8c;

        }

        #invoice h1 {
            width: 100%;
            color: #070a8c;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0  0 10px 0;
        }

        #invoice-left h1 {
            width: 100%;
            color: #070a8c;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0  0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #000;
        }

      #invoice-left .date {
            font-size: 1.1em;
            color: #000;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 5px;
        }

        table th,
        table td {
            /* padding: 2px; */
            /*background: #EEEEEE;*/
            text-align: left;
            border-bottom: 1px solid #FFFFFF;
        }


        table th {
            white-space: nowrap;
            font-weight: 700;
        }

        table td {
            text-align: left;
        }

        table td h3{
            color: #000;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
        }

        table .no {
            color: #000;
            font-size: 1.6em;

        }

        table .desc {
            text-align: left;
        }

        table .unit {

        }

        table .qty {
        }

        table .total {

            color: #000;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            /* padding: 10px 20px; */
            background: #FFFFFF;
            border-bottom: none;
            /* font-size: 1.2em; */
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr:last-child td {
            color: #000;
            font-size: 1.4em;
            border-top: 1px solid #000;

        }

        table tfoot tr td:first-child {
            border: none;
        }

        #thanks{
            font-size: 2em;
            margin-bottom: 50px;
        }

        #notices{
            padding-left: 6px;
            border-left: 6px solid #070a8c;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        .hd{
            color:#070a8c;
            font-size: large;
            font-weight: bold;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #000;
            padding: 8px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<div><img class="watermark" src="{{public_path().'/images/logo.png'}}"></div>
<header class="clearfix">
    <table >
        <tr >
            <td style="background-color: #ffffff47;">
                <div id="logo">

                    <img src="{{public_path().'/images/logo.png'}}">
                </div>
            </td>
            <td style="background-color: #ffffff47;">
                <div id="invoice">
                    <div class="date">Generated By: {{optional(\App\Models\User::find($quotation_details->created_by))->username}}</div>
                    <div class="date">Date Generated: {{$quotation_details->created_at}}</div>
                    <div class="date">Invoice #: <br> <span style="color:#070a8c">{{$quotation_details->invoice_number}}</span></div>
                </div>
            </td>
        </tr>
    </table>
</header>
<main>
    <div id="details" class="clearfix" style="width: 100%">
        <table>
            <tr>
                <td style="background-color: #ffffff47;">
                    @if($quotation_details->company_name == null)
                        <div id="invoice-left">
                            <h1>Invoiced To</h1>
                            <div class="date">{{$quotation_details->title." ".$quotation_details->name. " ".$quotation_details->surname}}</div>
                            <div class="date">{{$quotation_details->cell}}</div>
                            <div class="date"><a href="mailto:">{{$quotation_details->email}}</a></div>
                        </div>
                    @else
                        <div id="invoice-left">
                            <h3 class="hd">Invoiced To:</h3>
                            <div class="date">{{$quotation_details->company_name}}</div>
                            <div class="date">{{$quotation_details->address_1}}</div>
                            <div class="date">{{$quotation_details->city}}</div>
                            <div class="date">{{$quotation_details->company_cell}}</div>
                            <div class="date"><a href="mailto:{{$quotation_details->company_email}}">{{$quotation_details->company_email}}</a></div>
                            <div class="date">VAT #{{$quotation_details->vat_number}}</div>
                            <hr>
                            <h3 class="hd">For:</h3>
                            <div class="date">{{$quotation_details->title." ".$quotation_details->name. " ".$quotation_details->surname}}</div>
                            <div class="date">{{$quotation_details->cell}}</div>
                            <div class="date"><a href="mailto:">{{$quotation_details->email}}</a></div>
                        </div>
                    @endif
                </td>
                <td style="background-color: #ffffff47;">
                    <div id="company">
                        @include('livewire.components.address')
                    </div>
                </td>
            </tr>
        </table>

    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr style="background: #EEEEEE;">
            <th>Items #</th>
            <th>Module Code</th>
            <th>Service Types</th>
            <th>Semester</th>
            <th style="float:right">Amount</th>

        </tr>
        </thead>
        <tbody style="background-color: #ffffff47;">
            @php $count = 0; @endphp
            @foreach($quotation_modules as $quotation_module)
                @php $count = $count+1; @endphp
            <tr style="background-color: #ffffff47;">
                <td>{{$count}}</td>
                <td>{{$quotation_module->module_code}}</td>
                <td>{{$quotation_module->service_type}}</td>
                <td>{{$quotation_module->semester}}</td>
                <td style="float:right">{{get_setting('registration')['currency_symbol']}}{{$quotation_module->price}}.00</td>

            </tr>
            @endforeach

            <tr style="background-color: #ffffff47;">
                <td style="color: #ffffff00;">dummy</td>
                <td style="color: #ffffff00;">dummy</td>
                <td style="color: #ffffff00;">dummy</td>
                <td style="float:right;background: #ffffff00;"><strong>TOTAL</strong></td>
                <td style="float:right;background: #ffffff00;">{{get_setting('registration')['currency_symbol']}}{{$quotation_modules->pluck('price')->sum()}}.00</td>
            </tr>

        </tbody>

    </table>
    <br>
    @include('livewire.components.banking',['quotation_details' => $quotation_details])

</main>
<footer>
    This invoice was created on a computer and is valid for 7 days without the signature and seal. {{(new Helper)->banking()->bank_name}}
</footer>
</body>
</html>
