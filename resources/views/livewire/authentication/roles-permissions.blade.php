
@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components/tabs-accordian/custom-accordions.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('roles.permissions')}}">Roles & Permission</a></li>
    </ol>
@endsection

<div>
    <div class="seperator-header layout-top-spacing"></div>
    <div class="row layout-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                    <div id="iconsAccordion" class="accordion-icons">
                        <button class="mb-2" wire:click="$toggle('expand')">Expand All Roles For Editing</button>
                        @php $count = 0; @endphp
                        @foreach($this->roles as $role)
                            @php $count++; @endphp
                            <div class="card">
                                <div class="card-header" id="...">
                                    <section class="mb-0 mt-0">
                                        <div role="menu" data-toggle="collapse" data-target="#{{$role->name}}" aria-expanded="true" aria-controls="iconAccordionOne">
                                            <div class="accordion-icon">@include('components.svg.users')</div>
                                            {{ucwords($role->name)}} #{{$count}}  <div class="icons">@include('components.svg.arrow-down')</div>
                                        </div>
                                    </section>
                                </div>
                                <div id="{{$role->name}}" @if($expand) class="collapse" @endif aria-labelledby="{{$role->name}}" data-parent="#iconsAccordion">
                                    <div class="card-body">
                                        <h5>Update Permissions For Role:{{strtoupper($role->name)}}</h5>

                                        <div class="row">
                                                @foreach($this->permissions as $permission)
                                                    @php $checked = ($role->hasPermissionTo($permission->name))? 1 : 0;  @endphp
                                                    <div class="col-lg-4">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="{{$permission->name."|".$role->name}}"
                                                                   wire:click="give_permission_to_role('{{$checked}}','{{$role->name}}','{{$permission->name}}')"
                                                                   @if($checked) checked @endif value="{{$permission->name}}">
                                                            <label class="custom-control-label" for="{{$permission->name."|".$role->name}}">{{$permission->name}} </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script src="{{asset('assets/js/scrollspyNav.js')}}"></script>
    <script src="{{asset('assets/js/components/ui-accordions.js')}}"></script>
@endpush
