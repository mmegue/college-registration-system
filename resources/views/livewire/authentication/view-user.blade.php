    @section('page_title',$page_title)

    @section('links')
        <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css" />
    @endsection

    @section('breadcrumbs')
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('users')}}">Users</a></li>
            <li class="breadcrumb-item active"><a href="#">View User</a></li>
        </ol>
    @endsection

    <div>
        <div class="row layout-spacing">
            <!-- Content -->
            <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 layout-top-spacing">
                <div class="user-profile layout-spacing">
                    <div class="widget-content widget-content-area">
                        <div class="d-flex justify-content-between">
                            <h3 class="">Personal Information</h3>
                            <button @if(cn('can_crud_users')) wire:click="$toggle('update_user_info')" @else disabled @endif class="mt-2 btn btn-success btn-sm"><i class="fa fa-edit"></i></button>
                        </div>
                        <hr>
                        @if($update_user_info)
                            <div class="text-center user-info">

                                <div class="form-group row mb-2">
                                    <label for="user.title" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                        Title &nbsp;&nbsp;@include('components.error',['name'=>'user.title'])</label>
                                    <div class="col-sm-8">
                                        <select id="title" wire:model.defer="user.title" class="form-control form-control-sm" >
                                            @include('components.titles')
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <label for="user.username" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                        Username &nbsp;&nbsp;@include('components.error',['name'=>'user.username'])</label>
                                    <div class="col-sm-8">
                                        <input type="text" wire:model.defer="user.username" class="form-control form-control-sm">
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <label for="user.name" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                        Name &nbsp;&nbsp;@include('components.error',['name'=>'user.name'])</label>
                                    <div class="col-sm-8">
                                        <input type="text" wire:model.defer="user.name" class="form-control form-control-sm">
                                    </div>
                                </div>


                                <div class="form-group row mb-2">
                                    <label for="user.surname" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                        Surname &nbsp;&nbsp;@include('components.error',['name'=>'user.surname'])</label>
                                    <div class="col-sm-8">
                                        <input type="text" wire:model.defer="user.surname" class="form-control form-control-sm">
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <label for="user.cell" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                        Cell &nbsp;&nbsp;@include('components.error',['name'=>'user.cell'])</label>
                                    <div class="col-sm-8">
                                        <input type="text" wire:model.defer="user.cell" class="form-control form-control-sm">
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label for="user.email" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                        Email &nbsp;&nbsp;@include('components.error',['name'=>'user.email'])</label>
                                    <div class="col-sm-8">
                                        <input type="email" wire:model.defer="user.email" class="form-control form-control-sm">
                                    </div>
                                </div>


                                <div class="d-sm-flex justify-content-between mb-4">
                                    <div class="field-wrapper">
                                        <button type="button" @if(cn('can_crud_users')) wire:click="update_personal_info" @else disabled @endif
                                        class="btn btn-primary" value="">Update Account</button>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="user-info-list">
                                <dl class="row">
                                    <dt class="col-sm-3">Username</dt><dd class="col-sm-9">.{{$user->username}}</dd>
                                    <dt class="col-sm-3">Title</dt><dd class="col-sm-9">{{$user->title}}</dd>
                                    <dt class="col-sm-3">Name</dt><dd class="col-sm-9">{{$user->name}}</dd>
                                    <dt class="col-sm-3">Surname</dt><dd class="col-sm-9">{{$user->surname}}</dd>
                                    <dt class="col-sm-3">Cell</dt><dd class="col-sm-9">{{$user->cell}}</dd>
                                    <dt class="col-sm-3">Email</dt><dd class="col-sm-9"><a href="mailto:{{$user->email}}">{{$user->email}}</a></dd>
                                    <hr>
                                    <dt class="col-sm-3">Updated At</dt><dd class="col-sm-9">{{$user->updated_at}}</dd>
                                    <dt class="col-sm-3">Created At</dt><dd class="col-sm-9">{{$user->created_at}}</dd>
                                    <dt class="col-sm-3">User Roles</dt><dd class="col-sm-9">{{$userroles}}</dd>
                                </dl>

                            </div>
                        @endif
                    </div>
                </div>
                <div class="user-profilej layout-spacing">
                    <div class="widget-content widget-content-area">
                        <div class="d-flex justify-content-between">
                            <h5 class="">Reset Password</h5>
                        </div>
                        <hr>
                        @if(cn('can_update_password') || cn('can_crud_users'))
                            <div class="user-info-list">

                            <div class="form-group row mb-2">
                                <label for="password" class="text-left col-sm-12 col-md-12 col-form-label col-form-label-sm">
                                    Password &nbsp;&nbsp;@include('components.error',['name'=>'password'])</label>
                                <div class="col-sm-12 col-md-12">
                                    <input type="password" wire:model.defer="password" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="user.password_confirmation" class="text-left col-sm-12 col-md-12 col-form-label col-form-label-sm">
                                    Confirm Password &nbsp;&nbsp;@include('components.error',['name'=>'password_confirmation'])</label>
                                <div class="col-sm-12 col-md-12">
                                    <input type="password" name="password_confirmation" wire:model.defer="password_confirmation" class="form-control  form-control-sm">
                                </div>
                            </div>
                            <button type="button" @if(cn('can_crud_users')) wire:click="update_user_password" @else disabled @endif class="btn btn-primary mt-3">Reset Password</button>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 layout-top-spacing">
                <div class="bio layout-spacing ">
                    <div class="widget-content widget-content-area">
                        <button wire:click="toggle('update_roles')" class="btn btn-secondary mb-4 mr-2 btn-sm">Roles</button>
                        <button wire:click="toggle('update_permissions')" class="btn btn-warning mb-4 mr-2 btn-sm">Permissions</button>
                        <div class="bio-skill-box">
                            <div class="row">
                                    @if($update_roles)
                                        @can('can_change_roles')
                                        <div class="col-xl-8 col-lg-12 col-md-11 col-sm-11 mb-xl-2 mb-2 ">
                                            <div class="d-flex b-skills">
                                                <div class="">
                                                    <h5>Roles</h5>
                                                    <p class="text-danger">Please Note Avoid by any cost giving any user more than 1 role. If so make sure you remove these permissions from the user
                                                        ['can_crud_users']  ['can_change_permissions']  ['can_change_roles']
                                                        This will allow the user with upper and lower roles to be edited with a user with lower roles.Use Permissions instead
                                                    </p>
                                                    <div class="row">
                                                        @foreach($all_roles as $role)
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="{{$role->name}}"
                                                                           wire:click="update_roles_" wire:model="roles"  value="{{$role->name}}">
                                                                    <label class="custom-control-label" for="{{$role->name}}">{{$role->name}}</label>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endcan
                                    @endif
                                    
                                     @if($update_permissions)
                                        @can('can_change_permissions')
                                            <div class="d-flex b-skills">

                                                <div class="">
                                                    <h5>Direct Permissions</h5>
                                                    <p>Permissions assigned outside the role</p>
                                                    <div class="row">
                                                        @foreach($direct_permissions as $permission)
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" 
                                                                          wire:model="direct_permissions"  value="{{$permission}}" disabled>
                                                                    <label class="custom-control-label" >{{$permission}}</label>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                   

                                                <hr>
                                                <div class="">
                                                    <h5>Inherited Permissions</h5>
                                                    <p class="text-danger">Permissions inherited from the user's roles.
                                                        NOTE:These permissions are added directly to the roles and they cannot be removed or added here,
                                                        they can only be <a class="text-success" href="#">updated</a> on roles only.
                                                    </p>
                                                    <div class="row">
                                                        @foreach($role_permissions as $permission)
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                           wire:model="role_permissions"  value="{{$permission}}" disabled>
                                                                    <label class="custom-control-label">{{$permission}}</label>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                
                                                <hr>
                                                <div class="">
                                                    <h5>All Permissions</h5>
                                                    <p>All permissions which apply on the user (inherited and direct)</p>
                                                    <div class="row">
                                                        @foreach($all_user_permissions as $permission)
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                           wire:model="all_user_permissions" value="{{$permission}}" disabled>
                                                                    <label class="custom-control-label" >{{$permission}}</label>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>



                                                    <hr>
                                                    <div class="">
                                                        <h5>Update Permissions</h5>

                                                        <div class="row">
                                                            @foreach($all_permissions as $permission)
                                                                @php $checked = ($user->can($permission->name))? 1 : 0;  @endphp
                                                                @if(!collect($role_permissions->toArray())->contains($permission->name))
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="{{$permission->name}}"
                                                                                   wire:click="give_permission('{{$checked}}','{{$permission->name}}')"
                                                                                   @if($checked) checked @endif value="{{$permission->name}}">
                                                                            <label class="custom-control-label" for="{{$permission->name}}">{{$permission->name}}</label>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        </div>


                                                    </div>
                                                </div>

                                            </div>
                                        @endcan
                                     @endif
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    @section('scripts')

    @endsection
