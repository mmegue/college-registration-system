@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{route('users')}}">Users</a></li>
        <li class="breadcrumb-item active"><a href="#">View User</a></li>
    </ol>
@endsection

<div>
    <div class="row layout-spacing">
        <!-- Content -->
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 layout-top-spacing">
            <div class="user-profile layout-spacing">
                <div class="widget-content widget-content-area">
                    <div class="d-flex justify-content-between">
                        <h3 class="">Personal Information</h3>
                        <button wire:click="$toggle('update_user_info')" class="mt-2 btn btn-success btn-sm"><i class="fa fa-edit"></i></button>
                    </div>
                    <hr>
                    @if($update_user_info)
                        <div class="text-center user-info">

                            <div class="form-group row mb-2">
                                <label for="user.title" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                    Title &nbsp;&nbsp;@include('components.error',['name'=>'user.title'])</label>
                                <div class="col-sm-8">
                                    <select id="title" wire:model.defer="user.title" class="form-control form-control-sm" >
                                        @include('components.titles')
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="user.username" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                    Username &nbsp;&nbsp;@include('components.error',['name'=>'user.username'])</label>
                                <div class="col-sm-8">
                                    <input type="text" wire:model.defer="user.username" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="user.name" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                    Name &nbsp;&nbsp;@include('components.error',['name'=>'user.name'])</label>
                                <div class="col-sm-8">
                                    <input type="text" wire:model.defer="user.name" class="form-control form-control-sm">
                                </div>
                            </div>


                            <div class="form-group row mb-2">
                                <label for="user.surname" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                    Surname &nbsp;&nbsp;@include('components.error',['name'=>'user.surname'])</label>
                                <div class="col-sm-8">
                                    <input type="text" wire:model.defer="user.surname" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="user.cell" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                    Cell &nbsp;&nbsp;@include('components.error',['name'=>'user.cell'])</label>
                                <div class="col-sm-8">
                                    <input type="text" wire:model.defer="user.cell" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="user.email" class="text-left col-sm-4 col-form-label col-form-label-sm">
                                    Email &nbsp;&nbsp;@include('components.error',['name'=>'user.email'])</label>
                                <div class="col-sm-8">
                                    <input type="email" wire:model.defer="user.email" class="form-control form-control-sm">
                                </div>
                            </div>


                            <div class="d-sm-flex justify-content-between mb-4">
                                <div class="field-wrapper">
                                    <button type="button" wire:click="update_personal_info" class="btn btn-primary" value="">Update Account</button>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="user-info-list">
                            <dl class="row">
                                <dt class="col-sm-3">Username</dt><dd class="col-sm-9">{{$user->username}}</dd>
                                <dt class="col-sm-3">Title</dt><dd class="col-sm-9">{{$user->title}}</dd>
                                <dt class="col-sm-3">Name</dt><dd class="col-sm-9">{{$user->name}}</dd>
                                <dt class="col-sm-3">Surname</dt><dd class="col-sm-9">{{$user->surname}}</dd>
                                <dt class="col-sm-3">Cell</dt><dd class="col-sm-9">{{$user->cell}}</dd>
                                <dt class="col-sm-3">Email</dt><dd class="col-sm-9"><a href="mailto:{{$user->email}}">{{$user->email}}</a></dd>
                                <hr>
                                <dt class="col-sm-3">Updated At</dt><dd class="col-sm-9">{{$user->updated_at}}</dd>
                                <dt class="col-sm-3">Created At</dt><dd class="col-sm-9">{{$user->created_at}}</dd>
                                <dt class="col-sm-3">User Roles</dt><dd class="col-sm-9">{{$userroles}}</dd>
                            </dl>

                        </div>
                    @endif
                </div>
            </div>
        </div>

        <!-- Content -->
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 layout-top-spacing">
            <div class="user-profilej layout-spacing">
                <div class="widget-content widget-content-area">
                    <div class="d-flex justify-content-between">
                        <h5 class="">Reset Password</h5>
                    </div>
                    <hr>
                    <div class="user-info-list">
                        <div class="form-group row mb-2">
                            <label for="current_password" class="text-left col-sm-12 col-md-12 col-form-label col-form-label-sm">
                                Current Password &nbsp;&nbsp;@include('components.error',['name'=>'current_password'])</label>
                            <div class="col-sm-12 col-md-12">
                                <input type="password" wire:model.defer="current_password" class="form-control form-control-sm">
                            </div>
                        </div>

                        <div class="form-group row mb-2">
                            <label for="password" class="text-left col-sm-12 col-md-12 col-form-label col-form-label-sm">
                                New Password &nbsp;&nbsp;@include('components.error',['name'=>'password'])</label>
                            <div class="col-sm-12 col-md-12">
                                <input type="password" wire:model.defer="password" class="form-control form-control-sm">
                            </div>
                        </div>

                        <div class="form-group row mb-2">
                            <label for="user.password_confirmation" class="text-left col-sm-12 col-md-12 col-form-label col-form-label-sm">
                                Confirm New Password &nbsp;&nbsp;@include('components.error',['name'=>'password_confirmation'])</label>
                            <div class="col-sm-12 col-md-12">
                                <input type="password" name="password_confirmation" wire:model.defer="password_confirmation" class="form-control  form-control-sm">
                            </div>
                        </div>
                        <button type="button" wire:click="update_user_password" class="btn btn-primary mt-3">Reset Password</button>
                    </div>
                </div>
            </div>
        </div>



@section('scripts')

@endsection
