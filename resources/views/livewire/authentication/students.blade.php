
@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{route('students')}}">Students</a></li>
    </ol>
@endsection

<div>
    <div class="seperator-header layout-top-spacing"></div>
    <div class="row layout-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div x-data="{ open:false }">
                <div class="mb-1">
                    <button @if(cn('can_create_users') || cn('can_crud_users')) @click="open=!open" @else disabled @endif class="btn btn-secondary btn-sm "><i class="fa fa-user"></i> Create User</button>
                </div>
                <div class="statbox widget box box-shadow">

                    <div x-show="open">
                        <div class="widget-content widget-content-area">
                            <div class="form">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <select wire:model.lazy="title" class="form-control form-control-sm">
                                            <option value="">Title</option>
                                            @include('components.titles')
                                        </select>
                                    </div>

                                    <label for="title" class="col-sm-6 col-form-label col-form-label-md">Title &nbsp;<br>
                                        @include('components.error',['name'=>'title'])</label>
                                </div>

                                <x-input placeholder="Username" wire:model.lazy="username">Username</x-input>
                                <x-input placeholder="Name" wire:model.lazy="name">Name</x-input>
                                <x-input placeholder="Surname" wire:model.lazy="surname">Surname</x-input>
                                <x-input placeholder="Cell" wire:model.lazy="cell">Cell</x-input>
                                <x-input placeholder="Email" type="email" wire:model.lazy="email">Email</x-input>

                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <select wire:model.lazy="role" class="form-control form-control-sm" >
                                            <option value="">Roles</option>
                                            @include('livewire.components.roles-select')
                                        </select>
                                    </div>

                                    <label for="role" class="col-sm-6 col-form-label col-form-label-md">Roles &nbsp;<br>
                                        @include('components.error',['name'=>'role'])</label>
                                </div>

                                <div class="d-sm-flex justify-content-between mb-4">
                                    <div class="field-wrapper">
                                        <button type="button" @if(cn('can_create_users') || cn('can_crud_users')) wire:click="register_new_user" @else disabled @endif  class="btn btn-primary" value="">Create Account</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div x-show="!open">
                        <div class="widget-content widget-content-area">
                            <livewire:authentication.students-table/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


@push('scripts')

@endpush
