@section('page_title',$page_title)

@section('links')
    <link href="{{asset('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{asset('assets/css/tables/table-basic.css')}}" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/forms/theme-checkbox-radio.css')}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('plugins/flatpickr/flatpickr.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('plugins/flatpickr/custom-flatpickr.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{asset('assets/css/dashboard/dash_2.css')}}" rel="stylesheet" type="text/css" >

@endsection

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('student.dashboard')}}">Dashboard</a></li>
    </ol>
@endsection

<div>
    <div class="row layout-spacing" >
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-top-spacing">
            <div class="row" >

                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-wallet-one">
                        <div class="wallet-info text-center mb-3">
                            <h2 class="wallet-title mb-3">My Quotations</h2>
                            <h5 class="total-amount mb-3">[{{$my_quotations_count}}]</h5>
                        </div>

                        <div class="wallet-action text-center d-flex justify-content-around text-success">
                            Total number of quotations requested.
                        </div>

                        <a href="{{route('student.quotations')}}" class="btn btn-secondary w-100 mt-3 _effect--ripple waves-effect waves-light">View My Quotations</a>

                    </div>
                </div>


                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-wallet-one">
                        <div class="wallet-info text-center mb-3">
                            <h2 class="wallet-title mb-3">My Invoices</h2>
                            <h5 class="total-amount mb-3">[{{$my_invoices_count}}]</h5>
                        </div>
                        <div class="wallet-action text-center d-flex justify-content-around text-success">
                            Total number of invoices requested.
                        </div>

                        <a href="{{route('student.quotations')}}" class="btn btn-primary w-100 mt-3 _effect--ripple waves-effect waves-light">View My Invoices</a>
                    </div>
                </div>

                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-wallet-one">
                        <div class="wallet-info text-center mb-3">
                            <h2 class="wallet-title mb-3">My Referrals</h2>
                            <h5 class="total-amount mb-3">[{{$my_referrals}}]</h5>
                        </div>
                        <div class="wallet-action text-center d-flex justify-content-around text-success">
                            Total number of students I referred to the college.
                        </div>

                        <a href="#" class="btn btn-warning w-100 mt-3 _effect--ripple waves-effect waves-light">View My Referrals</a>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-wallet-one">
                        <div class="wallet-info text-center mb-3">
                            <h2 class="wallet-title mb-3">My Statement</h2>
                            <h5 class="total-amount mb-3">[--]</h5>
                        </div>
                        <div class="wallet-action text-center d-flex justify-content-around text-success">
                            Current semester statement .
                        </div>

                        <a href="#" class="btn btn-success w-100 mt-3 _effect--ripple waves-effect waves-light">Download My Statement</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-wallet-one">
                        <div class="wallet-info text-center mb-3">
                            <h2 class="wallet-title mb-3">My Timetable</h2>
                            <h5 class="total-amount mb-3">[0]</h5>
                        </div>
                        <div class="wallet-action text-center d-flex justify-content-around text-secondary">
                            Time table based on registered modules.
                        </div>

                        <a href="#" class="btn btn-dark w-100 mt-3 _effect--ripple waves-effect waves-light">View My Timetable</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-wallet-one">
                        <div class="wallet-info text-center mb-3">
                            <h2 class="wallet-title mb-3">My Resources</h2>
                            <h5 class="total-amount mb-3">[--]</h5>
                        </div>
                        <div class="wallet-action text-center d-flex justify-content-around text-success">
                            Study resources for registered modules.
                        </div>

                        <a href="#" class="btn btn-danger w-100 mt-3 _effect--ripple waves-effect waves-light">View My Resources</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>