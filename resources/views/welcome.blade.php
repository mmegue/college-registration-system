<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Noble Tutors, Landing page, Welcome, Get Quote, Register">
    <meta name="description" content="Welcome to Noble Tutors Portal. Unveiling the natural intelligence in you..." />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
    <title>Welcome to {{env('APP_NAME')}}</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="https://app.nobletutors.co.za/assets/images/icon.png">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <style>
        .btn-border-filled, .btn-common{
            background-color: #3568F6;
            border: 1px solid #3568F6
        }
        .hero-area .contents .btn-border{
            color: #3568F6 !important;
        }
        .color-white{
            color: #ffffff !important;
        }

        .services-item{
            cursor: pointer;
        }
    </style>
</head>
<body>

<header id="home" class="hero-area">
    <div class="overlay">
        <span></span>
        <span></span>
    </div>
    <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
            <a href="https://nobletutors.co.za" class="navbar-brand"><img src="https://app.nobletutors.co.za/assets/images/logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>


        </div>
    </nav>
    <div class="container">
        <div class="row space-100">
            <div class="col-lg-6 col-md-12 col-xs-12">
                <div class="contents">
                    <h6 class="head-title mb-0 color-white">Welcome to</h6>
                    <h1 class="mt-0 color-white">{{env('APP_NAME')}}</h1>

                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-xs-12 p-0">
                <div class="intro-img">
                    <img src="https://gitlab.com/uploads/-/system/project/avatar/55102073/cms-logo.png" width="200" alt="">
                </div>
            </div>
        </div>
    </div>
</header>

<br><hr><br>
<section id="services" class="section">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="services-item text-center">
                    <a href="{{route('guest.request.quote')}}">
                        <div class="icon">
                            <i class="fa fa-file-text-o"></i>
                        </div>
                        <h4>Get Quote</h4>
                        <p>Would you like to know the pricing for some of our services?</p>
                    </a>
                </div>
            </div>

            @auth
                <div class="col-lg-4 col-md-6 col-xs-12" style="border: #0b2e13;">
                    <div class="services-item text-center">
                        <a href="{{ url('/admin/dashboard') }}">
                            <div class="icon">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            <h4>Dashboard</h4>

                        </a>
                    </div>
                </div>
            @else
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="services-item text-center">
                        <a href="{{route('guest.request.quote')}}">
                            <div class="icon">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            <h4>Register</h4>
                            <p>Create an account with {{env('APP_NAME')}}< to get access to quality services.</p>
                        </a>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="services-item text-center">
                        <a href="{{route('login')}}">
                            <div class="icon">
                                <i class="fa fa-sign-in"></i>
                            </div>
                            <h4>Login</h4>
                            <p>Login to your portal using your existing login credentials.</p>
                        </a>
                    </div>
                </div>
            @endauth
        </div>
    </div>
</section>
<br><hr><br>
<section id="services" class="section">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="services-item text-center">
                    <a href="{{route('timetable-weekday')}}">
                        <div class="icon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <h4>Weekday Timetable</h4>
                    </a>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="services-item text-center">
                    <a href="{{route('timetable-weekend-sat')}}">
                        <div class="icon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <h4>Weekend Saturday Timetable</h4>

                    </a>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="services-item text-center">
                    <a href="{{route('timetable-weekend-sun')}}">
                        <div class="icon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <h4>Weekend Sunday Timetable</h4>

                    </a>
                </div>
            </div>

        </div>
    </div>
</section>
<br><hr><br>
<footer>

    <section id="footer-Content" style="padding: 0;">
        <div class="copyright mt-0">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="site-info text-center">
                            <p>{{config('app.name')}} &copy; {{date('Y')}}</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </section>

</footer>

<div id="preloader">
    <div class="loader" id="loader-1"></div>
</div>

</body>
</html>


