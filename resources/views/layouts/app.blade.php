@php
    $auth = Auth::user();
@endphp
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <title>{{ config('app.name')." | "}}@yield('page_title')</title>

        @livewireStyles

        <link rel="icon" type="image/x-icon" href="{{asset('assets/img/favicon.ico')}}"/>
        <link href="{{asset('assets/css/loader.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{asset('assets/js/loader.js')}}"></script>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
        <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('plugins/fontawesome/css/all.css')}}" rel="stylesheet" type="text/css" />

        <!--<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />-->
        <!-- END GLOBAL MANDATORY STYLES -->
        <!--  BEGIN CUSTOM STYLE FILE  -->
        <link href="{{asset('assets/css/elements/miscellaneous.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/elements/breadcrumb.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('plugins/animate/animate.css')}}" rel="stylesheet" type="text/css" />
      
        <!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        @yield('links')


        <link href="{{asset('assets/css/components/custom-modal.css')}}" rel="stylesheet" type="text/css" />
        
    </head>
    <body>
        <!-- BEGIN LOADER -->
        <div id="load_screen">
            <div class="loader">
                <div class="loader-content">
                    <div class="spinner-grow align-self-center"></div>
                </div>
            </div>
        </div>
        <!--  END LOADER -->

        @livewire('components.navigation')

        <!--  BEGIN MAIN CONTAINER  -->
        <div class="main-container " id="container">

            <div class="overlay"></div>
            <div class="search-overlay"></div>

            @include('components.sidebar')

            <!--  BEGIN CONTENT AREA  -->
            <div id="content" class="main-content">
                <div class="layout-px-spacing mt-4">
                    {{ $slot }}
                </div>
                <!--FOOTER-->
                <div class="footer-wrapper">
                    <div class="footer-section f-section-1">
                        <p class=""> Copyright © {{date('Y')}}
                            <a target="_blank" href="https://nobletutors.co.za">{{config('app.name')}}</a>, All rights reserved.
                        </p>
                    </div>
                    <div class="footer-section f-section-2">
                        <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>
                            Noble IT Solutions
                        </p>
                    </div>
                </div>
            </div>
            <!--  END CONTENT AREA  -->


        </div>
        <!-- END MAIN CONTAINER -->

        <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
        <script src="{{asset('assets/js/libs/jquery-3.1.1.min.js')}}"></script>
        <script src="{{asset('bootstrap/js/popper.min.js')}}"></script>
        <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
        <script src="{{asset('assets/js/app.js')}}"></script>
        <!-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> -->
        <script src="{{asset('assets/js/sweetalert2@11.js')}}"></script>

        <script src="{{asset('assets/js/custom.js')}}"></script>
        <!-- END GLOBAL MANDATORY SCRIPTS -->

        @livewireScripts
        <script>
            $(document).ready(function() {
                App.init();

            });
            window.addEventListener('swal',function(e){
                Swal.fire(e.detail);
            });

        </script>
        <!-- <script src="//unpkg.com/alpinejs" defer></script> -->
         <script src="{{asset('assets/js/apline.min.js')}}"></script>
        
        @stack('scripts')
    </body>
</html>