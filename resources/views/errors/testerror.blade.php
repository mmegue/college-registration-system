@extends('errors::illustrated-layout')

@section('code', '403 😭')

@section('title', __('Unauthorized Access'))

@section('image')

    <div style="background-image: url('/images/404-bg.jpg');" class="absolute pin bg-no-repeat md:bg-left lg:bg-center">
    </div>

@endsection

@section('message', __('Sorry, you don\'t have enough authority to perform requested action.'))