<div>
@section('page_title',$page_title)

@section('links')
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
        <link href="{{asset('plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    @endsection

    @section('breadcrumbs')
        <ol class="breadcrumb">
            <!--
            <li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Components</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="javascript:void(0);">UI Kit</a></li>
             -->
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item active"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        </ol>
    @endsection

    @section('content')
        <div class="row layout-top-spacing">
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-card-one">
                    <div class="widget-content">
                        STUDENT - {{$page_title}}
                    </div>
                </div>

            </div>
        </div>
    @endsection


    @section('scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
        <script src="{{asset('plugins/apex/apexcharts.min.js')}}"></script>
        <script src="{{asset('assets/js/dashboard/dash_1.js')}}"></script>
        <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    @endsection


</div>