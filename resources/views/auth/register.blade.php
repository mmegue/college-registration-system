@extends('layouts.auth')

@section('content')
    <div class="form-content">
        <img src="{{asset('images/logo.png')}}" width="150" alt="">
        <h1 class="">Create Account </h1>
        <p class="">Create an account to continue.</p>

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form">

                <div id="username-field" class="field-wrapper input">
                    <div class="d-flex justify-content-between">
                        <label for="title" class="float-left">TITLE @include('components.error',['name'=>'title'])</label>
                    </div>
                        <select id="title" class="form-control form-control-sm" name="title">
                        <option value="{{ old('title') }}">{{ old('title') }}</option>
                        @include('components.titles')
                    </select>

                </div>

                <div id="username-field" class="field-wrapper input">
                    <div class="d-flex justify-content-between">
                        <label for="name" class="float-left">NAME @include('components.error',['name'=>'name'])</label>
                    </div>
                        @include('components.svg.user')
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                           name="name" value="{{ old('name') }}"  autocomplete="name" >

                </div>

                <div id="username-field" class="field-wrapper input">
                    <div class="d-flex justify-content-between">
                        <label for="surname" class="float-left">SURNAME @include('components.error',['name'=>'surname'])</label>
                    </div>
                        @include('components.svg.user')
                    <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror"
                           name="surname" value="{{ old('surname') }}"  autocomplete="surname" >

                </div>

                <div id="username-field" class="field-wrapper input">
                    <div class="d-flex justify-content-between">
                        <label for="email" class="float-left">EMAIL @include('components.error',['name'=>'email'])</label>
                    </div>
                        @include('components.svg.mail')
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}"  autocomplete="email" >

                </div>

                <div id="username-field" class="field-wrapper input">
                    <div class="d-flex justify-content-between">
                        <label for="cell_" class="float-left">CELLPHONE @include('components.error',['name'=>'cell'])</label>
                    </div>

                    {{--<input id="cell" type="text" class="form-control @error('cell') is-invalid @enderror"--}}
                    {{--name="cell" value="{{ old('cell') }}"  autocomplete="cell" >--}}
                    <input style="width: 100%;" id="cell_" type="tel" name="cell_" class="form-control @error('cell') is-invalid @enderror" />
                    <input type="hidden"  id="cell" name="cell">
                </div>

                <div id="password-field" class="field-wrapper input mb-2">
                    <div class="d-flex justify-content-between">
                        <label for="password">PASSWORD @include('components.error',['name'=>'password'])</label>
                    </div>
                    @include('components.svg.lock')
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                           name="password"  autocomplete="current-password">
                    @include('components.svg.eye')
                </div>

                <div id="password-field" class="field-wrapper input mb-2">
                    <div class="d-flex justify-content-between">
                        <label for="password">CONFIRM PASSWORD</label>
                    </div>
                    @include('components.svg.lock')
                    <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror"
                           name="password_confirmation"  autocomplete="current-password">
                </div>

                <div class="d-sm-flex justify-content-between mb-4">
                    <div class="field-wrapper">
                        <button type="submit" class="btn btn-primary" value="">Create Account</button>
                    </div>
                </div>

                <p class="signup-link register">Already have an account? <a href="{{route('login')}}">Log in</a></p>

            </div>
        </form>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function() {
                const phoneInputField = document.querySelector("#cell_");
                const phoneInput = window.intlTelInput(phoneInputField, {
                    preferredCountries: ["za", "zw", "us"],
                    utilsScript:
                        "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
                });
                //const phoneNumber = phoneInput.getNumber();

                $("#cell_").on("focusout", function(){
                    const phoneNumber = phoneInput.getNumber();
                    $("#cell").val(phoneNumber)
                });
            });
        </script>
    @endpush

@endsection
