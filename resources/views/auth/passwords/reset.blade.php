{{--}}@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
@extends('layouts.auth')

@section('content')
    <div class="form-content">

        <h1 class="">Reset Password</h1>
        <p class="">Reset your password</p>

        <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <div class="form">
                <input type="hidden" name="token" value="{{ $token }}">
                <div id="username-field" class="field-wrapper input">
                    <div class="d-flex justify-content-between">
                        <label for="email" class="float-left">EMAIL @include('components.error',['name'=>'email'])</label>
                    </div>
                    @include('components.svg.mail')
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}"  autocomplete="email" >

                </div>

                <div id="password-field" class="field-wrapper input mb-2">
                    <div class="d-flex justify-content-between">
                        <label for="password">PASSWORD @include('components.error',['name'=>'password'])</label>
                    </div>
                    @include('components.svg.lock')
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                           name="password"  autocomplete="current-password">
                    @include('components.svg.eye')
                </div>

                <div id="password-field" class="field-wrapper input mb-2">
                    <div class="d-flex justify-content-between">
                        <label for="password">CONFIRM PASSWORD</label>
                    </div>
                    @include('components.svg.lock')
                    <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror"
                           name="password_confirmation"  autocomplete="current-password">
                </div>

                <div class="d-sm-flex justify-content-between">
                    <div class="field-wrapper">
                        <button type="submit" class="btn btn-primary" value="">Reset Password</button>
                    </div>
                </div>

            </div>
        </form>

    </div>
@endsection

