{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Confirm Password') }}</div>

                <div class="card-body">
                    {{ __('Please confirm your password before continuing.') }}

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
@extends('layouts.auth')

@section('content')
    <div class="form-content">

        <h1 class="">Confirm Password</h1>
        <p class="">Please confirm your password before continuing.</p>

        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf
            <div class="form">

                <div id="password-field" class="field-wrapper input mb-2">
                    <div class="d-flex justify-content-between">
                        <label for="password">PASSWORD @include('components.error',['name'=>'password'])</label>
                    </div>
                    @include('components.svg.lock')
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                           name="password"  autocomplete="current-password">
                    @include('components.svg.eye')
                </div>

                <div id="password-field" class="field-wrapper input mb-2">
                    <div class="d-flex justify-content-between">
                        <label for="password">CONFIRM PASSWORD</label>
                    </div>
                    @include('components.svg.lock')
                    <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror"
                           name="password_confirmation"  autocomplete="current-password">
                </div>

                <div class="d-sm-flex justify-content-between mb-4">
                    <div class="field-wrapper">
                        <button type="submit" class="btn btn-dark" value="">Confirm Password</button>
                    </div>
                </div>

                @if (Route::has('password.request'))
                    <p class="signup-link register">Did You Forgot Your Password ? <a href="{{route('password.request')}}">Forgot Password</a></p>
                @endif


            </div>
        </form>

    </div>
@endsection

