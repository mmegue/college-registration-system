@extends('layouts.auth')

@section('content')
    <div class="form-content">
        <img src="{{asset('images/logo.png')}}" width="150" alt="">
        <h1 class="">Sign In</h1>
        <p class="">Log in to your account to continue.</p>

        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form">

                <div id="username-field" class="field-wrapper input">
                    <label for="email" class="float-left">EMAIL</label>
                    @include('components.svg.user')
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                </div>

                <div id="password-field" class="field-wrapper input mb-2">
                    <div class="d-flex justify-content-between">
                        <label for="password">PASSWORD</label>
                    </div>
                    @include('components.svg.lock')
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                           name="password" required autocomplete="current-password">
                    @include('components.svg.eye')
                </div>

                <div class="field-wrapper terms_condition">
                    <div class="n-chk">
                        <label class="new-control new-checkbox checkbox-primary">
                            <input type="checkbox" class="new-control-input">
                            <span class="new-control-indicator"></span><span>Remember Me</span>
                        </label>
                    </div>

                </div>

                <div class="d-sm-flex justify-content-between">
                    <div class="field-wrapper">
                        <button type="submit" class="btn btn-primary" value="">Log In</button>
                    </div>
                </div>

{{--                <div class="col-md-6 offset-md-3 mt-4 w-100">--}}
{{--                    <a href="{{route('login')}}" class="btn btn-danger btn-block">Login with Google</a>--}}
{{--                    <a href="{{route('login')}}" class="btn btn-primary btn-block">Login with Facebook</a>--}}
{{--                    <a href="{{route('login')}}" class="btn btn-dark btn-block">Login with Github</a>--}}
{{--                </div>--}}
                <div class="d-sm-fle mt-2">
                    <a style="float: left; text-decoration: underline;" href="{{route('password.request')}}">Forgot Your Password?</a>
                    <a style="float: right; text-decoration: underline;" href="{{route('register')}}" >Create Account</a>
                </div>
{{--                <p class="signup-link">Not registered ? <a href="{{route('register')}}">Create an account</a></p>--}}

            </div>
        </form>

    </div>
@endsection
